const attachTimestampFields = (entity) => {
  return {
    ...entity,
    createdAt: Date.now(),
    updatedAt: null,
    deletedAt: null,
  };
};

module.exports = {
  attachTimestampFields,
};
