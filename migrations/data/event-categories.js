const { attachTimestampFields } = require('./utilities');

const eventCategories = [
  {
    title: 'Aerial',
    imageUrl: 'https://img.vimbly.com/images//full_photos/silks-3.jpg',
  },
  {
    title: 'Aerobics',
    imageUrl:
      'https://econofitness.ca/getmedia/be3a4897-a07b-4e02-8352-29cd25e0062b/D-aerobics-pg',
  },
  {
    title: 'Ballet',
    imageUrl:
      'https://national.ballet.ca/NBOC/media/MediaLibrary/Images/Biographies/Principals/Heather-Ogden/Ogden.jpg?width=673&height=811&ext=.jpg',
  },
  {
    title: 'Baseball',
    imageUrl:
      'https://cdn.rotoballer.com/2019/04/chad-pinder-fantasy-baseball-waiver-wire-pickups-second-base-MLB-DFS-lineup-picks-icon.jpeg',
  },
  {
    title: 'Basketball',
    imageUrl:
      'https://www.gannett-cdn.com/presto/2018/11/05/USAT/c265117e-39ce-4426-a423-b2fc24c14e7e-USATSI_10710500.jpg?width=534&height=712&fit=bounds&auto=webp',
  },
  {
    title: 'Bowling',
    imageUrl:
      'https://www.qubicaamf.com/images-1/bwc-images/ildemaro-1.jpg/@@images/cd1d3e74-fea8-4f78-b107-4290e3cc5326.jpeg',
  },
  {
    title: 'Boxing',
    imageUrl:
      'https://asset-sports.abs-cbn.com/web/dev/articles/1547971796_ap19020222677351.jpg',
  },
  {
    title: 'CrossFit',
    imageUrl:
      'https://i.cbc.ca/1.4772106.1533241552!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/898430526.jpg',
  },
  {
    title: 'Cycling',
    imageUrl:
      'https://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/2/2016/12/Dan-Baines-cornering-bike-handling-Gabba-wet-rain-winter.jpg',
  },
  {
    title: 'Fencing',
    imageUrl:
      'https://www.straitstimes.com/sites/default/files/styles/article_pictrure_780x520_/public/articles/2018/04/10/81764540-b10b-4b61-a983-2b9dd115a5de.jpg?itok=edFmzk5h&timestamp=1523299689',
  },
  {
    title: 'Football',
    imageUrl:
      'https://www.gannett-cdn.com/-mm-/86b1b821d2c65cc63c2d81be94bac666be8cbeae/c=174-0-2999-2119/local/-/media/2018/10/22/USATODAY/usatsports/37a09e34-3d51-4c20-93f4-886c870b9dfc-kevin_c.jpg?width=534&height=401&fit=crop',
  },
  {
    title: 'Golf',
    imageUrl:
      'https://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/5/2019/04/tiger-coach.jpg',
  },
  {
    title: 'Ice Hockey',
    imageUrl:
      'https://www.tsn.ca/polopoly_fs/1.1172254!/fileimage/httpImage/image.jpg_gen/derivatives/landscape_620/patrik-laine-and-auston-matthews.jpg',
  },
  {
    title: 'Kayaking',
    imageUrl:
      'https://www.pandotrip.com/wp-content/uploads/2015/07/Top-10-Kayaking-Norway2-980x653.jpg',
  },
  {
    title: 'Lacrosse',
    imageUrl:
      'https://neulioncs.hs.llnwd.net/pics33/800/UO/UONXXXABWMFGECJ.20190224003946.jpg',
  },
  {
    title: 'MMA',
    imageUrl:
      'https://cdn.evolve-vacation.com/wp-content/uploads/2018/02/MMA-wrestler.jpg',
  },
  {
    title: 'Table Tennis',
    imageUrl:
      'https://www.thoughtco.com/thmb/1jTlzfVAjV9qhsBwha3E5Jj61cw=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/488273125-56a92aa83df78cf772a463a3.jpg',
  },
  {
    title: 'Volleyball',
    imageUrl:
      'http://image.cdnllnwnl.xosnetwork.com/pics33/640/VX/VXYLLOAJFBXNONV.20171003200634.jpg',
  },
  {
    title: 'Yoga',
    imageUrl:
      'https://media.self.com/photos/5b7c4e71ecbb7f4c41c77335/4:3/w_746/triangle-pose-beginner-yoga.jpg',
  },
  {
    title: 'Adventure Racing',
    imageUrl:
      'http://advancetravelinternational.com/wp-content/uploads/2018/01/Adventure-Racing.jpg',
  },
  {
    title: 'Aikido',
    imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/e/e3/Shihonage.jpg',
  },
  {
    title: 'Bachata',
    imageUrl: 'https://i.udemycdn.com/course/750x422/1745602_bd77_2.jpg',
  },
  {
    title: 'Badminton',
    imageUrl:
      'https://www.thestar.com.my/~/media/online/2019/03/21/18/02/55754.ashx/?w=620&h=413&crop=1&hash=CB69330E4CD8410F1D818BC0747CEFD91A2D2EF8',
  },
  {
    title: 'Barre',
    imageUrl:
      'https://media.self.com/photos/57d8a00724fe9dae32831cd6/master/w_728,c_limit/best-barre-workouts-physique-57-870.jpg',
  },
  {
    title: 'Belly Dancing',
    imageUrl:
      'https://ae01.alicdn.com/kf/HTB10kAHOXXXXXc3XVXXq6xXFXXX7/New-style-belly-dance-belt-newest-multi-color-glass-silk-belly-dancing-belt-scarf-crystal-bellydance.jpg_640x640.jpg',
  },
  {
    title: 'Jiu Jitsu',
    imageUrl: 'https://res.cloudinary.com/display97/image/upload/98855.jpeg',
  },
  {
    title: 'Camping',
    imageUrl:
      'https://www.reserveamerica.com/webphotos/racms/articles/images/bca19684-d902-422d-8de2-f083e77b50ff_image2_GettyImages-677064730.jpg',
  },
  {
    title: 'Canoeing',
    imageUrl:
      'https://i.cbc.ca/1.4764103.1532699684!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/canoeing.jpg',
  },
  {
    title: 'Canyoneering',
    imageUrl:
      'http://uberadventures.net/wp-content/uploads/2017/04/Swiftwater-Canyoneering-Course.jpg',
  },
  {
    title: 'Capoeira',
    imageUrl:
      'https://cdn.cnn.com/cnnnext/dam/assets/181123095801-01-capoeira-brazil-file-restricted-super-tease.jpg',
  },
  {
    title: 'Caving',
    imageUrl: 'http://newtocaving.com/images/img-facebook.jpg',
  },
  {
    title: 'Cha Cha',
    imageUrl: 'https://usercontent2.hubstatic.com/12161217.jpg',
  },
  {
    title: 'Circuit Training',
    imageUrl:
      'https://www.fitandme.com/wp-content/uploads/2016/08/Benefits-Of-Circuit-Training-3.jpg',
  },
  {
    title: 'Climbing',
    imageUrl:
      'https://outdoorgearlab-mvnab3pwrvp3t0.stackpathdns.com/photos/15/81/279616_31645_L2.jpg',
  },
  {
    title: 'Cricket',
    imageUrl:
      'https://www.cricbuzz.com/a/img/v1/420x235/i1/c177837/england-off-to-steady-start.jpg',
  },
  {
    title: 'Cricket',
    imageUrl:
      'http://makingmusic.wpengine.com/wp-content/uploads/2014/02/Cumbia-2WEB.jpg',
  },
  {
    title: 'Curling',
    imageUrl: 'https://www.abc.net.au/news/image/10515824-3x2-700x467.jpg',
  },
  {
    title: 'Dodgeball',
    imageUrl:
      'https://www.savethechildren.org/content/dam/usa/images/events/events-teamraiser/P2P_DodgeballThrowdown_219951_sq.jpg/_jcr_content/renditions/cq5dam.thumbnail.768.768.jpg',
  },
  {
    title: 'Duathlon',
    imageUrl:
      'https://www.active.com/Assets/Triathlon/460x345/Speedy-Duathlon-460.jpg',
  },
  {
    title: 'East Coast Swing',
    imageUrl:
      'http://www.getoutanddance.com/wp-content/uploads/2018/08/swing1.png',
  },
  {
    title: 'Horseback Riding',
    imageUrl:
      'https://s24193.pcdn.co/wp-content/uploads/2016/09/benefits-of-horseback-riding-for-men-and-women-entity-1320x720.jpg',
  },
  {
    title: 'Fishing',
    imageUrl:
      'https://splnhub.cbsistatic.com/i/r/2019/07/08/2ab53ebc-0314-47d0-8426-7f99d6e7e4e1/thumbnail/640x360/df7ee52ce5363800d42b1be9f80107ba/mitch-frog-lmb-cc.jpg',
  },
  {
    title: 'Field Hockey',
    imageUrl:
      'https://neulioncs.hs.llnwd.net/pics33/800/ZK/ZKGTZYDPPEDCIJV.20181217174103.jpg',
  },
  {
    title: 'Gymnastics',
    imageUrl:
      'http://www.gymcan.org/uploads/events_images/RG_2018_CWG_TEAM_078.jpg',
  },
  {
    title: 'Handball',
    imageUrl:
      'https://ichef.bbci.co.uk/childrens-responsive-ichef-live/r/640/1x/cbbc/spt_Handball-guide-hero2.jpg',
  },
  {
    title: 'Hiking',
    imageUrl:
      'https://www.blackforest-tourism.com/var/stg/storage/images/media/belchen-wandern-1/3101310-1-ger-DE/Belchen-Wandern-1_front_large.jpg',
  },
  {
    title: 'Hunting',
    imageUrl:
      'https://vtfishandwildlife.com/sites/fishandwildlife/files/images/link_widget/hunt-fw-seminars-2col.jpg',
  },
  {
    title: 'Ice Skating',
    imageUrl:
      'https://cdn.theatlantic.com/assets/media/img/mt/2018/02/RTX17A67/lead_720_405.jpg?mod=1533691625',
  },
  {
    title: 'Judo',
    imageUrl:
      'https://cdn.cnn.com/cnnnext/dam/assets/170818132444-judo-throw-baku-2017-super-169.jpg',
  },
  {
    title: 'Karate',
    imageUrl:
      'https://healthyplanbyann.com/wp-content/uploads//2017/11/MW_Ania_KalendarzMotywacyjny_14.07.17.1275-1020x680.jpg',
  },
  {
    title: 'Kendo',
    imageUrl:
      'https://i2.wp.com/wattention.com/wp-content/uploads/2015/07/Kendo-and-the-2020-Tokyo-Olympics.jpg?fit=696%2C413&ssl=1',
  },
  {
    title: 'Kickball',
    imageUrl:
      'https://www.playworks.org/wp-content/uploads/2013/10/NorCalKickball2017_600x400.jpg',
  },
  {
    title: 'Kickboxing',
    imageUrl:
      'https://www.atimartialarts.com.au/wp-content/uploads/2018/09/ati-adult-kickboxing-450x500.jpg',
  },
  {
    title: 'Kite Surfing',
    imageUrl:
      'http://www.muchmorocco.com/wp-content/uploads/2015/02/tangiers-kite-surfing_000043649422_Large.jpg',
  },
  {
    title: 'Krav Maga',
    imageUrl:
      'http://cdn.shopify.com/s/files/1/2014/2509/articles/krav_maga_1024x1024.jpg?v=1513873105',
  },
  {
    title: 'Kung Fu',
    imageUrl: 'https://img.vimbly.com/images/full_photos/kungfu-16.jpg',
  },
  {
    title: 'Marathon',
    imageUrl:
      'https://static01.nyt.com/images/2017/10/17/science/11physed-marathon-photo/11physed-marathon-photo-articleLarge.jpg?quality=75&auto=webp&disable=upscale',
  },
  {
    title: 'Meditation',
    imageUrl:
      'https://www.verywellmind.com/thmb/7Fomll0Eohxzd-vN2DMsprTkHJc=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-938890492-becc3fc4757849bea672f148454943f9.jpg',
  },
  {
    title: 'Mountain Biking',
    imageUrl:
      'https://www.switchbacktravel.com/sites/default/files/images/landing-page/Mountain%20Bike%20%28landing%20page%29%20%28m%29.jpg',
  },
  {
    title: 'Muay Thai',
    imageUrl:
      'https://cdn.evolve-vacation.com/wp-content/uploads/2016/06/noom-kick.jpg',
  },
  {
    title: 'Netball',
    imageUrl:
      'https://images.theconversation.com/files/213056/original/file-20180404-189830-1o4o94l.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=496&fit=clip',
  },
  {
    title: 'Ninjutsu',
    imageUrl:
      'https://i0.wp.com/kungfukingdom.com/wp-content/uploads/2018/02/MAOTM-Ninjutsu-Kung-Fu-Kingdom-770x472.jpg?fit=770%2C472',
  },
  {
    title: 'Paddleboarding',
    imageUrl:
      'http://paddleboardthrills.com/wp-content/uploads/2017/02/paddleboarding-752x500.jpg',
  },
  {
    title: 'Paintball',
    imageUrl: 'https://cdn.getyourguide.com/img/tour_img-144786-148.jpg',
  },
  {
    title: 'Paso Doble',
    imageUrl:
      'https://i.pinimg.com/originals/ef/cf/85/efcf853e3bad0c108d3d430fc43e9588.jpg',
  },
  {
    title: 'Pilates',
    imageUrl:
      'https://hips.hearstapps.com/womenshealth-production.s3.amazonaws.com/images/7014/best-pilates-youtube-workouts-2__medium_4x3.jpg?resize=480:*',
  },
  {
    title: 'Polo',
    imageUrl:
      'https://www.uspolo.org/assets/img/Daily-Racing-Forms-Hilario-Ulloa-Valientes-Adolfo-Cambiaso-%C2%A9David-Lominska-WEB-1.jpg',
  },
  {
    title: 'Prenatal Fitness',
    imageUrl:
      'https://www.sciencenews.org/sites/default/files/styles/growth_curve_main/public/2018/06/main/blogposts/052518_ls_gc_yoga_feat_free.jpg?itok=mgVjrZQ8',
  },
  {
    title: 'Racquetball',
    imageUrl:
      'https://pwestpathfinder.com/wp-content/uploads/2018/01/newpicture-1-900x600.jpg',
  },
  {
    title: 'Rafting',
    imageUrl:
      'https://www.seefeld.com/images/1emj4v!khao-/wildwasser-rafting-tirol.jpeg',
  },
  {
    title: 'Rowing',
    imageUrl:
      'https://cdn3.sportngin.com/attachments/photo/1d64-116773659/Final_DSC0197__2__large.JPG',
  },
  {
    title: 'Rugby',
    imageUrl:
      'https://ichef.bbci.co.uk/onesport/cps/480/cpsprodpb/6969/production/_107758962_gettyimages-1125870390.jpg',
  },
  {
    title: 'Rumba',
    imageUrl:
      'https://i.pinimg.com/originals/03/4e/00/034e00b36135901ebb21e403ff5b3b27.jpg',
  },
  {
    title: 'Running',
    imageUrl:
      'https://d2z0k43lzfi12d.cloudfront.net/blog/vcdn269/wp-content/uploads/2018/04/thumbnail_8-tips-beginner_1200x800-1024x683.jpg',
  },
  {
    title: 'Sailing',
    imageUrl: 'https://www.sail-world.com/photos/finn/yysw247802.jpg',
  },
  {
    title: 'Salsa',
    imageUrl:
      'http://3.imimg.com/data3/ST/RY/GLADMIN-176459/salsa-dance-500x500.jpg',
  },
  {
    title: 'Samba',
    imageUrl:
      'http://www.wikidancesport.com/Attachments/dances/Samba/Samba-2-crop.jpg',
  },
  {
    title: 'Skating',
    imageUrl:
      'https://cdn2.howtostartanllc.com/images/business-ideas/business-idea-images/Roller-Skating-Rink.jpg',
  },
  {
    title: 'Skiing',
    imageUrl:
      'https://www.telegraph.co.uk/content/dam/Travel/ski/Gear/All-mountain-skis.jpg?imwidth=450',
  },
  {
    title: 'Skydiving',
    imageUrl:
      'https://www.newzealand.com/assets/Tourism-NZ/Queenstown/78cf3d031a/img-1536958564-8524-3686-p-A740C108-BEA2-6991-C42C2957D4ADD83B-2544003__FocalPointCropWzQyNyw2NDAsNDYsNTAsODUsInBuZyIsNjUsMi41XQ.png',
  },
  {
    title: 'Soccer',
    imageUrl:
      'https://neulioncs.hs.llnwd.net/pics33/800/ID/IDZWDRZLOYXRJGX.20190211191634.jpg',
  },
  {
    title: 'Softball',
    imageUrl:
      'http://a.espncdn.com/combiner/i?img=/photo/2019/0514/r543074_800x450_16-9.jpg&w=800',
  },
  {
    title: 'Squash',
    imageUrl:
      'https://cdn.washingtoncitypaper.com/files/base/scomm/wcp/image/2018/08/640w/squash_on_fire1.5b8708a0898ee.jpg',
  },
  {
    title: 'Sumo',
    imageUrl:
      'https://japan-magazine.jnto.go.jp/jnto2wm/wp-content/uploads/1312_sumo_main.jpg',
  },
  {
    title: 'Surfing',
    imageUrl:
      'http://a.espncdn.com/combiner/i?img=/photo/2018/0726/r406100_800x450_16-9.jpg&w=800',
  },
  {
    title: 'Swimming',
    imageUrl:
      'https://wpassets.trainingpeaks.com/wp-content/uploads/2017/10/10165849/10252-andy-potts-kona-swim-tips-700x394.jpg',
  },
  {
    title: 'Taekwondo',
    imageUrl:
      'https://cdn.shopify.com/s/files/1/2580/4298/articles/Karate_Training.png?v=1534341241',
  },
  {
    title: 'Tai Chi',
    imageUrl:
      'https://www.indoindians.com/wp-content/uploads/2018/12/Healing-Tai-Chi-700x405.jpg',
  },
  {
    title: 'Tai Chi',
    imageUrl:
      'https://static1.squarespace.com/static/56a810183b0be3b91e153b32/t/5ac2eb7e562fa73cd88dd81f/1522723715276/Tango+champs+2014.jpg',
  },
  {
    title: 'Tap Dancing',
    imageUrl:
      'https://i.pinimg.com/originals/b2/72/44/b272445aa0fe4291643f92ffdf1c9733.jpg',
  },
  {
    title: 'Tennis',
    imageUrl:
      'https://www.straitstimes.com/sites/default/files/styles/article_pictrure_780x520_/public/articles/2019/07/07/tennis-gbr-wimbledon-150100.jpg?itok=JOpcBMkb&timestamp=1562438565',
  },
  {
    title: 'Trail Running',
    imageUrl:
      'http://home.bt.com/images/6-reasons-to-take-up-trail-running-why-its-better-than-using-the-treadmill-136407478005303901-160721142306.jpg',
  },
  {
    title: 'Triathlon',
    imageUrl:
      'https://wpassets.trainingpeaks.com/wp-content/uploads/2017/05/08155324/04120-when-to-upgrade-your-triathlon-bike-700x394.jpg',
  },
  {
    title: 'Walking',
    imageUrl:
      'http://www.cheatsheet.com/wp-content/uploads/2015/12/Young-athlete-is-walking-along-the-embankment.jpg',
  },
  {
    title: 'Waltz',
    imageUrl:
      'https://i.pinimg.com/originals/53/88/e8/5388e8cf94e23cfc97aec2123fb11cfa.jpg',
  },
  {
    title: 'Water Polo',
    imageUrl:
      'https://images.theconversation.com/files/134054/original/image-20160815-25505-sdzh3b.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=754&fit=clip',
  },
  {
    title: 'Weight Lifting',
    imageUrl:
      'https://amp.businessinsider.com/images/5a5c995728eecc26008b4e8d-750-563.jpg',
  },
  {
    title: 'West Coast Swing',
    imageUrl:
      'https://cdn.yacsocial.com/shared/widgets/192057/website_files/images/dance-styles-swing.jpg',
  },
  {
    title: 'Wrestling',
    imageUrl:
      'https://goairforcefalcons.com/images/2019/5/17/Mossing_WTT.jpg?width=1024&height=576&mode=crop',
  },
  {
    title: 'Zumba',
    imageUrl:
      'https://d2e38eoy4d0oxc.cloudfront.net/blt/products/D0D00363/images/5da039b1-d92d-11e7-8439-0a23d6a68194-strong-by-zumba-dvd-d0d00363-product-carousel-2-medium-1524597463.jpeg',
  },
];

module.exports = eventCategories.map(attachTimestampFields);
