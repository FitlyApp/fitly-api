const eventCategories = require('./data/event-categories');

const up = (db) => {
  return db.collection('event-categories').insertMany(eventCategories);
};

const down = (db) => {
  return db.collection('event-categories').drop();
};

module.exports = {
  up,
  down,
};
