const up = async (db) => {
  await db.collection('users').updateMany({}, { $unset: { blockedAt: '' } });
  await db.collection('users').updateMany({}, { $set: { bannedAt: null } });
};

const down = async (db) => {
  await db.collection('users').updateMany({}, { $unset: { bannedAt: '' } });
  await db.collection('users').updateMany({}, { $set: { blockedAt: null } });
};

module.exports = {
  up,
  down,
};
