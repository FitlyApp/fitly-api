import {
  validate,
  isValidEmail,
  isValidUrl,
  isValidUnixMillisTimestamp,
  isValidLongitude,
  isValidLatitude,
  isValidMongoID,
} from '../../src/utilities/validation';
import { Constraints, Dictionary, ValidationResult } from '../../src/types';

describe('Utilities: Validation', () => {
  describe('validate()', () => {
    test('should accept a valid entity', () => {
      const constraints: Constraints = {
        age: {
          validationFunction: (num) => num > 20 && num < 30,
          invalidMessage: (num) => `Invalid age: ${num}`,
        },
      };

      const entity: Dictionary = {
        age: 25,
      };

      const expectedValidationResult: ValidationResult = {
        isValid: true,
        message: null,
      };

      const actualValidationResult: ValidationResult = validate(constraints)(
        entity,
      );

      expect(actualValidationResult).toEqual(expectedValidationResult);
    });

    test('should accept a valid nested entity', () => {
      const constraints: Constraints = {
        'coordinates.latitude': {
          validationFunction: (num) => num < 90 && num > -90,
          invalidMessage: (num) => `Invalid latitude: ${num}`,
        },
      };

      const entity: Dictionary = {
        coordinates: {
          latitude: 45,
        },
      };

      const expectedValidationResult: ValidationResult = {
        isValid: true,
        message: null,
      };

      const actualValidationResult: ValidationResult = validate(constraints)(
        entity,
      );

      expect(actualValidationResult).toEqual(expectedValidationResult);
    });

    test('should reject an invalid entity', () => {
      const constraints: Constraints = {
        age: {
          validationFunction: (num) => num > 20 && num < 30,
          invalidMessage: (num) => `Invalid age: ${num}`,
        },
      };

      const entity = {
        age: 35,
      };

      const expectedValidationResult = {
        isValid: false,
        message: 'Invalid age: 35',
      };

      const actualValidationResult: ValidationResult = validate(constraints)(
        entity,
      );

      expect(actualValidationResult).toEqual(expectedValidationResult);
    });

    test('should reject an ivalid nested entity', () => {
      const constraints: Constraints = {
        'coordinates.latitude': {
          validationFunction: (num) => num < 90 && num > -90,
          invalidMessage: (num) => `Invalid latitude: ${num}`,
        },
      };

      const entity: Dictionary = {
        coordinates: {
          latitude: 135,
        },
      };

      const expectedValidationResult: ValidationResult = {
        isValid: false,
        message: 'Invalid latitude: 135',
      };

      const actualValidationResult: ValidationResult = validate(constraints)(
        entity,
      );

      expect(actualValidationResult).toEqual(expectedValidationResult);
    });

    test('should ignore undefined props', () => {
      const constraints: Constraints = {
        age: {
          validationFunction: (num) => num > 20 && num < 30,
          invalidMessage: (num) => `Invalid age: ${num}`,
        },
      };

      const entity: Dictionary = {
        name: 'John Doe',
      };

      const expectedValidationResult: ValidationResult = {
        isValid: true,
        message: null,
      };

      const actualValidationResult: ValidationResult = validate(constraints)(
        entity,
      );

      expect(actualValidationResult).toEqual(expectedValidationResult);
    });
  });

  describe('isValidEmail()', () => {
    test('should accept a valid email', () => {
      const validEmail: string = 'john.doe@email.com';
      expect(isValidEmail(validEmail)).toEqual(true);
    });

    test('should reject an invalid email', () => {
      const invalidEmail: string = 'john.doe';
      expect(isValidEmail(invalidEmail)).toEqual(false);
    });
  });

  describe('isValidUrl()', () => {
    test('should accept a valid URL', () => {
      const validUrl: string = 'https://www.google.com';
      expect(isValidUrl(validUrl)).toEqual(true);
    });

    test('should reject an invalid URL', () => {
      const invalidUrl: string = 'https:/www.google.com';
      expect(isValidUrl(invalidUrl)).toEqual(false);
    });
  });

  describe('isValidUnixMillisTimestamp()', () => {
    test('should accept a valid UNIX millis timestamp', () => {
      const validTimestamp: number = 1544889978350;
      expect(isValidUnixMillisTimestamp(validTimestamp)).toEqual(true);
    });

    test('should reject an invalid UNIX millis timestamp (float)', () => {
      const invalidTimestamp: number = 1544889978350.1;
      expect(isValidUnixMillisTimestamp(invalidTimestamp)).toEqual(false);
    });

    test('should reject an invalid UNIX millis timestamp (negative)', () => {
      const invalidTimestamp: number = -1544889978350;
      expect(isValidUnixMillisTimestamp(invalidTimestamp)).toEqual(false);
    });
  });

  describe('isValidLongitude()', () => {
    test('should accept a valid longitude (eastern hemisphere)', () => {
      const validLongitude: number = 10.4515;
      expect(isValidLongitude(validLongitude)).toEqual(true);
    });

    test('should accept a valid longitude (western hemisphere)', () => {
      const validLongitude: number = -10.4515;
      expect(isValidLongitude(validLongitude)).toEqual(true);
    });

    test('should reject an invalid longitude (eastern hemisphere)', () => {
      const invalidLongitude: number = 181.1523;
      expect(isValidLongitude(invalidLongitude)).toEqual(false);
    });

    test('should reject an invalid longitude (western hemisphere)', () => {
      const invalidLongitude: number = -181.1523;
      expect(isValidLongitude(invalidLongitude)).toEqual(false);
    });
  });

  describe('isValidLatitude()', () => {
    test('should accept a valid latitude (northern hemisphere)', () => {
      const validLatitude: number = 51.1657;
      expect(isValidLatitude(validLatitude)).toEqual(true);
    });

    test('should accept a valid latitude (southern hemisphere)', () => {
      const validLatitude: number = -51.1657;
      expect(isValidLatitude(validLatitude)).toEqual(true);
    });

    test('should reject an invalid latitude (northern hemisphere)', () => {
      const invalidLatitude: number = 93.1342;
      expect(isValidLatitude(invalidLatitude)).toEqual(false);
    });

    test('should reject an invalid latitude (southern hemisphere)', () => {
      const invalidLatitude: number = -93.1342;
      expect(isValidLatitude(invalidLatitude)).toEqual(false);
    });
  });

  describe('isValidMongoID()', () => {
    test('should accept a valid Mongo ID', () => {
      const validMongoID = '53cb6b9b4f4ddef1ad47f943';
      expect(isValidMongoID(validMongoID)).toEqual(true);
    });

    test('should reject an invalid Mongo ID', () => {
      const invalidMongoID = 'notavalidid';
      expect(isValidMongoID(invalidMongoID)).toEqual(false);
    });
  });
});
