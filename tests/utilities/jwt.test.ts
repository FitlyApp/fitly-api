import { issueJWT, verifyJWT, extractJWT } from '../../src/utilities/jwt';
import { loadEnvironmentVariables, isCI } from '../../src/utilities';

describe('Utilities: JWT', () => {
  beforeAll(() => {
    if (!isCI()) {
      loadEnvironmentVariables(['JWT_SECRET']);
    }
  });

  describe('signJWT()', () => {
    test('should generate a valid JWT', async () => {
      const payload: any = {
        id: '123456',
        role: 'admin',
      };

      const jwt: string = issueJWT(payload);

      expect(jwt).toHaveLength(121);
    });
  });

  describe('verifyJWT()', () => {
    test('should verify a valid JWT', async () => {
      const payload: any = {
        id: '123456',
        role: 'admin',
      };

      const jwt: string = issueJWT(payload);

      const decodedPayload: string | object = verifyJWT(jwt);

      expect(decodedPayload).toEqual(payload);
    });
  });

  describe('extractJWT()', () => {
    test('should extract the JWT from the authorization header', () => {
      const authorizationHeader: string =
        'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViZTczZmJmOTI3ODViMmFkZDE2NTJlMSIsInJvbGUiOiJ1c2VyIn0.jVXoUrhsEHJZS3a_KaIHkSB-8mGdKa4b1ihzF8oPZW4';

      const expectedJWT: string =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViZTczZmJmOTI3ODViMmFkZDE2NTJlMSIsInJvbGUiOiJ1c2VyIn0.jVXoUrhsEHJZS3a_KaIHkSB-8mGdKa4b1ihzF8oPZW4';

      const actualJWT: string = extractJWT(authorizationHeader);

      expect(actualJWT).toEqual(expectedJWT);
    });
  });
});
