import { hashPassword, comparePasswords } from '../../src/utilities/bcrypt';

describe('Utilities: BCrypt', () => {
  describe('hashPassword()', () => {
    test('should hash the same password differently', async () => {
      const password = '123456';

      const passwordHash1: string = await hashPassword(password);
      const passwordHash2: string = await hashPassword(password);

      expect(passwordHash1).not.toEqual(passwordHash2);
    });
  });

  describe('comparePasswords()', () => {
    test('should match a password & its hash', async () => {
      const password: string = '123456';
      const passwordHash: string = await hashPassword(password);

      const isMatch = await comparePasswords(password, passwordHash);

      expect(isMatch).toEqual(true);
    });
  });
});
