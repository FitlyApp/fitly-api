import { nowMillis } from '../../src/utilities/time';

describe('nowMillis()', () => {
  test('should generate a valid UNIX milliseconds timestamp', () => {
    const timestamp: number = nowMillis();

    /* FIXME: Test does not cover entirety of possible UNIX millisecond values */
    expect(timestamp.toString()).toHaveLength(13);
  });
});
