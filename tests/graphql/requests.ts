import * as graphql from 'graphql-request';
import * as _ from 'lodash';
import {
  Dictionary,
  User,
  Event,
  PingQueryArgs,
  GetUserQueryArgs,
  GetEventQueryArgs,
  SignupUserMutationArgs,
  LoginUserMutationArgs,
  UpdateUserMutationArgs,
  DeleteUserMutationArgs,
  BanUserMutationArgs,
  FollowUserMutationArgs,
  UnfollowUserMutationArgs,
  BlockUserMutationArgs,
  UnblockUserMutationArgs,
  CreateEventMutationArgs,
  UpdateEventMutationArgs,
  DeleteEventMutationArgs,
  AttendEventMutationArgs,
  UnattendEventMutationArgs,
  LikeEventMutationArgs,
  UnlikeEventMutationArgs,
} from '../../src/types';

// TODO: Relocate
type Port = number | string;

// TODO: Relocate & test
const constructHttpUrl = ({
  host,
  port,
  path,
}: {
  host: string;
  port: Port;
  path: string;
}) => `http://${host}:${port}/${path}`;

/* Queries */

export const ping = (port: Port) => (token?: string) => async (
  args?: PingQueryArgs,
): Promise<string> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const query = `
    query {
      ping
    }  
  `;

  const response: Dictionary = await request(token)(url, query, args);

  return response.ping;
};

export const getUser = (port: Port) => (token?: string) => async (
  args?: GetUserQueryArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const query = `
    query($id: String!) {
      getUser(id: $id) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
        followers {
          id
        }
        followees {
          id
        }
        followersCount
        followeesCount
      }
    }
  `;

  const response: Dictionary = await request(token)(url, query, args);

  return response.getUser;
};

export const getEvent = (port: Port) => (token?: string) => async (
  args?: GetEventQueryArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const query = `
    query($id: String!) {
      getEvent(id: $id) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }
  `;

  const response: Dictionary = await request(token)(url, query, args);

  return response.getEvent;
};

/* Mutations */

export const signupUser = (port: Port) => (token?: string) => async (
  args: SignupUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation(
      $email: String!
      $password: String!
      $firstName: String!
      $lastName: String!
      $bio: String!
      $avatarUrl: String!
    ) {
      signupUser(
        email: $email
        password: $password
        firstName: $firstName
        lastName: $lastName
        bio: $bio
        avatarUrl: $avatarUrl
      ) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
        token
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.signupUser;
};

export const loginUser = (port: Port) => (token?: string) => async (
  args: LoginUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($email: String!, $password: String!) {
      loginUser(email: $email, password: $password) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
        token
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.loginUser;
};

export const updateUser = (port: Port) => (token?: string) => async (
  args: UpdateUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation(
      $id: String!
      $firstName: String
      $lastName: String
      $bio: String
      $avatarUrl: String
    ) {
      updateUser(
        id: $id
        firstName: $firstName
        lastName: $lastName
        bio: $bio
        avatarUrl: $avatarUrl
      ) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.updateUser;
};

export const deleteUser = (port: Port) => (token?: string) => async (
  args: DeleteUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      deleteUser(id: $id) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.deleteUser;
};

export const banUser = (port: Port) => (token?: string) => async (
  args: BanUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      banUser(id: $id) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.banUser;
};

export const followUser = (port: Port) => (token?: string) => async (
  args: FollowUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      followUser(id: $id) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
        followers {
          id
        }
        followees {
          id
        }
        followersCount
        followeesCount
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.followUser;
};

export const unfollowUser = (port: Port) => (token?: string) => async (
  args: UnfollowUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      unfollowUser(id: $id) {
        id
        email
        name {
          first
          last
        }
        role
        bio
        avatarUrl
        createdAt
        updatedAt
        deletedAt
        bannedAt
        followers {
          id
        }
        followees {
          id
        }
        followersCount
        followeesCount
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.unfollowUser;
};

export const blockUser = (port: Port) => (token?: string) => async (
  args: BlockUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      blockUser(id: $id) {
        id
        isViewerBlocking
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.blockUser;
};

export const unblockUser = (port: Port) => (token?: string) => async (
  args: UnblockUserMutationArgs,
): Promise<User> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      unblockUser(id: $id) {
        id
        isViewerBlocking
      }
    }
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.unblockUser;
};

export const createEvent = (port: Port) => (token?: string) => async (
  args?: CreateEventMutationArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation(
      $externalOriginName: String
      $externalOriginId: String
      $externalOriginUrl: String
      $title: String!
      $description: String
      $startsAt: Float!
      $endsAt: Float!
      $isPublic: Boolean!
      $costAmount: Float!
      $costCurrency: String!
      $categoryId: String
      $imageUrl: String
      $venueName: String
      $venueAddress: String
      $venueLocationType: String
      $venueLocationCoordinates: [Float]
    ) {
      createEvent(
        externalOriginName: $externalOriginName
        externalOriginId: $externalOriginId
        externalOriginUrl: $externalOriginUrl
        title: $title
        description: $description
        startsAt: $startsAt
        endsAt: $endsAt
        isPublic: $isPublic
        costAmount: $costAmount
        costCurrency: $costCurrency
        categoryId: $categoryId
        imageUrl: $imageUrl
        venueName: $venueName
        venueAddress: $venueAddress
        venueLocationType: $venueLocationType
        venueLocationCoordinates: $venueLocationCoordinates
      ) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }  
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.createEvent;
};

export const updateEvent = (port: Port) => (token?: string) => async (
  args?: UpdateEventMutationArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation(
      $id: String!
      $title: String
      $description: String
      $startsAt: Float
      $endsAt: Float
      $isPublic: Boolean
      $costAmount: Float
      $costCurrency: String
      $categoryId: String
      $imageUrl: String
      $venueName: String
      $venueAddress: String
      $venueLocationType: String
      $venueLocationCoordinates: [Float]
    ) {
      updateEvent(
        id: $id
        title: $title
        description: $description
        startsAt: $startsAt
        endsAt: $endsAt
        isPublic: $isPublic
        costAmount: $costAmount
        costCurrency: $costCurrency
        categoryId: $categoryId
        imageUrl: $imageUrl
        venueName: $venueName
        venueAddress: $venueAddress
        venueLocationType: $venueLocationType
        venueLocationCoordinates: $venueLocationCoordinates
      ) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }  
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.updateEvent;
};

export const deleteEvent = (port: Port) => (token?: string) => async (
  args?: DeleteEventMutationArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      deleteEvent(id: $id) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }  
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.deleteEvent;
};

export const attendEvent = (port: Port) => (token?: string) => async (
  args?: AttendEventMutationArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      attendEvent(id: $id) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }  
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.attendEvent;
};

export const unattendEvent = (port: Port) => (token?: string) => async (
  args?: UnattendEventMutationArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      unattendEvent(id: $id) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }  
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.unattendEvent;
};

export const likeEvent = (port: Port) => (token?: string) => async (
  args?: LikeEventMutationArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      likeEvent(id: $id) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }  
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.likeEvent;
};

export const unlikeEvent = (port: Port) => (token?: string) => async (
  args?: UnlikeEventMutationArgs,
): Promise<Event> => {
  const url = constructHttpUrl({
    host: 'localhost',
    port,
    path: 'graphql',
  });

  const mutation = `
    mutation($id: String!) {
      unlikeEvent(id: $id) {
        id
        externalOrigin {
          name
          id
          url
        }
        title
        description
        startsAt
        endsAt
        isPublic
        cost {
          amount
          currency
        }
        categoryId
        imageUrl
        venue {
          name
          address
          location {
            type
            coordinates
          }
        }
        createdAt
        updatedAt
        deletedAt
        hosts {
          id
        }
        externalHosts {
          fullName
        }
        attendees {
          id
        }
        externalAttendees {
          fullName
        }
        likers {
          id
        }
        isViewerHosting
        isViewerAttending
        isViewerLiking
      }
    }  
  `;

  const response: Dictionary = await request(token)(url, mutation, args);

  return response.unlikeEvent;
};

/* Helpers */

export const signupDummyUser = (port: Port) => (
  index?: number,
): Promise<User> => {
  const dummyUsers: SignupUserMutationArgs[] = [
    {
      email: 'john.doe@email.com',
      password: '123456',
      firstName: 'John',
      lastName: 'Doe',
      bio: 'Hello world',
      avatarUrl: 'https://bit.ly/2OWXR5x',
    },
    {
      email: 'jane.doe@email.com',
      password: '123456',
      firstName: 'Jane',
      lastName: 'Doe',
      bio: 'Foo baz',
      avatarUrl: 'https://bit.ly/2zA0Mwe',
    },
  ];

  if (_.isUndefined(index)) {
    return signupUser(port)()(dummyUsers[0]);
  }

  return signupUser(port)()(dummyUsers[index]);
};

export const createDummyEvent = (port: Port) => (token: string) => (
  index?: number,
): Promise<Event> => {
  const dummyEvents: CreateEventMutationArgs[] = [
    {
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    },
  ];

  if (_.isUndefined(index)) {
    return createEvent(port)(token)(dummyEvents[0]);
  }

  return createEvent(port)(token)(dummyEvents[index]);
};

/* Networking */

export const request = (token?: string) => (
  url: string,
  query: string,
  variables?: Dictionary,
): Promise<Dictionary> => {
  if (_.isUndefined(token)) {
    return graphql.request(url, query, variables);
  }

  return new graphql.GraphQLClient(url, {
    headers: { Authorization: `Bearer ${token}` },
  }).request(query, variables);
};
