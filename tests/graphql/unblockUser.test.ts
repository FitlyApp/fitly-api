import { signupDummyUser, unblockUser, blockUser, getUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: unblockUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should unblock the user with the provided ID', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    await blockUser(server.options.port)(user0.token)({ id: user1.id });

    const unblockUserResponse: User = await unblockUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    expect(unblockUserResponse.id).toEqual(user1.id);
    expect(unblockUserResponse.isViewerBlocking).toEqual(false);
  });

  test('should throw when a redundant unblock is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    await blockUser(server.options.port)(user0.token)({ id: user1.id });
    await unblockUser(server.options.port)(user0.token)({ id: user1.id });

    const unblockUserRequest: Promise<User> = unblockUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    await expect(unblockUserRequest).rejects.toThrow();
  });

  test('should throw when an unfounded unblock is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const unblockUserRequest: Promise<User> = unblockUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    await expect(unblockUserRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const unblockUserRequest: Promise<User> = unblockUser(
      server.options.port,
    )()({
      id: user1.id,
    });

    await expect(unblockUserRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const unblockUserRequest: Promise<User> = unblockUser(server.options.port)(
      user0.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(unblockUserRequest).rejects.toThrow();
  });
});
