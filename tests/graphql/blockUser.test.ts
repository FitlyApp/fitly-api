import { signupDummyUser, blockUser, getUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: blockUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should block the user with the provided ID', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const blockUserResponse: User = await blockUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    expect(blockUserResponse.id).toEqual(user1.id);
    expect(blockUserResponse.isViewerBlocking).toEqual(true);
  });

  test('should throw when a redundant block is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    await blockUser(server.options.port)(user0.token)({ id: user1.id });

    const blockUserRequest: Promise<User> = blockUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    await expect(blockUserRequest).rejects.toThrow();
  });

  test('should throw when a self-block is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const blockUserRequest: Promise<User> = blockUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
    });

    await expect(blockUserRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const blockUserRequest: Promise<User> = blockUser(server.options.port)()({
      id: user1.id,
    });

    await expect(blockUserRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const blockUserRequest: Promise<User> = blockUser(server.options.port)(
      user0.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(blockUserRequest).rejects.toThrow();
  });
});
