import {
  signupDummyUser,
  createDummyEvent,
  unlikeEvent,
  likeEvent,
} from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: unlikeEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should unlike the event with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    await likeEvent(server.options.port)(viewer.token)({ id: event.id });

    const unlikeEventResponse: Event = await unlikeEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    expect(unlikeEventResponse.id).toEqual(event.id);
    expect(unlikeEventResponse.likers).toEqual([]);
    expect(unlikeEventResponse.isViewerLiking).toEqual(false);
  });

  test('should throw when a redundant unlike is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    await likeEvent(server.options.port)(viewer.token)({ id: event.id });
    await unlikeEvent(server.options.port)(viewer.token)({ id: event.id });

    const unlikeEventRequest: Promise<Event> = unlikeEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    await expect(unlikeEventRequest).rejects.toThrow();
  });

  test('should throw when an unfounded unlike is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const unlikeEventRequest: Promise<Event> = unlikeEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    await expect(unlikeEventRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const unlikeEventRequest: Promise<Event> = unlikeEvent(
      server.options.port,
    )()({
      id: event.id,
    });

    await expect(unlikeEventRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const unlikeEventRequest: Promise<Event> = unlikeEvent(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(unlikeEventRequest).rejects.toThrow();
  });
});
