import { signupDummyUser, createDummyEvent, deleteEvent } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: deleteEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should delete the event with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
    };

    const actualData: Event = await deleteEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.deletedAt).not.toBeNull();
  });

  test('should throw when a redundant delete is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    await deleteEvent(server.options.port)(viewer.token)({ id: event.id });

    const deleteEventRequest: Promise<Event> = deleteEvent(server.options.port)(
      viewer.token,
    )({ id: event.id });

    await expect(deleteEventRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const deleteEventRequest: Promise<Event> = deleteEvent(
      server.options.port,
    )()({ id: event.id });

    await expect(deleteEventRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const deleteEventRequest: Promise<Event> = deleteEvent(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(deleteEventRequest).rejects.toThrow();
  });

  test('should throw when an illegal delete is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const event: Event = await createDummyEvent(server.options.port)(
      user0.token,
    )();

    const deleteEventRequest: Promise<User> = deleteEvent(server.options.port)(
      user1.token,
    )({
      id: event.id,
    });

    await expect(deleteEventRequest).rejects.toThrow();
  });
});
