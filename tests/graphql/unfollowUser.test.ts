import { signupDummyUser, unfollowUser, followUser, getUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: unfollowUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should unfollow the user with the provided ID', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    await followUser(server.options.port)(user0.token)({ id: user1.id });

    const unfollowUserResponse: User = await unfollowUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    expect(unfollowUserResponse.id).toEqual(user1.id);

    expect(unfollowUserResponse.followers).toEqual([]);
    expect(unfollowUserResponse.followersCount).toEqual(0);

    expect(unfollowUserResponse.followees).toEqual([]);
    expect(unfollowUserResponse.followeesCount).toEqual(0);

    const getUserResponse: User = await getUser(server.options.port)(
      user0.token,
    )({ id: user0.id });

    expect(getUserResponse.followers).toEqual([]);
    expect(getUserResponse.followersCount).toEqual(0);

    expect(getUserResponse.followees).toEqual([]);
    expect(getUserResponse.followeesCount).toEqual(0);
  });

  test('should throw when a redundant unfollow is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    await followUser(server.options.port)(user0.token)({ id: user1.id });
    await unfollowUser(server.options.port)(user0.token)({ id: user1.id });

    const unfollowUserRequest: Promise<User> = unfollowUser(
      server.options.port,
    )(user0.token)({
      id: user1.id,
    });

    await expect(unfollowUserRequest).rejects.toThrow();
  });

  test('should throw when an unfounded unfollow is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const unfollowUserRequest: Promise<User> = unfollowUser(
      server.options.port,
    )(user0.token)({
      id: user1.id,
    });

    await expect(unfollowUserRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const unfollowUserRequest: Promise<User> = unfollowUser(
      server.options.port,
    )()({
      id: user1.id,
    });

    await expect(unfollowUserRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const unfollowUserRequest: Promise<User> = unfollowUser(
      server.options.port,
    )(user0.token)({
      id: 'a1b2c3d4',
    });

    await expect(unfollowUserRequest).rejects.toThrow();
  });
});
