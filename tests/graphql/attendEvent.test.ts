import { signupDummyUser, createDummyEvent, attendEvent } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: attendEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should attend the event with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const attendEventResponse: Event = await attendEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    expect(attendEventResponse.id).toEqual(event.id);

    expect(attendEventResponse.attendees).toHaveLength(1);
    expect(attendEventResponse.attendees[0]).toMatchObject({ id: viewer.id });

    expect(attendEventResponse.isViewerAttending).toEqual(true);
  });

  test('should throw when a redundant attend is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    await attendEvent(server.options.port)(viewer.token)({
      id: event.id,
    });

    const attendEventRequest: Promise<Event> = attendEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    await expect(attendEventRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const attendEventRequest: Promise<Event> = attendEvent(
      server.options.port,
    )()({ id: event.id });

    await expect(attendEventRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const attendEventRequest: Promise<Event> = attendEvent(server.options.port)(
      viewer.token,
    )({ id: 'a1b2c3d4' });

    await expect(attendEventRequest).rejects.toThrow();
  });
});
