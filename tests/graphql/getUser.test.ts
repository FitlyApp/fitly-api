import { signupDummyUser, getUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: getUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should get the user with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: User = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: 'Doe',
      },
    };

    const actualData: User = await getUser(server.options.port)(viewer.token)({
      id: viewer.id,
    });

    expect(actualData).toMatchObject(expectedData);
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const request: Promise<User> = getUser(server.options.port)()({
      id: viewer.id,
    });

    await expect(request).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const getUserRequest: Promise<User> = getUser(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(getUserRequest).rejects.toThrow();
  });
});
