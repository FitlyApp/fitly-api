import { signupUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: signupUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should sign up a new user', async () => {
    const expectedData: User = {
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: 'Doe',
      },
    };

    const actualData: User = await signupUser(server.options.port)()({
      email: 'john.doe@email.com',
      password: '123456',
      firstName: 'John',
      lastName: 'Doe',
      bio: 'Hello world',
      avatarUrl: 'https://bit.ly/2OWXR5x',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.id).not.toBeNull();
    expect(actualData.token).not.toBeNull();
    expect(actualData.createdAt).not.toBeNull();
    expect(actualData.updatedAt).toBeNull();
    expect(actualData.deletedAt).toBeNull();
    expect(actualData.bannedAt).toBeNull();
  });

  test('should not signup a new user with an invalid email', async () => {
    const signupUserRequest: Promise<User> = signupUser(server.options.port)()({
      email: 'john.doe',
      password: '123456',
      firstName: 'John',
      lastName: 'Doe',
      bio: 'Hello world',
      avatarUrl: 'https://bit.ly/2OWXR5x',
    });

    await expect(signupUserRequest).rejects.toThrow();
  });

  test('should not signup a new user with an invalid password', async () => {
    const signupUserRequest: Promise<User> = signupUser(server.options.port)()({
      email: 'john.doe@email.com',
      password: '123',
      firstName: 'John',
      lastName: 'Doe',
      bio: 'Hello world',
      avatarUrl: 'https://bit.ly/2OWXR5x',
    });

    await expect(signupUserRequest).rejects.toThrow();
  });

  test('should not signup a new user with an invalid avatar URL', async () => {
    const signupUserRequest: Promise<User> = signupUser(server.options.port)()({
      email: 'john.doe@email.com',
      password: '123456',
      firstName: 'John',
      lastName: 'Doe',
      bio: 'Hello world',
      avatarUrl: 'https:/bit.ly/2OWXR5x',
    });

    await expect(signupUserRequest).rejects.toThrow();
  });
});
