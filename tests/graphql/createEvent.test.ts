import { signupDummyUser, createEvent } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: createEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should create a new event', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: Event = {
      externalOrigin: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      cost: {
        amount: 0,
        currency: 'USD',
      },
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venue: {
        name: 'Ocean Beach',
        address: 'Ocean Beach, San Francisco, CA, USA',
        location: {
          type: 'Point',
          coordinates: [-122.525622, 37.7525655],
        },
      },
      hosts: [{ id: viewer.id }],
      attendees: [],
      likers: [],
      isViewerHosting: true,
      isViewerAttending: false,
      isViewerLiking: false,
    };

    const actualData: Event = await createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.id).not.toBeNull();
    expect(actualData.createdAt).not.toBeNull();
    expect(actualData.updatedAt).toBeNull();
    expect(actualData.deletedAt).toBeNull();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(
      server.options.port,
    )()({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid external origin name', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: 'dfcbmqyxqbtb',
      externalOriginName: 'TWITTER',
      externalOriginUrl: 'https://bit.ly/2S84bKa',
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid external origin URL', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: 'dfcbmqyxqbtb',
      externalOriginName: 'MEETUP',
      externalOriginUrl: 'https//bit.ly/2S84bKa',
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid starting time', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: -1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid ending time', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: -1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with invalid starting & ending times', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543629600000,
      endsAt: 1543622400000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid cost amount', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: -1,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid cost currency', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'EGP',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid category ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: 'HITCHHIKING',
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with an invalid image URL', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https//bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with invalid location coordinates (longitude)', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-181.525622, 37.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });

  test('should not create a new event with invalid location coordinates (latitude)', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const createEventRequest: Promise<Event> = createEvent(server.options.port)(
      viewer.token,
    )({
      externalOriginId: null,
      externalOriginName: null,
      externalOriginUrl: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      costAmount: 0,
      costCurrency: 'USD',
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venueName: 'Ocean Beach',
      venueAddress: 'Ocean Beach, San Francisco, CA, USA',
      venueLocationType: 'Point',
      venueLocationCoordinates: [-122.525622, 91.7525655],
    });

    await expect(createEventRequest).rejects.toThrow();
  });
});
