import { signupDummyUser, deleteUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: deleteUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should delete the user with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: 'Doe',
      },
    };

    const actualData: User = await deleteUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.createdAt).not.toBeNull();
    expect(actualData.updatedAt).not.toBeNull();
    expect(actualData.deletedAt).not.toBeNull();
    expect(actualData.bannedAt).toBeNull();
  });

  test('should throw when a redundant delete is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    await deleteUser(server.options.port)(viewer.token)({ id: viewer.id });

    const deleteUserRequest: Promise<User> = deleteUser(server.options.port)(
      viewer.token,
    )({ id: viewer.id });

    await expect(deleteUserRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const deleteUserRequest: Promise<User> = deleteUser(server.options.port)()({
      id: viewer.id,
    });

    await expect(deleteUserRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const deleteUserRequest: Promise<User> = deleteUser(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(deleteUserRequest).rejects.toThrow();
  });

  test('should throw when an illegal delete is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const updateUserRequest: Promise<User> = deleteUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    await expect(updateUserRequest).rejects.toThrow();
  });
});
