import {
  startTestingMongoDBConnection,
  closeTestingMongoDBConnection,
  dropMongoDBDatabase,
} from '../../src/mongodb';
import {
  startTestingGraphQLServer,
  closeTestingGraphQLServer,
} from '../../src/server';
import { loadEnvironmentVariables, isCI } from '../../src/utilities';
import { Server, Database, DatabaseServer } from '../../src/types';

export const beforeAllHook = async (): Promise<
  [Server, Database, DatabaseServer]
> => {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

  if (!isCI()) {
    loadEnvironmentVariables(['JWT_SECRET']);
  }

  const [database, databaseServer]: [
    Database,
    DatabaseServer,
  ] = await startTestingMongoDBConnection();

  const server: Server = await startTestingGraphQLServer();

  await dropMongoDBDatabase(database);

  return [server, database, databaseServer];
};

export const afterAllHook = async (
  server: Server,
  database: Database,
  databaseServer: DatabaseServer,
): Promise<void> => {
  await closeTestingMongoDBConnection(database, databaseServer);
  await closeTestingGraphQLServer(server);
};

export const beforeEachHook = async (database: Database): Promise<void> => {
  await dropMongoDBDatabase(database);
};
