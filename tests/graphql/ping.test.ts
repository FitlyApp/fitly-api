import { ping } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer } from '../../src/types';

describe('GraphQL Server: ping()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should respond to the ping with a pong', async () => {
    const expectedData: string = 'pong';
    const actualData: string = await ping(server.options.port)()();

    expect(actualData).toEqual(expectedData);
  });
});
