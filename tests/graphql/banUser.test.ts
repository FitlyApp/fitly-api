import { signupDummyUser, loginUser, banUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: banUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should ban the user with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: User = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: 'Doe',
      },
    };

    const actualData: User = await banUser(server.options.port)(viewer.token)({
      id: viewer.id,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.createdAt).not.toBeNull();
    expect(actualData.updatedAt).not.toBeNull();
    expect(actualData.deletedAt).toBeNull();
    expect(actualData.bannedAt).not.toBeNull();
  });

  test('should throw when a redundant ban is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    await banUser(server.options.port)(viewer.token)({ id: viewer.id });

    const banUserRequest: Promise<User> = banUser(server.options.port)(
      viewer.token,
    )({ id: viewer.id });

    await expect(banUserRequest).rejects.toThrow();
  });

  test('should throw when no viewer.token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const banUserRequest: Promise<User> = banUser(server.options.port)()({
      id: viewer.id,
    });

    await expect(banUserRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const banUserRequest: Promise<User> = banUser(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(banUserRequest).rejects.toThrow();
  });

  test('should prevent the user with the provided ID from logging in', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    await banUser(server.options.port)(viewer.token)({ id: viewer.id });

    const loginRequest: Promise<User> = loginUser(server.options.port)(
      viewer.token,
    )({
      email: 'john.doe@email.com',
      password: '123456',
    });

    await expect(loginRequest).rejects.toThrow();
  });
});
