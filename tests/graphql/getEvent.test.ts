import { signupDummyUser, createDummyEvent, getEvent } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: getEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should get the event with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      externalOrigin: null,
      title: 'Yoga on the Beach',
      description: 'Yoga is amazing',
      startsAt: 1543622400000,
      endsAt: 1543629600000,
      isPublic: true,
      cost: {
        amount: 0,
        currency: 'USD',
      },
      categoryId: null,
      imageUrl: 'https://bit.ly/2SNXJb2',
      venue: {
        name: 'Ocean Beach',
        address: 'Ocean Beach, San Francisco, CA, USA',
        location: {
          type: 'Point',
          coordinates: [-122.525622, 37.7525655],
        },
      },
      hosts: [{ id: viewer.id }],
      attendees: [],
      likers: [],
      isViewerHosting: true,
      isViewerAttending: false,
      isViewerLiking: false,
    };

    const actualData: Event = await getEvent(server.options.port)(
      viewer.token,
    )({ id: event.id });

    expect(actualData).toMatchObject(expectedData);
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const getEventRequest: Promise<Event> = getEvent(server.options.port)()({
      id: event.id,
    });

    await expect(getEventRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const getEventRequest: Promise<Event> = getEvent(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(getEventRequest).rejects.toThrow();
  });
});
