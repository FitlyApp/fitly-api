import { signupDummyUser, loginUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: loginUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should log in an existing user', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: User = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: 'Doe',
      },
    };

    const actualData: User = await loginUser(server.options.port)()({
      email: 'john.doe@email.com',
      password: '123456',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.token).not.toBeNull();
  });

  test('should not log in a user with an invalid email', async () => {
    await signupDummyUser(server.options.port)();

    const loginUserRequest: Promise<User> = loginUser(server.options.port)()({
      email: 'john.doe',
      password: '123456',
    });

    await expect(loginUserRequest).rejects.toThrow();
  });

  test('should not log in a user with a nonexistent email', async () => {
    await signupDummyUser(server.options.port)();

    const loginUserRequest: Promise<User> = loginUser(server.options.port)()({
      email: 'jane.doe@email.com',
      password: '123456',
    });

    await expect(loginUserRequest).rejects.toThrow();
  });

  test('should not log in a user with an incorrect password', async () => {
    await signupDummyUser(server.options.port)();

    const loginUserRequest: Promise<User> = loginUser(server.options.port)()({
      email: 'john.doe@email.com',
      password: 'abcde',
    });

    await expect(loginUserRequest).rejects.toThrow();
  });
});
