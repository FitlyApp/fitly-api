import { signupDummyUser, updateUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: updateUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test(`should update the selected user's first name`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: User = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'Jane',
        last: 'Doe',
      },
    };

    const actualData: User = await updateUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
      firstName: 'Jane',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected user's last name`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: User = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: null,
      },
    };

    const actualData: User = await updateUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
      lastName: null,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected user's bio`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: User = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: 'Doe',
      },
      bio: 'Screw the world',
    };

    const actualData: User = await updateUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
      bio: 'Screw the world',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected user's avatar URL`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const expectedData: User = {
      id: viewer.id,
      email: 'john.doe@email.com',
      name: {
        first: 'John',
        last: 'Doe',
      },
      avatarUrl: 'https://bit.ly/2P2WWRg',
    };

    const actualData: User = await updateUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
      avatarUrl: 'https://bit.ly/2P2WWRg',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const updateUserRequest: Promise<User> = updateUser(server.options.port)()({
      id: viewer.id,
      firstName: 'Jane',
    });

    await expect(updateUserRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const updateUserRequest: Promise<User> = updateUser(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
      firstName: 'Jane',
    });

    await expect(updateUserRequest).rejects.toThrow();
  });

  test('should throw when an s update is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const updateUserRequest: Promise<User> = updateUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
      firstName: 'Jane',
    });

    await expect(updateUserRequest).rejects.toThrow();
  });

  test('should not update the user with an invalid avatar URL', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const updateUserRequest: Promise<User> = updateUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
      avatarUrl: 'https:/bit.ly/2OWXR5x',
    });

    await expect(updateUserRequest).rejects.toThrow();
  });
});
