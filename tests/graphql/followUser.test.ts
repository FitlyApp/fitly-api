import { signupDummyUser, followUser, getUser } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User } from '../../src/types';

describe('GraphQL Server: followUser()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should follow the user with the provided ID', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const followUserResponse: User = await followUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    expect(followUserResponse.id).toEqual(user1.id);

    expect(followUserResponse.followers).toHaveLength(1);
    expect(followUserResponse.followers[0]).toMatchObject({ id: user0.id });
    expect(followUserResponse.followersCount).toEqual(1);

    expect(followUserResponse.followees).toEqual([]);
    expect(followUserResponse.followeesCount).toEqual(0);

    const getUserResponse: User = await getUser(server.options.port)(
      user0.token,
    )({ id: user0.id });

    expect(getUserResponse.followers).toEqual([]);
    expect(getUserResponse.followersCount).toEqual(0);

    expect(getUserResponse.followees).toHaveLength(1);
    expect(getUserResponse.followees[0]).toMatchObject({ id: user1.id });
    expect(getUserResponse.followeesCount).toEqual(1);
  });

  test('should throw when a redundant follow is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    await followUser(server.options.port)(user0.token)({ id: user1.id });

    const followUserRequest: Promise<User> = followUser(server.options.port)(
      user0.token,
    )({
      id: user1.id,
    });

    await expect(followUserRequest).rejects.toThrow();
  });

  test('should throw when a self-follow is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const followUserRequest: Promise<User> = followUser(server.options.port)(
      viewer.token,
    )({
      id: viewer.id,
    });

    await expect(followUserRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const followUserRequest: Promise<User> = followUser(server.options.port)()({
      id: user1.id,
    });

    await expect(followUserRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const followUserRequest: Promise<User> = followUser(server.options.port)(
      user0.token,
    )({
      id: 'a1b2c3d4',
    });

    await expect(followUserRequest).rejects.toThrow();
  });
});
