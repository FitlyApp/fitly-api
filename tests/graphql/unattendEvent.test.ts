import {
  signupDummyUser,
  createDummyEvent,
  unattendEvent,
  attendEvent,
} from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: unattendEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should unattend the event with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    await attendEvent(server.options.port)(viewer.token)({ id: event.id });

    const unattendEventResponse: Event = await unattendEvent(
      server.options.port,
    )(viewer.token)({
      id: event.id,
    });

    expect(unattendEventResponse.id).toEqual(event.id);
    expect(unattendEventResponse.attendees).toEqual([]);
    expect(unattendEventResponse.isViewerAttending).toEqual(false);
  });

  test('should throw when a redundant unattend is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    await attendEvent(server.options.port)(viewer.token)({ id: event.id });
    await unattendEvent(server.options.port)(viewer.token)({ id: event.id });

    const unattendEventRequest: Promise<Event> = unattendEvent(
      server.options.port,
    )(viewer.token)({
      id: event.id,
    });

    await expect(unattendEventRequest).rejects.toThrow();
  });

  test('should throw when an unfounded unattend is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const unattendEventRequest: Promise<Event> = unattendEvent(
      server.options.port,
    )(viewer.token)({
      id: event.id,
    });

    await expect(unattendEventRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const unattendEventRequest: Promise<Event> = unattendEvent(
      server.options.port,
    )()({
      id: event.id,
    });

    await expect(unattendEventRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const unattendEventRequest: Promise<Event> = unattendEvent(
      server.options.port,
    )(viewer.token)({
      id: 'a1b2c3d4',
    });

    await expect(unattendEventRequest).rejects.toThrow();
  });
});
