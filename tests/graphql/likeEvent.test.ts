import { signupDummyUser, createDummyEvent, likeEvent } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: likeEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test('should like the event with the provided ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const likeEventResponse: Event = await likeEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    expect(likeEventResponse.id).toEqual(event.id);

    expect(likeEventResponse.likers).toHaveLength(1);
    expect(likeEventResponse.likers[0]).toMatchObject({ id: viewer.id });

    expect(likeEventResponse.isViewerLiking).toEqual(true);
  });

  test('should throw when a redundant like is attempted', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    await likeEvent(server.options.port)(viewer.token)({
      id: event.id,
    });

    const likeEventRequest: Promise<Event> = likeEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
    });

    await expect(likeEventRequest).rejects.toThrow();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const likeEventRequest: Promise<Event> = likeEvent(server.options.port)()({
      id: event.id,
    });

    await expect(likeEventRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const likeEventRequest: Promise<Event> = likeEvent(server.options.port)(
      viewer.token,
    )({ id: 'a1b2c3d4' });

    await expect(likeEventRequest).rejects.toThrow();
  });
});
