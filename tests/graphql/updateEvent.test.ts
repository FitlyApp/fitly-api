import { signupDummyUser, createDummyEvent, updateEvent } from './requests';
import { beforeAllHook, afterAllHook, beforeEachHook } from './hooks';
import { Server, Database, DatabaseServer, User, Event } from '../../src/types';

describe('GraphQL Server: updateEvent()', () => {
  let server: Server;
  let database: Database;
  let databaseServer: DatabaseServer;

  beforeAll(async () => {
    const [
      testingServer,
      testingDatabase,
      testingDatabaseServer,
    ] = await beforeAllHook();

    server = testingServer;
    database = testingDatabase;
    databaseServer = testingDatabaseServer;
  });

  afterAll(async () => {
    await afterAllHook(server, database, databaseServer);
  });

  beforeEach(async () => {
    await beforeEachHook(database);
  });

  test(`should update the selected event's title`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      title: 'Jogging in the Wild',
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      title: 'Jogging in the Wild',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's description`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      description: 'Jogging is amazing',
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      description: 'Jogging is amazing',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's starting time`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      startsAt: 1543629600000,
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      startsAt: 1543629600000,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's ending time`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      endsAt: 1543636800000,
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      endsAt: 1543636800000,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's publicity setting`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      isPublic: false,
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      isPublic: false,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's cost amount`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      cost: {
        amount: 2,
        currency: 'USD',
      },
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      costAmount: 2,
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  // test(`should update the selected event's cost currency`, async () => {
  //   const viewer: User = await signupDummyUser(server.options.port)();

  //   const event: Event = await createDummyEvent(server.options.port)(
  //     viewer.token,
  //   )();

  //   const expectedData: Event = {
  //     id: event.id,
  //     cost: {
  //       currency: 'EUR',
  //     },
  //   };

  //   const actualData: Event = await updateEvent(server.options.port)(
  //     viewer.token,
  //   )({
  //     id: event.id,
  //     costCurrency: 'EUR',
  //   });

  //   expect(actualData).toMatchObject(expectedData);
  //   expect(actualData.updatedAt).not.toBeNull();
  // });

  // test(`should update the selected event's category`, async () => {
  //   const viewer: User = await signupDummyUser(server.options.port)();

  //   const event: Event = await createDummyEvent(server.options.port)(
  //     viewer.token,
  //   )();

  //   const expectedData: Event = {
  //     id: event.id,
  //     category: 'AEROBICS',
  //   };

  //   const actualData: Event = await updateEvent(server.options.port)(
  //     viewer.token,
  //   )({
  //     id: event.id,
  //     category: 'AEROBICS',
  //   });

  //   expect(actualData).toMatchObject(expectedData);
  //   expect(actualData.updatedAt).not.toBeNull();
  // });

  test(`should update the selected event's image URL`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      imageUrl: 'https://bit.ly/2GTrVR8',
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      imageUrl: 'https://bit.ly/2GTrVR8',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's venue's name`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      venue: {
        name: 'Pier 33',
      },
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      venueName: 'Pier 33',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's venue's address`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      venue: {
        address: 'Embarcadero, San Francisco, CA 94133, USA',
      },
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      venueAddress: 'Embarcadero, San Francisco, CA 94133, USA',
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test(`should update the selected event's venue's address`, async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const expectedData: Event = {
      id: event.id,
      venue: {
        location: {
          coordinates: [-122.4035822, 37.806971],
        },
      },
    };

    const actualData: Event = await updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      venueLocationCoordinates: [-122.4035822, 37.806971],
    });

    expect(actualData).toMatchObject(expectedData);
    expect(actualData.updatedAt).not.toBeNull();
  });

  test('should throw when no token is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(
      server.options.port,
    )()({
      id: event.id,
      title: 'Jogging in the Wild',
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should throw when an invalid ID is provided', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: 'a1b2c3d4',
      title: 'Jogging in the Wild',
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should throw when an illegal update is attempted', async () => {
    const user0: User = await signupDummyUser(server.options.port)(0);
    const user1: User = await signupDummyUser(server.options.port)(1);

    const event: Event = await createDummyEvent(server.options.port)(
      user0.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      user1.token,
    )({
      id: event.id,
      title: 'Jogging in the Wild',
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with an invalid starting time', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      startsAt: -1543622400000,
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with an invalid ending time', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      endsAt: -1543622400000,
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with invalid starting & ending times', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      startsAt: 1543629600000,
      endsAt: 1543622400000,
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with an invalid cost amount', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      costAmount: -1,
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with an invalid cost currency', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      costCurrency: 'EGP',
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with an invalid category ID', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      categoryId: 'HITCHHIKING',
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with an invalid image URL', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      imageUrl: 'https//bit.ly/2SNXJb2',
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with invalid location coordinates (longitude)', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      venueLocationCoordinates: [-181.525622, 37.7525655],
    });

    await expect(updateEventRequest).rejects.toThrow();
  });

  test('should not update the event with invalid location coordinates (latitude)', async () => {
    const viewer: User = await signupDummyUser(server.options.port)();

    const event: Event = await createDummyEvent(server.options.port)(
      viewer.token,
    )();

    const updateEventRequest: Promise<Event> = updateEvent(server.options.port)(
      viewer.token,
    )({
      id: event.id,
      venueLocationCoordinates: [-122.525622, 91.7525655],
    });

    await expect(updateEventRequest).rejects.toThrow();
  });
});
