import {
  getMatchingRequest,
  searchMatchingRequests,
  createMatchingRequest,
  updateMatchingRequest,
} from '../../../services/partner-matching';
import { ConflictError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  FindMatchMutationArgs,
  MatchingRequest,
} from './../../../types';

// TODO: Test
const findMatchResolver: Resolver = async (
  parent: Dictionary,
  args: FindMatchMutationArgs,
  context: Context,
): Promise<MatchingRequest> => {
  const { coordinates, workoutType, activityLevel } = args;
  const { viewerId, pubsub } = context;

  /* Get any existing pending matching request by viewer */
  const existingMatchingRequest = await getMatchingRequest({
    requesterId: viewerId,
    status: 'PENDING',
  });

  /* If viewer already has a pending matching request, throttle him */
  if (existingMatchingRequest) {
    throw new ConflictError({
      data: {
        developerMessage: 'Viewer already has a pending matching request',
        userMessage: 'You already have a pending matching request',
      },
    });
  }

  /* Search pending matching requests in viewer's vicinity */
  const [nearestMatchingRequest] = await searchMatchingRequests({
    requesterId: viewerId,
    coordinates,
    workoutType,
    activityLevel,
    maximumDistanceMeters: 25 * 1000,
    status: 'PENDING',
  });

  /* If an existing matching request was found right away */
  if (nearestMatchingRequest) {
    /**
     *  Match viewer with the existing matching request,
     *  and change status from pending to complete.
     */
    const successfulMatchingRequest = await updateMatchingRequest(
      { id: nearestMatchingRequest.id },
      { accepterId: viewerId, status: 'COMPLETE' },
    );

    /* Notify subscribers about the successful match */
    pubsub.publish('PARTNER_MATCHING', {
      matchFound: successfulMatchingRequest,
    });

    return successfulMatchingRequest;
  }

  /* If a matching request was not found, create a new one */
  const createdMatchingRequest = await createMatchingRequest({
    requesterId: viewerId,
    location: {
      type: 'Point',
      coordinates,
    },
    workoutType,
    activityLevel,
    status: 'PENDING',
  });

  /* After one minute: */
  setTimeout(async () => {
    const recentlyCreatedMatchingRequest = await getMatchingRequest({
      id: createdMatchingRequest.id,
    });

    /* If the matching request is still pending */
    if (recentlyCreatedMatchingRequest.status === 'PENDING') {
      /* Time it out */
      const failedMatchingRequest = await updateMatchingRequest(
        { id: createdMatchingRequest.id },
        { status: 'TIMED_OUT' },
      );

      /* Notify subscriber about the failed match */
      pubsub.publish('PARTNER_MATCHING', {
        matchFound: failedMatchingRequest,
      });
    }
  }, 60 * 1000);

  return createdMatchingRequest;
};

export default withAccessControl(['USER', 'ADMIN'])(findMatchResolver);
