import { getEvent } from '../../../services/events';
import { unlikeEvent } from '../../../services/events-liking';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  UnlikeEventMutationArgs,
  Event,
} from './../../../types';

const unlikeEventResolver: Resolver = async (
  parent: Dictionary,
  args: UnlikeEventMutationArgs,
  context: Context,
): Promise<Event> => {
  const { id: eventId } = args;
  const { viewerId: unlikerId } = context;

  /* TODO: Fail if event is deleted */

  await unlikeEvent(unlikerId, eventId);

  return getEvent({ id: eventId });
};

export default withAccessControl(['USER', 'ADMIN'])(unlikeEventResolver);
