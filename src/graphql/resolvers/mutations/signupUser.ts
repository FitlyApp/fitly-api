import { createUser } from '../../../services/users';
import { issueJWT } from '../../../utilities';
import {
  Dictionary,
  Resolver,
  Context,
  SignupUserMutationArgs,
  JWTPayload,
  User,
} from './../../../types';

const signupUserResolver: Resolver = async (
  parent: Dictionary,
  args: SignupUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { email, password, firstName, lastName, bio, avatarUrl } = args;

  const createdUser: User = await createUser({
    email,
    password,
    authenticationMethod: 'PASSWORD',
    name: {
      first: firstName,
      last: lastName,
    },
    role: 'USER',
    bio,
    avatarUrl,
  });

  const tokenPayload: JWTPayload = {
    id: createdUser.id,
    role: 'USER',
  };

  const token: string = issueJWT(tokenPayload);

  return {
    ...createdUser,
    token,
  };
};

export default signupUserResolver;
