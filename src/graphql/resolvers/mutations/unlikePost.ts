import { getPost } from '../../../services/posts';
import { unlikePost } from '../../../services/posts-liking';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  LikePostMutationArgs,
  Event,
} from './../../../types';

const unlikePostResolver: Resolver = async (
  parent: Dictionary,
  args: LikePostMutationArgs,
  context: Context,
): Promise<Event> => {
  const { id: postId } = args;
  const { viewerId: unlikerId } = context;

  /* TODO: Fail if post is deleted */

  await unlikePost(unlikerId, postId);

  return getPost({ id: postId });
};

export default withAccessControl(['USER', 'ADMIN'])(unlikePostResolver);
