import * as _ from 'lodash';
import { getUser } from '../../../services/users';
import { AuthenticationError } from '../../../errors';
import { comparePasswords, issueJWT } from '../../../utilities';
import {
  Dictionary,
  Resolver,
  Context,
  LoginUserMutationArgs,
  JWTPayload,
  User,
} from './../../../types';

const loginUserResolver: Resolver = async (
  parent: Dictionary,
  args: LoginUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { email, password } = args;

  const retrievedUser: User = await getUser({ email });

  if (_.isNull(retrievedUser)) {
    throw new AuthenticationError({
      data: {
        developerMessage: `Email provided cannot be found: ${email}`,
        userMessage: 'This email cannot be found',
      },
    });
  }

  if (retrievedUser.authenticationMethod === 'FACEBOOK') {
    throw new AuthenticationError({
      data: {
        developerMessage: `Email provided is associated with a Facebook login: ${email}`,
        userMessage: 'This email is associated with a Facebook login',
      },
    });
  }

  if (retrievedUser.authenticationMethod === 'GOOGLE') {
    throw new AuthenticationError({
      data: {
        developerMessage: `Email provided is associated with a Google login: ${email}`,
        userMessage: 'This email is associated with a Google login',
      },
    });
  }

  const doPasswordsMatch: boolean = await comparePasswords(
    password,
    retrievedUser.passwordHash,
  );

  if (!doPasswordsMatch) {
    throw new AuthenticationError({
      data: {
        developerMessage: 'Password provided mismatches actual password',
        userMessage: 'Your email & password do not match',
      },
    });
  }

  if (!_.isNull(retrievedUser.bannedAt)) {
    throw new AuthenticationError({
      data: {
        developerMessage: `Account is banned: ${email}`,
        userMessage: 'Your account is banned',
      },
    });
  }

  const tokenPayload: JWTPayload = {
    id: retrievedUser.id,
    role: retrievedUser.role,
  };

  const token: string = issueJWT(tokenPayload);

  return {
    ...retrievedUser,
    token,
  };
};

export default loginUserResolver;
