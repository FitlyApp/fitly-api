import { unfollowUser } from '../../../services/following';
import { getUser } from '../../../services/users';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  FollowUserMutationArgs,
  User,
} from './../../../types';

const unfollowUserResolver: Resolver = async (
  parent: Dictionary,
  args: FollowUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { id: followeeId } = args;
  const { viewerId: followerId } = context;

  await unfollowUser(followerId, followeeId);
  return getUser({ id: followeeId });
};

export default withAccessControl(['USER', 'ADMIN'])(unfollowUserResolver);
