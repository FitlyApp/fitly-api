import { fetchMeetupEvents } from '../../../services/events';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  FetchMeetupEventsMutationArgs,
  Event,
} from './../../../types';

const fetchMeetupEventsResolver: Resolver = async (
  parent: Dictionary,
  args: FetchMeetupEventsMutationArgs,
  context: Context,
): Promise<Event[]> => {
  const { city } = args;
  return fetchMeetupEvents(city);
};

export default withAccessControl(['USER' /* FIXME: Remove */, 'ADMIN'])(
  fetchMeetupEventsResolver,
);
