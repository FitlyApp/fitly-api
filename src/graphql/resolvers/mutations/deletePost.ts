import { getPost, deletePost } from '../../../services/posts';
import activityModel from '../../../services/activities/model';
import { deleteMany } from '../../../mongodb';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  DeletePostMutationArgs,
  Post,
} from './../../../types';

const deletePostResolver: Resolver = async (
  parent: Dictionary,
  args: DeletePostMutationArgs,
  context: Context,
): Promise<Post> => {
  const { id } = args;
  const { viewerId, viewerRole } = context;

  const retrievedPost = await getPost({ id });
  const doesViewerOwnPost = retrievedPost.authorId === viewerId;

  if (viewerRole !== 'ADMIN' && !doesViewerOwnPost) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer does not own this resource to update it',
        userMessage: 'You cannot update this post',
      },
    });
  }

  const deletedPost = await deletePost({ id });

  await deleteMany(activityModel)({
    query: {
      objectType: 'Post',
      objectId: id,
    },
  });

  return deletedPost;
};

export default withAccessControl(['USER', 'ADMIN'])(deletePostResolver);
