import { getEvent } from '../../../services/events';
import { likeEvent } from '../../../services/events-liking';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  LikeEventMutationArgs,
  Event,
} from './../../../types';

const likeEventResolver: Resolver = async (
  parent: Dictionary,
  args: LikeEventMutationArgs,
  context: Context,
): Promise<Event> => {
  const { id: eventId } = args;
  const { viewerId: likerId } = context;

  /* TODO: Fail if event is deleted */

  await likeEvent(likerId, eventId);

  return getEvent({ id: eventId });
};

export default withAccessControl(['USER', 'ADMIN'])(likeEventResolver);
