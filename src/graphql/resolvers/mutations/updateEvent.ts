import { updateEvent } from '../../../services/events';
import { isHosting } from '../../../services/events-hosting';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  UpdateEventMutationArgs,
  Event,
  Cost,
  Venue,
} from './../../../types';

const updateEventResolver: Resolver = async (
  parent: Dictionary,
  args: UpdateEventMutationArgs,
  context: Context,
): Promise<Event> => {
  const {
    id,
    title,
    description,
    startsAt,
    endsAt,
    isPublic,
    costAmount,
    costCurrency,
    categoryId,
    imageUrl,
    venueName,
    venueAddress,
    venueLocationType,
    venueLocationCoordinates,
  } = args;
  const { viewerId, viewerRole } = context;

  const cost: Cost = {
    amount: costAmount,
    currency: costCurrency,
  };

  const venue: Venue = {
    name: venueName,
    address: venueAddress,
    location: {
      type: venueLocationType,
      coordinates: venueLocationCoordinates,
    },
  };

  const doesViewerOwnEvent = await isHosting(viewerId, id);

  if (viewerRole !== 'ADMIN' && !doesViewerOwnEvent) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer does not own this resource to update it',
        userMessage: 'You cannot update this event',
      },
    });
  }

  return updateEvent(
    { id },
    {
      title,
      description,
      startsAt,
      endsAt,
      isPublic,
      cost,
      categoryId,
      imageUrl,
      venue,
    },
  );
};

export default withAccessControl(['USER', 'ADMIN'])(updateEventResolver);
