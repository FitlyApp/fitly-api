import { followUser } from '../../../services/following';
import { getUser } from '../../../services/users';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  FollowUserMutationArgs,
  User,
} from './../../../types';

const followUserResolver: Resolver = async (
  parent: Dictionary,
  args: FollowUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { id: followeeId } = args;
  const { viewerId: followerId } = context;

  if (followerId === followeeId) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer cannot follow himself',
        userMessage: 'You cannot follow yourself',
      },
    });
  }

  await followUser(followerId, followeeId);
  return getUser({ id: followeeId });
};

export default withAccessControl(['USER', 'ADMIN'])(followUserResolver);
