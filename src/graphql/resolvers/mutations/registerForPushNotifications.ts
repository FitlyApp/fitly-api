import { registerForPushNotifications } from '../../../services/push-notifications';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  RegisterForPushNotificationsMutationArgs,
  PushNotificationsRegistration,
} from './../../../types';

// TODO: Test
const registerForPushNotificationsResolver: Resolver = async (
  parent: Dictionary,
  args: RegisterForPushNotificationsMutationArgs,
  context: Context,
): Promise<PushNotificationsRegistration> => {
  const { pushToken } = args;
  const { viewerId } = context;

  return registerForPushNotifications({
    userId: viewerId,
    pushToken,
  });
};

export default withAccessControl(['USER', 'ADMIN'])(
  registerForPushNotificationsResolver,
);
