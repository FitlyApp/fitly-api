import { getEvent } from '../../../services/events';
import { unattendEvent } from '../../../services/events-attending';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  UnattendEventMutationArgs,
  Event,
} from './../../../types';

const unattendEventResolver: Resolver = async (
  parent: Dictionary,
  args: UnattendEventMutationArgs,
  context: Context,
): Promise<Event> => {
  const { id: eventId } = args;
  const { viewerId: unattenderId } = context;

  /* TODO: Fail if event is deleted */

  await unattendEvent(unattenderId, eventId);

  return getEvent({ id: eventId });
};

export default withAccessControl(['USER', 'ADMIN'])(unattendEventResolver);
