import { getEvent } from '../../../services/events';
import { attendEvent } from '../../../services/events-attending';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  AttendEventMutationArgs,
  Event,
} from './../../../types';

const attendEventResolver: Resolver = async (
  parent: Dictionary,
  args: AttendEventMutationArgs,
  context: Context,
): Promise<Event> => {
  const { id: eventId } = args;
  const { viewerId: attenderId } = context;

  /* TODO: Fail if event is deleted */

  await attendEvent(attenderId, eventId);

  return getEvent({ id: eventId });
};

export default withAccessControl(['USER', 'ADMIN'])(attendEventResolver);
