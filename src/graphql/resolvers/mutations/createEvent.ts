import * as _ from 'lodash';
import { createEvent } from '../../../services/events';
import { hostEvent } from '../../../services/events-hosting';
import { createActivity } from '../../../services/activities';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  CreateEventMutationArgs,
  Event,
  ExternalOrigin,
  Cost,
  Venue,
} from './../../../types';

const createEventResolver: Resolver = async (
  parent: Dictionary,
  args: CreateEventMutationArgs,
  context: Context,
): Promise<Event> => {
  const {
    /* TODO: Remove external-event related fields from GraphQL */
    externalOriginName,
    externalOriginId,
    externalOriginUrl,
    title,
    description,
    startsAt,
    endsAt,
    isPublic,
    costAmount,
    costCurrency,
    categoryId,
    imageUrl,
    venueName,
    venueAddress,
    venueLocationType,
    venueLocationCoordinates,
  } = args;
  const { viewerId } = context;

  let externalOrigin: ExternalOrigin = {
    name: externalOriginName,
    id: externalOriginId,
    url: externalOriginUrl,
  };

  if (
    _.isNull(externalOriginName) ||
    _.isNull(externalOriginId) ||
    _.isNull(externalOriginUrl)
  ) {
    externalOrigin = null;
  }

  const cost: Cost = {
    amount: costAmount,
    currency: costCurrency,
  };

  const venue: Venue = {
    name: venueName,
    address: venueAddress,
    location: {
      type: venueLocationType,
      coordinates: venueLocationCoordinates,
    },
  };

  const createdEvent: Event = await createEvent({
    externalOrigin,
    title,
    description,
    startsAt,
    endsAt,
    isPublic,
    cost,
    categoryId,
    imageUrl,
    venue,
  });

  await Promise.all([
    hostEvent(viewerId, createdEvent.id),
    createActivity({
      actorId: viewerId,
      objectId: createdEvent.id,
      objectType: 'Event',
      verb: 'CREATED_EVENT',
    }),
  ]);

  return createdEvent;
};

export default withAccessControl(['USER', 'ADMIN'])(createEventResolver);
