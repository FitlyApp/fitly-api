import { updateUser } from '../../../services/users';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  UpdateUserMutationArgs,
  User,
} from './../../../types';

const updateUserResolver: Resolver = async (
  parent: Dictionary,
  args: UpdateUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { id, firstName, lastName, bio, avatarUrl } = args;
  const { viewerId, viewerRole } = context;

  if (viewerRole !== 'ADMIN' && viewerId !== id) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer does not own this resource to update it',
        userMessage: 'You cannot update this user',
      },
    });
  }

  return updateUser(
    { id },
    {
      name: {
        first: firstName,
        last: lastName,
      },
      bio,
      avatarUrl,
    },
  );
};

export default withAccessControl(['USER', 'ADMIN'])(updateUserResolver);
