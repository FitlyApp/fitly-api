import { createMessage } from '../../../services/messages';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  SendMessageMutationArgs,
  Message,
} from './../../../types';

const sendMessageResolver: Resolver = async (
  parent: Dictionary,
  args: SendMessageMutationArgs,
  context: Context,
): Promise<Message> => {
  const { threadId, content } = args;
  const { viewerId, pubsub } = context;

  const createdMessage = await createMessage({
    threadId,
    content,
    senderId: viewerId,
  });

  pubsub.publish('MESSAGE_SENDING', {
    messageSent: createdMessage,
  });

  return createdMessage;
};

export default withAccessControl(['USER', 'ADMIN'])(sendMessageResolver);
