import { deleteEvent } from '../../../services/events';
import { isHosting } from '../../../services/events-hosting';
import activityModel from '../../../services/activities/model';
import { deleteMany } from '../../../mongodb';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  DeleteEventMutationArgs,
  Event,
} from './../../../types';

const deleteEventResolver: Resolver = async (
  parent: Dictionary,
  args: DeleteEventMutationArgs,
  context: Context,
): Promise<Event> => {
  const { id } = args;
  const { viewerId, viewerRole } = context;

  const doesViewerOwnEvent = await isHosting(viewerId, id);

  if (viewerRole !== 'ADMIN' && !doesViewerOwnEvent) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer does not own this resource to delete it',
        userMessage: 'You cannot delete this event',
      },
    });
  }

  const deletedEvent = await deleteEvent({ id });

  await deleteMany(activityModel)({
    query: {
      objectType: 'Event',
      objectId: id,
    },
  });

  return deletedEvent;
};

export default withAccessControl(['USER', 'ADMIN'])(deleteEventResolver);
