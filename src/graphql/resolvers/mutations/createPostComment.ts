import { createPostComment } from '../../../services/posts-comments';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  CreatePostCommentMutationArgs,
  PostComment,
} from './../../../types';

const createPostCommentResolver: Resolver = (
  parent: Dictionary,
  args: CreatePostCommentMutationArgs,
  context: Context,
): Promise<PostComment> => {
  const { content, postId } = args;
  const { viewerId: authorId } = context;

  /* TODO: Fail if post is deleted */

  return createPostComment({
    content,
    authorId,
    postId,
  });
};

export default withAccessControl(['USER', 'ADMIN'])(createPostCommentResolver);
