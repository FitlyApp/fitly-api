import { getPost, updatePost } from '../../../services/posts';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  UpdatePostMutationArgs,
  Post,
} from './../../../types';

const updatePostResolver: Resolver = async (
  parent: Dictionary,
  args: UpdatePostMutationArgs,
  context: Context,
): Promise<Post> => {
  const { id, description, imageUrl } = args;
  const { viewerId, viewerRole } = context;

  const retrievedPost = await getPost({ id });
  const doesViewerOwnPost = retrievedPost.authorId === viewerId;

  if (viewerRole !== 'ADMIN' && !doesViewerOwnPost) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer does not own this resource to update it',
        userMessage: 'You cannot update this post',
      },
    });
  }

  return updatePost(
    { id },
    {
      description,
      imageUrl,
    },
  );
};

export default withAccessControl(['USER', 'ADMIN'])(updatePostResolver);
