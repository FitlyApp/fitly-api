import { deleteUser } from '../../../services/users';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  DeleteUserMutationArgs,
  User,
} from './../../../types';

const deleteUserResolver: Resolver = async (
  parent: Dictionary,
  args: DeleteUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { id } = args;
  const { viewerId, viewerRole } = context;

  if (viewerRole !== 'ADMIN' && viewerId !== id) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer does not own this resource to delete it',
        userMessage: 'You cannot delete this user',
      },
    });
  }

  return deleteUser({ id });
};

export default withAccessControl(['USER' /*FIXME: Remove*/, 'ADMIN'])(
  deleteUserResolver,
);
