import * as _ from 'lodash';
import { getUser, createUser } from '../../../services/users';
import { issueJWT } from '../../../utilities';
import {
  Dictionary,
  Resolver,
  Context,
  LoginUserWithGoogleMutationArgs,
  User,
  JWTPayload,
} from './../../../types';

// TODO: Test
// FIXME: Insecure
const loginUserWithGoogleResolver: Resolver = async (
  parent: Dictionary,
  args: LoginUserWithGoogleMutationArgs,
  context: Context,
): Promise<User> => {
  const { email, firstName, lastName, bio, avatarUrl } = args;

  const retrievedUser: User = await getUser({ email });

  if (_.isNull(retrievedUser)) {
    const createdUser: User = await createUser({
      email,
      authenticationMethod: 'GOOGLE',
      name: {
        first: firstName,
        last: lastName,
      },
      role: 'USER',
      bio,
      avatarUrl,
    });

    const tokenPayload: JWTPayload = {
      id: createdUser.id,
      role: 'USER',
    };

    const token: string = issueJWT(tokenPayload);

    return {
      ...createdUser,
      token,
    };
  }

  const tokenPayload: JWTPayload = {
    id: retrievedUser.id,
    role: 'USER',
  };

  const token: string = issueJWT(tokenPayload);

  return {
    ...retrievedUser,
    token,
  };
};

export default loginUserWithGoogleResolver;
