import { blockUser } from '../../../services/blocking';
import { getUser } from '../../../services/users';
import { ForbiddenError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  BlockUserMutationArgs,
  User,
} from './../../../types';

const blockUserResolver: Resolver = async (
  parent: Dictionary,
  args: BlockUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { id: blockedId } = args;
  const { viewerId: blockerId } = context;

  if (blockerId === blockedId) {
    throw new ForbiddenError({
      data: {
        developerMessage: 'Viewer cannot block himself',
        userMessage: 'You cannot block yourself',
      },
    });
  }

  await blockUser(blockerId, blockedId);
  return getUser({ id: blockedId });
};

export default withAccessControl(['USER', 'ADMIN'])(blockUserResolver);
