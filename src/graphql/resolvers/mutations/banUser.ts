import { banUser } from '../../../services/users';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  BanUserMutationArgs,
  User,
} from '../../../types';

const banUserResolver: Resolver = async (
  parent: Dictionary,
  args: BanUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { id } = args;
  return banUser({ id });
};

export default withAccessControl(['USER' /* TODO: REMOVE */, 'ADMIN'])(
  banUserResolver,
);
