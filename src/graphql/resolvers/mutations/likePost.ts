import { getPost } from '../../../services/posts';
import { likePost } from '../../../services/posts-liking';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  LikePostMutationArgs,
  Event,
} from './../../../types';

const likePostResolver: Resolver = async (
  parent: Dictionary,
  args: LikePostMutationArgs,
  context: Context,
): Promise<Event> => {
  const { id: postId } = args;
  const { viewerId: likerId } = context;

  /* TODO: Fail if post is deleted */

  await likePost(likerId, postId);

  return getPost({ id: postId });
};

export default withAccessControl(['USER', 'ADMIN'])(likePostResolver);
