import { unblockUser } from '../../../services/blocking';
import { getUser } from '../../../services/users';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  UnblockUserMutationArgs,
  User,
} from './../../../types';

const unblockUserResolver: Resolver = async (
  parent: Dictionary,
  args: UnblockUserMutationArgs,
  context: Context,
): Promise<User> => {
  const { id: blockedId } = args;
  const { viewerId: blockerId } = context;

  await unblockUser(blockerId, blockedId);
  return getUser({ id: blockedId });
};

export default withAccessControl(['USER', 'ADMIN'])(unblockUserResolver);
