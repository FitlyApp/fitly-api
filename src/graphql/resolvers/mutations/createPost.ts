import { createPost } from '../../../services/posts';
import { createActivity } from '../../../services/activities';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  CreatePostMutationArgs,
  Post,
} from './../../../types';

// TODO: Test
const createPostResolver: Resolver = async (
  parent: Dictionary,
  args: CreatePostMutationArgs,
  context: Context,
): Promise<Post> => {
  const { description, imageUrl } = args;
  const { viewerId } = context;

  const createdPost: Post = await createPost({
    description,
    imageUrl,
    authorId: viewerId,
  });

  await createActivity({
    actorId: viewerId,
    objectId: createdPost.id,
    objectType: 'Post',
    verb: 'CREATED_POST',
  });

  return createdPost;
};

export default withAccessControl(['USER', 'ADMIN'])(createPostResolver);
