import { getUser } from '../../../../services/users';
import { Resolver, Message, User } from './../../../../types';

const senderResolver: Resolver = (parent: Message): Promise<User> => {
  const { senderId } = parent;
  return getUser({ id: senderId });
};

export default senderResolver;
