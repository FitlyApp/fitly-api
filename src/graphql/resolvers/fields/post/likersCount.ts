import { getLikersCount } from '../../../../services/posts-liking';
import { Resolver, Post } from './../../../../types';

const likersCountResolver: Resolver = async (parent: Post): Promise<number> => {
  const { id } = parent;
  return getLikersCount(id);
};

export default likersCountResolver;
