import { searchPostsComments } from '../../../../services/posts-comments';
import { Resolver, Context, Post, PostComment } from './../../../../types';

const commentsResolver: Resolver = (
  parent: Post,
  _,
  context: Context,
): Promise<PostComment[]> => {
  const { id: postId } = parent;
  return searchPostsComments({ postId });
};

export default commentsResolver;
