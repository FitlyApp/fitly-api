import { Resolver, Context, Post, User } from './../../../../types';

const authorResolver: Resolver = (
  parent: Post,
  args,
  context: Context,
): Promise<User> => {
  const { authorId } = parent;
  const {
    dataloaders: { usersLoader },
  } = context;

  return usersLoader.load(authorId);
};

export default authorResolver;
