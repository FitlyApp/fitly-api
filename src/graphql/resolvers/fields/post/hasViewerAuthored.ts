import { Resolver, Context, Post } from './../../../../types';

const hasViewerAuthoredResolver: Resolver = (
  parent: Post,
  _,
  context: Context,
): boolean => {
  const { authorId } = parent;
  const { viewerId } = context;

  return authorId === viewerId;
};

export default hasViewerAuthoredResolver;
