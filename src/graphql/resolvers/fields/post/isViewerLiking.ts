import { isLiking } from '../../../../services/posts-liking';
import { Resolver, Context, Post } from './../../../../types';

const isViewerLikingResolver: Resolver = async (
  parent: Post,
  _,
  context: Context,
): Promise<boolean> => {
  const { id: postId } = parent;
  const { viewerId: userId } = context;

  return isLiking(userId, postId);
};

export default isViewerLikingResolver;
