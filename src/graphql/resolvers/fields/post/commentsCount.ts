import { getPostCommentsCount } from '../../../../services/posts-comments';
import { Resolver, Post } from './../../../../types';

const commentsCountResolver: Resolver = (parent: Post): Promise<number> => {
  const { id } = parent;
  return getPostCommentsCount(id);
};

export default commentsCountResolver;
