import * as _ from 'lodash';
import {
  Resolver,
  ActivityObjectType,
  ActivityObject,
  Event,
  Post,
} from './../../../../types';

const typeResolver: Resolver = (parent: ActivityObject): ActivityObjectType => {
  if (isEvent(parent)) {
    return 'Event';
  }

  if (isPost(parent)) {
    return 'Post';
  }

  return null;
};

const isEvent = (entity: any): entity is Event => {
  return !_.isUndefined(entity.startsAt);
};

const isPost = (entity: any): entity is Post => {
  return !_.isUndefined(entity.authorId);
};

export default typeResolver;
