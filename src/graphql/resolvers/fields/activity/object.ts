import {
  Resolver,
  Context,
  Activity,
  ActivityObject,
} from './../../../../types';

const objectResolver: Resolver = (
  parent: Activity,
  args,
  context: Context,
): Promise<ActivityObject> => {
  const { objectType, objectId } = parent;
  const {
    dataloaders: { eventsLoader, postsLoader },
  } = context;

  if (objectType === 'Event') {
    return eventsLoader.load(objectId);
  }

  if (objectType === 'Post') {
    return postsLoader.load(objectId);
  }

  return null;
};

export default objectResolver;
