import { Resolver, Context, Activity, User } from './../../../../types';

const actorResolver: Resolver = (
  parent: Activity,
  args,
  context: Context,
): Promise<User> => {
  const { actorId } = parent;
  const {
    dataloaders: { usersLoader },
  } = context;

  return usersLoader.load(actorId);
};

export default actorResolver;
