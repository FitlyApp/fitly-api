import { searchMessages } from '../../../../services/messages';
import { Resolver, Thread, Message } from './../../../../types';

const messagesResolver: Resolver = (parent: Thread): Promise<Message[]> => {
  const { id } = parent;
  return searchMessages({ threadId: id });
};

export default messagesResolver;
