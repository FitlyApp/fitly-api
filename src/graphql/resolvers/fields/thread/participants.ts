import { getUsersByIds } from '../../../../services/users';
import { Resolver, Thread, User } from './../../../../types';

const participantsResolver: Resolver = (parent: Thread): Promise<User[]> => {
  const { participantsIds } = parent;
  return getUsersByIds(participantsIds);
};

export default participantsResolver;
