import { isLiking } from '../../../../services/events-liking';
import { Resolver, Context, Event } from './../../../../types';

const isViewerLikingResolver: Resolver = async (
  parent: Event,
  _,
  context: Context,
): Promise<boolean> => {
  const { id: eventId } = parent;
  const { viewerId: userId } = context;

  return isLiking(userId, eventId);
};

export default isViewerLikingResolver;
