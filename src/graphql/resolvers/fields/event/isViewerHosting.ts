import { isHosting } from '../../../../services/events-hosting';
import { Resolver, Context, Event } from './../../../../types';

const isViewerHostingResolver: Resolver = async (
  parent: Event,
  _,
  context: Context,
): Promise<boolean> => {
  const { id: eventId } = parent;
  const { viewerId: userId } = context;

  return isHosting(userId, eventId);
};

export default isViewerHostingResolver;
