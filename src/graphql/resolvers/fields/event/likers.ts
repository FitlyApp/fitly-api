import { getLikers } from '../../../../services/events-liking';
import { Resolver, Event, User } from './../../../../types';

const likersResolver: Resolver = async (parent: Event): Promise<User[]> => {
  const { id } = parent;
  return getLikers(id);
};

export default likersResolver;
