import { getExternalHosts } from '../../../../services/events-hosting';
import { Resolver, Event, ExternalUser } from './../../../../types';

const externalHostsResolver: Resolver = async (
  parent: Event,
): Promise<ExternalUser[]> => {
  const { id } = parent;
  return getExternalHosts(id);
};

export default externalHostsResolver;
