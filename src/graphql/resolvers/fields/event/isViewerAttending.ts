import { isAttending } from '../../../../services/events-attending';
import { Resolver, Context, Event } from './../../../../types';

const isViewerAttendingResolver: Resolver = async (
  parent: Event,
  _,
  context: Context,
): Promise<boolean> => {
  const { id: eventId } = parent;
  const { viewerId: userId } = context;

  return isAttending(userId, eventId);
};

export default isViewerAttendingResolver;
