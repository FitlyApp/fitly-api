import { getExternalAttendees } from '../../../../services/events-attending';
import { Resolver, Event, ExternalUser } from './../../../../types';

const externalAttendeesResolver: Resolver = async (
  parent: Event,
): Promise<ExternalUser[]> => {
  const { id } = parent;
  return getExternalAttendees(id);
};

export default externalAttendeesResolver;
