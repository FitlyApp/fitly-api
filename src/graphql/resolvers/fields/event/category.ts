import { Resolver, Context, Event, EventCategory } from './../../../../types';

const categoryResolver: Resolver = async (
  parent: Event,
  args,
  context: Context,
): Promise<EventCategory> => {
  const { categoryId } = parent;
  const {
    dataloaders: { eventCategoriesLoader },
  } = context;

  if (!categoryId) {
    return null;
  }

  return eventCategoriesLoader.load(categoryId);
};

export default categoryResolver;
