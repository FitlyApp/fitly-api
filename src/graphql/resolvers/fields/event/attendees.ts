import { getAttendees } from '../../../../services/events-attending';
import { Resolver, Event, User } from './../../../../types';

const attendeesResolver: Resolver = async (parent: Event): Promise<User[]> => {
  const { id } = parent;
  return getAttendees(id);
};

export default attendeesResolver;
