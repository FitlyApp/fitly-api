import { getHosts } from '../../../../services/events-hosting';
import { Resolver, Event, User } from './../../../../types';

const hostsResolver: Resolver = async (parent: Event): Promise<User[]> => {
  const { id } = parent;
  return getHosts(id);
};

export default hostsResolver;
