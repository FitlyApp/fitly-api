import { getFollowers } from '../../../../services/following';
import { Resolver, User } from './../../../../types';

const followersResolver: Resolver = async (parent: User): Promise<User[]> => {
  const { id } = parent;
  return getFollowers(id);
};

export default followersResolver;
