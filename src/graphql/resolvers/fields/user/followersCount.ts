import { getFollowersCount } from '../../../../services/following';
import { Resolver, User } from './../../../../types';

const followersCountResolver: Resolver = async (
  parent: User,
): Promise<number> => {
  const { id } = parent;
  return getFollowersCount(id);
};

export default followersCountResolver;
