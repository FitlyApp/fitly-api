import { isBlocking } from '../../../../services/blocking';
import { Resolver, Context, User } from './../../../../types';

const isViewerBlockingResolver: Resolver = (
  parent: User,
  _,
  context: Context,
): Promise<boolean> => {
  const { id: userId } = parent;
  const { viewerId } = context;

  return isBlocking(viewerId, userId);
};

export default isViewerBlockingResolver;
