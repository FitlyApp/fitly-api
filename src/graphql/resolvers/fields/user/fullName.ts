import { Resolver, User } from './../../../../types';

const attendeesResolver: Resolver = (parent: User): string => {
  const { name } = parent;
  return `${name.first} ${name.last}`;
};

export default attendeesResolver;
