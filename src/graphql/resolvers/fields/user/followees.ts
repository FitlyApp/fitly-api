import { getFollowees } from '../../../../services/following';
import { Resolver, User } from './../../../../types';

const followeesResolver: Resolver = async (parent: User): Promise<User[]> => {
  const { id } = parent;
  return getFollowees(id);
};

export default followeesResolver;
