import { isFollowing } from '../../../../services/following';
import { Resolver, Context, User } from './../../../../types';

// TODO: Test
const isViewerFollowingResolver: Resolver = (
  parent: User,
  _,
  context: Context,
): Promise<boolean> => {
  const { id: userId } = parent;
  const { viewerId } = context;

  return isFollowing(viewerId, userId);
};

export default isViewerFollowingResolver;
