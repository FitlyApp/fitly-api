import { getFolloweesCount } from '../../../../services/following';
import { Resolver, User } from './../../../../types';

const followeesCountResolver: Resolver = async (
  parent: User,
): Promise<number> => {
  const { id } = parent;
  return getFolloweesCount(id);
};

export default followeesCountResolver;
