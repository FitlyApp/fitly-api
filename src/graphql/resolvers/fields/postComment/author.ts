import { getUser } from '../../../../services/users';
import { Resolver, PostComment, User } from './../../../../types';

const authorResolver: Resolver = (parent: PostComment): Promise<User> => {
  const { authorId } = parent;
  return getUser({ id: authorId });
};

export default authorResolver;
