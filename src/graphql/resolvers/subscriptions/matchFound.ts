import { withFilter } from 'graphql-yoga';
import {
  Context,
  MatchFoundSubscriptionArgs,
  MatchingRequest,
} from '../../../types';

// TODO: Test
const matchFoundResolver = {
  subscribe: withFilter(
    (parent: undefined, args: MatchFoundSubscriptionArgs, context: Context) => {
      const { pubsub } = context;
      return pubsub.asyncIterator('PARTNER_MATCHING');
    },
    (
      parent: { matchFound: MatchingRequest },
      args: MatchFoundSubscriptionArgs,
      context: Context,
    ) => {
      const { requesterId, accepterId } = parent.matchFound;
      const { userId } = args;

      return userId === requesterId || userId === accepterId;
    },
  ),
};

// TODO: Authenticate access
export default matchFoundResolver;
