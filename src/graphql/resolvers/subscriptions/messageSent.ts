import { withFilter } from 'graphql-yoga';
import { Context, MessageSentSubscriptionArgs, Message } from '../../../types';

// TODO: Test
const messageSentResolver = {
  subscribe: withFilter(
    (
      parent: undefined,
      args: MessageSentSubscriptionArgs,
      context: Context,
    ) => {
      const { pubsub } = context;
      return pubsub.asyncIterator('MESSAGE_SENDING');
    },
    (
      parent: { messageSent: Message },
      args: MessageSentSubscriptionArgs,
      context: Context,
    ) => {
      /* Notify whoever is: */
      return (
        /* listening to updates on this thread */
        parent.messageSent.threadId === args.threadId &&
        /* and is not the sender of the message */
        parent.messageSent.senderId !== args.userId
      );
    },
  ),
};

// TODO: Authenticate access
export default messageSentResolver;
