import * as _ from 'lodash';
import {
  getThreadByParticipantsIds,
  createThread,
} from '../../../services/threads';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  GetThreadQueryArgs,
  Thread,
} from './../../../types';

// TODO: Test
const getThreadResolver: Resolver = async (
  parent: Dictionary,
  args: GetThreadQueryArgs,
  context: Context,
): Promise<Thread> => {
  const { participantsIds } = args;

  // TODO: Prevent anyone but participants from reading this chat

  const existingThread = await getThreadByParticipantsIds(participantsIds);

  if (_.isNull(existingThread)) {
    // TODO: Should this be a mutation instead?
    return createThread({ participantsIds });
  }

  return existingThread;
};

export default withAccessControl(['USER', 'ADMIN'])(getThreadResolver);
