import { searchEventCategories } from '../../../services/events-categories';
import { withAccessControl } from '../../access-control';
import { Dictionary, Resolver, Context, Event } from './../../../types';

// TODO: Test
const getEventCategoriesResolver: Resolver = (
  parent: Dictionary,
  args: {},
  context: Context,
): Promise<Event[]> => {
  return searchEventCategories({});
};

export default withAccessControl(['USER', 'ADMIN'])(getEventCategoriesResolver);
