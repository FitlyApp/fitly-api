import { Dictionary, Resolver, Context, PingQueryArgs } from './../../../types';

const pingResolver: Resolver = (
  parent: Dictionary,
  args: PingQueryArgs,
  context: Context,
): string => {
  return 'pong';
};

export default pingResolver;
