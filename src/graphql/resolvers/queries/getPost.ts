import { getPost } from '../../../services/posts';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  GetPostQueryArgs,
  Post,
} from './../../../types';

// TODO: Test
const getPostResolver: Resolver = (
  parent: Dictionary,
  args: GetPostQueryArgs,
  context: Context,
): Promise<Post> => {
  const { id } = args;
  return getPost({ id });
};

export default withAccessControl(['USER', 'ADMIN'])(getPostResolver);
