import * as _ from 'lodash';
import { searchEvents } from '../../../services/events';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  SearchEventsQueryArgs,
  Event,
} from './../../../types';

// TODO: Test
const searchEventsResolver: Resolver = async (
  parent: Dictionary,
  args: SearchEventsQueryArgs,
  context: Context,
): Promise<Event[]> => {
  const { longitude, latitude } = args;

  const events = await searchEvents({
    longitude,
    latitude,
    deletedAt: null,
  });

  return _.uniqWith(events, (event1, event2) => {
    return event1.title === event2.title && event1.startsAt === event2.startsAt;
  });
};

export default withAccessControl(['USER', 'ADMIN'])(searchEventsResolver);
