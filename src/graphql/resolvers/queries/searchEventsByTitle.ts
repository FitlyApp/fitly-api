import * as _ from 'lodash';
import { searchEventsByTitle } from '../../../services/events';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  SearchEventsByTitleQueryArgs,
  Event,
} from '../../../types';

const searchEventsByTitleResolver: Resolver = async (
  parent: Dictionary,
  args: SearchEventsByTitleQueryArgs,
  context: Context,
): Promise<Event[]> => {
  const { title } = args;

  const events = await searchEventsByTitle({
    title,
    deletedAt: null,
  });

  return _.uniqWith(events, (event1, event2) => {
    return event1.title === event2.title && event1.startsAt === event2.startsAt;
  });
};

export default withAccessControl(['USER', 'ADMIN'])(
  searchEventsByTitleResolver,
);
