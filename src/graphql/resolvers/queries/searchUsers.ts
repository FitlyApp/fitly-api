import { searchUsers } from '../../../services/users';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  SearchUsersQueryArgs,
  User,
} from './../../../types';

// TODO: Test
const searchUsersResolver: Resolver = (
  parent: Dictionary,
  args: SearchUsersQueryArgs,
  context: Context,
): Promise<User[]> => {
  const { name } = args;
  return searchUsers({ name });
};

export default withAccessControl(['USER', 'ADMIN'])(searchUsersResolver);
