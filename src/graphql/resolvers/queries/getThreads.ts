import * as _ from 'lodash';
import { searchThreads } from '../../../services/threads';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  GetThreadsQueryArgs,
  Thread,
} from './../../../types';

// TODO: Test
const getThreadsResolver: Resolver = (
  parent: Dictionary,
  args: GetThreadsQueryArgs,
  context: Context,
): Promise<Thread[]> => {
  const { viewerId } = context;

  // TODO: Prevent anyone but participants from reading these chats

  return searchThreads({ participantsIds: viewerId });
};

export default withAccessControl(['USER', 'ADMIN'])(getThreadsResolver);
