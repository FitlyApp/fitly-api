import { getEvent } from '../../../services/events';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  GetEventQueryArgs,
  Event,
} from './../../../types';

const getEventResolver: Resolver = (
  parent: Dictionary,
  args: GetEventQueryArgs,
  context: Context,
): Promise<Event> => {
  const { id } = args;
  return getEvent({ id });
};

export default withAccessControl(['USER', 'ADMIN'])(getEventResolver);
