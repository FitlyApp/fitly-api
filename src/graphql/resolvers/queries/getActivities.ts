import { searchActivities } from '../../../services/activities';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  GetActivitiesQueryArgs,
  Activity,
} from './../../../types';

// TODO: Test
const getActivitiesResolver: Resolver = (
  parent: Dictionary,
  args: GetActivitiesQueryArgs,
  context: Context,
): Promise<Activity[]> => {
  const { userId } = args;
  return searchActivities({ actorId: userId });
};

export default withAccessControl(['USER', 'ADMIN'])(getActivitiesResolver);
