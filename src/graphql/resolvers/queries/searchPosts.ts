import * as _ from 'lodash';
import postModel from '../../../services/posts/model';
import { getMany } from '../../../mongodb';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  SearchPostsQueryArgs,
  Post,
} from './../../../types';

// TODO: Test
const searchPostsResolver: Resolver = async (
  parent: Dictionary,
  args: SearchPostsQueryArgs,
  context: Context,
): Promise<Post[]> => {
  /* TODO: Honor description argument */
  const { description } = args;

  const posts = await getMany(postModel)({
    query: {
      imageUrl: { $ne: null },
      deletedAt: null,
    },
  });

  return _.shuffle(posts);
};

export default withAccessControl(['USER', 'ADMIN'])(searchPostsResolver);
