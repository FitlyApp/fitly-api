import * as _ from 'lodash';
import { getUser } from '../../../services/users';
import { AuthenticationError } from '../../../errors';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  GetUserQueryArgs,
  User,
} from './../../../types';

const getUserResolver: Resolver = async (
  parent: Dictionary,
  args: GetUserQueryArgs,
  context: Context,
): Promise<User> => {
  const { id } = args;

  const retrievedUser: User = await getUser({ id });

  if (_.isNull(retrievedUser)) {
    throw new AuthenticationError({
      data: {
        developerMessage: `User cannot be found: ${id}`,
        userMessage: 'This user cannot be found',
      },
    });
  }

  return retrievedUser;
};

export default withAccessControl(['USER', 'ADMIN'])(getUserResolver);
