import * as _ from 'lodash';
import { getFolloweesIds } from '../../../services/following';
import { searchActivities } from '../../../services/activities';
import { withAccessControl } from '../../access-control';
import {
  Dictionary,
  Resolver,
  Context,
  GetFeedQueryArgs,
  Activity,
} from './../../../types';

// TODO: Test
const getFeedResolver: Resolver = async (
  parent: Dictionary,
  args: GetFeedQueryArgs,
  context: Context,
): Promise<Activity[]> => {
  const { viewerId } = context;

  /* TODO: Replace with native MongoDB query */

  const followeesIds = await getFolloweesIds(viewerId);

  const followeesActivities = await Promise.all(
    followeesIds.map((id) => searchActivities({ actorId: id })),
  );

  return _.chain(followeesActivities)
    .flatten()
    .sortBy('createdAt')
    .reverse()
    .value();
};

export default withAccessControl(['USER', 'ADMIN'])(getFeedResolver);
