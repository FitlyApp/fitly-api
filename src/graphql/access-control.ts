import * as _ from 'lodash';
import { AuthorizationError } from '../errors';
import { extractJWT, verifyJWT } from '../utilities';
import {
  Dictionary,
  Resolver,
  Context,
  AuthorizationRole,
  JWTPayload,
} from '../types';

export const withAccessControl = (authorizedRoles: AuthorizationRole[]) => (
  resolver: Resolver,
): Resolver => {
  return (
    parent: Dictionary,
    args: Dictionary,
    context: Context,
    info: Dictionary,
  ): string | Dictionary | Promise<Dictionary> => {
    const authorizationHeader: string = context.request.get('Authorization');

    if (_.isUndefined(authorizationHeader)) {
      throw new AuthorizationError({
        data: {
          developerMessage: 'HTTP `Authorization` header not provided',
          userMessage: null,
        },
      });
    }

    try {
      const token: string = extractJWT(authorizationHeader);
      const payload: JWTPayload = <JWTPayload>verifyJWT(token);

      context.viewerId = payload.id;
      context.viewerRole = payload.role;
    } catch (error) {
      throw new AuthorizationError({
        data: {
          developerMessage: 'Invalid HTTP `Authorization` header',
          userMessage: null,
        },
      });
    }

    if (!_.includes(authorizedRoles, context.viewerRole)) {
      throw new AuthorizationError({
        data: {
          developerMessage:
            'Viewer authorization role does not allow this action',
          userMessage: null,
        },
      });
    }

    return resolver(parent, args, context, info);
  };
};
