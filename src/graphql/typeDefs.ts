export default `
  type Query {
    ping: String
    getUser(id: String!): User
    searchUsers(name: String!): [User]
    searchEvents(longitude: Float!, latitude: Float!): [Event]
    searchEventsByTitle(title: String!): [Event]
    getEvent(id: String!): Event
    # TODO: Rename getEventCategories to searchEventCategories
    getEventCategories: [EventCategory]
    searchPosts(title: String): [Post]
    getPost(id: String!): Post
    getActivities(userId: String!): [Activity]
    getFeed: [Activity]
    getThread(participantsIds: [String!]!): Thread
    getThreads: [Thread]
  }

  type Mutation {
    signupUser(
      email: String!
      password: String!
      firstName: String!
      lastName: String!
      bio: String!
      avatarUrl: String!
    ): User

    loginUser(email: String!, password: String!): User

    loginUserWithFacebook(
      email: String!
      firstName: String!
      lastName: String!
      bio: String!
      avatarUrl: String!
    ): User

    loginUserWithGoogle(
      email: String!
      firstName: String!
      lastName: String!
      bio: String!
      avatarUrl: String!
    ): User

    updateUser(
      id: String!
      firstName: String
      lastName: String
      bio: String
      avatarUrl: String
    ): User

    deleteUser(id: String!): User

    banUser(id: String!): User

    followUser(id: String!): User

    unfollowUser(id: String!): User

    blockUser(id: String!): User

    unblockUser(id: String!): User

    createEvent(
      # TODO: Remove external-event related fields from GraphQL
      externalOriginName: String
      externalOriginId: String
      externalOriginUrl: String
      title: String!
      description: String
      startsAt: Float!
      endsAt: Float!
      isPublic: Boolean!
      costAmount: Float!
      costCurrency: String!
      categoryId: String
      imageUrl: String
      venueName: String
      venueAddress: String
      venueLocationType: String
      venueLocationCoordinates: [Float]
    ): Event

    updateEvent(
      id: String!
      title: String
      description: String
      startsAt: Float
      endsAt: Float
      isPublic: Boolean
      costAmount: Float
      costCurrency: String
      categoryId: String
      imageUrl: String
      venueName: String
      venueAddress: String
      venueLocationType: String
      venueLocationCoordinates: [Float]
    ): Event

    deleteEvent(id: String!): Event

    attendEvent(id: String!): Event
    
    unattendEvent(id: String!): Event

    likeEvent(id: String!): Event

    unlikeEvent(id: String!): Event

    createPost(
      description: String!
      imageUrl: String
    ): Post

    updatePost(
      id: String!
      description: String
      imageUrl: String
    ): Post

    deletePost(id: String!): Post

    createPostComment(
      postId: String!
      content: String!
    ): PostComment

    likePost(id: String!): Post

    unlikePost(id: String!): Post

    findMatch(
      coordinates: [Float!]!
      workoutType: String!
      activityLevel: String!
    ): MatchingRequest

    sendMessage(
      threadId: String!
      content: String!
    ): Message

    fetchMeetupEvents(city: String!): [Event]

    registerForPushNotifications(
      pushToken: String!
    ): PushNotificationsRegistration
  }

  type Subscription {
    matchFound(userId: String!): MatchingRequest
    messageSent(userId: String!, threadId: String!): Message
  }

  type User {
    id: String
    email: String
    name: Name
    role: String
    bio: String
    avatarUrl: String
    createdAt: Float
    updatedAt: Float
    deletedAt: Float
    bannedAt: Float
    token: String
    fullName: String
    followers: [User]
    followees: [User]
    followersCount: Int
    followeesCount: Int
    isViewerFollowing: Boolean
    isViewerBlocking: Boolean
  }

  type Event {
    id: String
    externalOrigin: ExternalOrigin
    title: String
    description: String
    startsAt: Float
    endsAt: Float
    isPublic: Boolean
    cost: Cost
    categoryId: String
    imageUrl: String
    venue: Venue
    createdAt: Float
    updatedAt: Float
    deletedAt: Float
    category: EventCategory
    hosts: [User]
    externalHosts: [ExternalUser]
    attendees: [User]
    externalAttendees: [ExternalUser]
    likers: [User]
    isViewerHosting: Boolean
    isViewerAttending: Boolean
    isViewerLiking: Boolean
  }

  type Post {
    id: String
    description: String
    imageUrl: String
    authorId: String
    createdAt: Float
    updatedAt: Float
    deletedAt: Float
    author: User
    comments: [PostComment]
    commentsCount: Int
    likersCount: Int
    hasViewerAuthored: Boolean
    isViewerLiking: Boolean
  }

  type PostComment {
    id: String
    content: String
    authorId: String
    postId: String
    createdAt: Float
    author: User
  }

  type Activity {
    id: String
    actorId: String
    objectId: String
    objectType: ActivityObjectType
    verb: ActivityVerb
    createdAt: Float
    actor: User
    object: ActivityObject
  }

  type MatchingRequest {
    id: String
    requesterId: String
    accepterId: String
    location: Location
    workoutType: String
    activityLevel: String
    status: String
    createdAt: Float
    updatedAt: Float
    deletedAt: Float
  }

  type Message {
    id: String
    senderId: String
    threadId: String
    content: String
    createdAt: Float
    updatedAt: Float
    deletedAt: Float
    sender: User
  }

  type Thread {
    id: String
    participantsIds: [String]
    createdAt: Float
    updatedAt: Float
    deletedAt: Float
    messages: [Message]
    participants: [User]
  }

  type PushNotificationsRegistration {
    userId: String
    pushToken: String
  }

  type Name {
    first: String
    last: String
  }

  type ExternalOrigin {
    name: String
    id: String
    url: String
  }

  type ExternalUser {
    id: String
    fullName: String
    avatarUrl: String
  }

  type Cost {
    amount: Float
    currency: String
  }

  type Venue {
    name: String
    address: String
    location: Location
  }

  type Location {
    type: String
    coordinates: [Float]
  }

  type EventCategory {
    id: String
    title: String
    imageUrl: String
    createdAt: Float
    updatedAt: Float
    deletedAt: Float
  }

  type File {
    id: ID
    path: String
    filename: String
    mimetype: String
    encoding: String
  }

  enum ActivityVerb {
    CREATED_EVENT
    CREATED_POST
  }

  enum ActivityObjectType {
    Event
    Post
  }

  union ActivityObject = Event | Post

  scalar Upload
`;
