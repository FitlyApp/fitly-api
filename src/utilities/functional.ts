const ramda = require('ramda');

type Case<IfType, ThenType> = {
  if: IfType;
  then: ThenType | undefined;
};

// TODO: Test
export const match = <IfType, ThenType>(cases: Case<IfType, ThenType>[]) => (
  value: IfType,
): ThenType | undefined => {
  return cases.reduceRight(
    (
      resultSoFar: ThenType | undefined,
      currentCase: Case<IfType, ThenType>,
    ) => {
      if (currentCase.if === value) {
        return currentCase.then;
      }

      return resultSoFar;
    },
    undefined,
  );
};

/**
 *  FIXME: Exporting untyped Ramda, because
 *  Ramda is not playing well with TypeScript
 *  at the moment.
 */
export const R = ramda;
