import { hash, compare } from 'bcryptjs';

export const hashPassword = (password: string): Promise<string> => {
  return hash(password, 10);
};

export const comparePasswords = (
  password: string,
  passwordHash: string,
): Promise<boolean> => {
  return compare(password, passwordHash);
};
