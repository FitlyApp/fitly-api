import * as _ from 'lodash';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import isEmail from 'validator/lib/isEmail';
import { isHttpUri, isHttpsUri } from 'valid-url';
import Expo from 'expo-server-sdk';
import { Constraints, Dictionary, ValidationResult } from '../types';

export const validate = (constraints: Constraints) => (
  entity: Dictionary,
): ValidationResult => {
  return Object.keys(constraints).reduce<ValidationResult>(
    (accumulative, path) => {
      const validationFunction = constraints[path].validationFunction;
      const invalidMessage = constraints[path].invalidMessage;

      const value = _.get(entity, path);

      const isValueInsignificant = _.isNil(value);

      if (isValueInsignificant) {
        return accumulative;
      }

      const isValueValid = validationFunction(value);

      if (isValueValid) {
        return accumulative;
      }

      return {
        isValid: false,
        message: invalidMessage(value),
      };
    },
    {
      isValid: true,
      message: null,
    },
  );
};

export const isValidEmail = (str: string): boolean => {
  return isEmail(str);
};

export const isValidUrl = (str: string): boolean => {
  return Boolean(isHttpUri(str)) || Boolean(isHttpsUri(str));
};

export const isValidUnixMillisTimestamp = (num: number): boolean => {
  return moment(num).isValid() && _.isInteger(num) && num >= 0;
};

export const isValidLongitude = (num: number): boolean => {
  return num < 180 && num > -180;
};

export const isValidLatitude = (num: number): boolean => {
  return num < 90 && num > -90;
};

export const isValidMongoID = (str: string): boolean => {
  return mongoose.Types.ObjectId.isValid(str);
};

// TODO: Test
export const isValidExpoPushToken = (str: string): boolean => {
  return Expo.isExpoPushToken(str);
};
