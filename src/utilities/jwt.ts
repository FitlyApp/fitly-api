import { sign, verify } from 'jsonwebtoken';

export const issueJWT = (payload: string | object): string => {
  return sign(payload, process.env.JWT_SECRET, { noTimestamp: true });
};

export const verifyJWT = (token: string): string | object => {
  return verify(token, process.env.JWT_SECRET);
};

export const extractJWT = (authorizationHeader: string): string => {
  return authorizationHeader.replace('Bearer ', '');
};
