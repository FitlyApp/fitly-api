import * as DataLoader from 'dataloader';
import { getMany } from '../mongodb';
import postModel from '../services/posts/model';
import { orderByIds } from './utilities';
import { Post } from '../types';

export const postsLoader = () => {
  return new DataLoader<string, Post>(batchPosts as any);
};

const batchPosts = async (postsIds: string[]): Promise<Post[]> => {
  const posts = await getMany(postModel)({
    query: { _id: { $in: postsIds } },
  });

  return orderByIds(postsIds)(posts);
};
