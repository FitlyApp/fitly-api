import { Dictionary } from '../../types';

export const orderByIds = (ids: string[]) => <T>(collection: T[]): T[] => {
  const map: Dictionary<T> = {};

  collection.forEach((element) => {
    map[(element as any).id] = element;
  });

  return ids.map((id) => map[id]);
};
