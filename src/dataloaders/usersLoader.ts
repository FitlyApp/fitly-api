import * as DataLoader from 'dataloader';
import { getUsersByIds } from '../services/users';
import { orderByIds } from './utilities';
import { User } from '../types';

export const usersLoader = () => {
  return new DataLoader<string, User>(batchUsers as any);
};

const batchUsers = async (usersIds: string[]): Promise<User[]> => {
  const users = await getUsersByIds(usersIds);
  return orderByIds(usersIds)(users);
};
