import * as DataLoader from 'dataloader';
import { getMany } from '../mongodb';
import eventModel from '../services/events/model';
import { orderByIds } from './utilities';
import { Event } from '../types';

export const eventsLoader = () => {
  return new DataLoader<string, Event>(batchEvents as any);
};

const batchEvents = async (eventsIds: string[]): Promise<Event[]> => {
  const events = await getMany(eventModel)({
    query: { _id: { $in: eventsIds } },
  });

  return orderByIds(eventsIds)(events);
};
