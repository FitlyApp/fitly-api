import * as DataLoader from 'dataloader';
import { getMany } from '../mongodb';
import eventCategoryModel from '../services/events-categories/model';
import { orderByIds } from './utilities';
import { EventCategory } from '../types';

export const eventCategoriesLoader = () => {
  return new DataLoader<string, EventCategory>(batchEventCategories as any);
};

const batchEventCategories = async (
  eventCategoriesIds: string[],
): Promise<EventCategory[]> => {
  const eventCategories = await getMany(eventCategoryModel)({
    query: { _id: { $in: eventCategoriesIds } },
  });

  return orderByIds(eventCategoriesIds)(eventCategories);
};
