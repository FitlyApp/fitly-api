import { startMongoDBConnection } from './mongodb';
import { startGraphQLServer } from './server';
import { loadEnvironmentVariables } from './utilities';
import { Database, Server } from './types';

(async () => {
  try {
    loadEnvironmentVariables([
      'NODE_ENV',
      'JWT_SECRET',
      'MONGODB_USERNAME',
      'MONGODB_PASSWORD',
    ]);

    const database: Database = await startMongoDBConnection();
    const server: Server = await startGraphQLServer();

    console.log(`Server is running on port ${server.options.port}...`);
  } catch (error) {
    console.log('Failed to start server:', error);
  }
})();
