import { Http2Server } from 'http2';
import { Options, PubSub } from 'graphql-yoga';
import { Mongoose } from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Request } from 'express-serve-static-core';
import {
  usersLoader,
  eventsLoader,
  postsLoader,
  eventCategoriesLoader,
} from './dataloaders';

/* Generic */

export type Dictionary<T = any> = {
  [key: string]: T;
};

/* Server */

export type Server = {
  httpServer: Http2Server;
  options: Options;
};

/* Database */

export type Database = Mongoose;

export type DatabaseServer = MongoMemoryServer;

/* Validation */

export type Constraints = {
  [path: string]: {
    validationFunction: (value: any) => boolean;
    invalidMessage: (value: any) => string;
  };
};

export type ValidationResult = {
  isValid: boolean;
  message: string;
};

/* Services */

// Users

export type User = {
  id?: string;
  email?: string;
  password?: string;
  passwordHash?: string;
  authenticationMethod?: AuthenticationMethod;
  name?: {
    first?: string;
    last?: string;
  };
  role?: AuthorizationRole;
  bio?: string;
  avatarUrl?: string;
  createdAt?: number;
  updatedAt?: number;
  deletedAt?: number;
  bannedAt?: number;
  token?: string;
  fullName?: string;
  followers?: User[];
  followees?: User[];
  followersCount?: number;
  followeesCount?: number;
  isViewerBlocking?: boolean;
};

// Following

export type Following = {
  id?: string;
  followerId?: string;
  followeeId?: string;
  createdAt?: number;
};

// Blocking

export type Blocking = {
  id?: string;
  blockerId?: string;
  blockedId?: string;
  createdAt?: number;
};

// Events

export type Event = {
  id?: string;
  externalOrigin?: ExternalOrigin;
  title?: string;
  description?: string;
  startsAt?: number;
  endsAt?: number;
  isPublic?: boolean;
  cost?: Cost;
  categoryId?: string | null;
  imageUrl?: string;
  venue?: Venue;
  createdAt?: number;
  updatedAt?: number;
  deletedAt?: number;
  hosts?: User[];
  externalHosts?: ExternalUser[];
  attendees?: User[];
  externalAttendees?: ExternalUser[];
  likers?: User[];
  isViewerHosting?: boolean;
  isViewerAttending?: boolean;
  isViewerLiking?: boolean;
};

export type EventCategory = {
  id?: string;
  title?: string;
  imageUrl?: string;
  createdAt?: number;
  updatedAt?: number;
  deletedAt?: number;
};

export type ExternalOrigin = {
  name?: string;
  id?: string;
  url?: string;
};

export type ExternalUser = {
  id?: string;
  fullName?: string;
  avatarUrl?: string;
};

export type Cost = {
  amount?: number;
  currency?: string;
};

export type Venue = {
  name?: string;
  address?: string;
  location?: Location;
};

// Events Hosting

export type EventHosting = {
  id?: string;
  eventId?: string;
  userId?: string;
  externalUser?: ExternalUser;
  createdAt?: number;
};

// Events Attending

export type EventAttending = {
  id?: string;
  eventId?: string;
  userId?: string;
  externalUser?: ExternalUser;
  createdAt?: number;
};

// Events Liking

export type EventLiking = {
  id?: string;
  eventId?: string;
  userId?: string;
  createdAt?: number;
};

// Posts

export type Post = {
  id?: string;
  description?: string;
  imageUrl?: string;
  authorId?: string;
  createdAt?: number;
  updatedAt?: number;
  deletedAt?: number;
};

// Posts Comments

export type PostComment = {
  id?: string;
  content?: string;
  authorId?: string;
  postId?: string;
  createdAt?: number;
};

// Posts Liking

export type PostLiking = {
  id?: string;
  postId?: string;
  userId?: string;
  createdAt?: number;
};

// Feeds

export type Activity = {
  id?: string;
  actorId?: string;
  objectId?: string;
  objectType?: ActivityObjectType;
  verb?: ActivityVerb;
  createdAt?: number;
};

export type ActivityVerb = 'CREATED_EVENT' | 'CREATED_POST';

export type ActivityObjectType = 'Event' | 'Post';

export type ActivityObject = Event | Post;

// Partner Matching

export type MatchingRequest = {
  id?: string;
  requesterId?: string;
  accepterId?: string;
  location?: Location;
  workoutType?: MatchingRequestWorkoutType;
  activityLevel?: MatchingRequestActivityLevel;
  status?: MatchingRequestStatus;
  createdAt?: number;
  updatedAt?: number;
  deletedAt?: number;
};

export type MatchingRequestWorkoutType = 'CARDIO' | 'STRENGTH_TRAINING';

export type MatchingRequestActivityLevel =
  | 'COUCH_POTATO'
  | 'SLOTH'
  | 'WIMP'
  | 'BABY'
  | 'STUDENT'
  | 'DETERMINED'
  | 'ATHLETE'
  | 'CHAMP'
  | 'BEAST'
  | 'WARRIOR'
  | 'SPARTAN';

export type MatchingRequestStatus = 'PENDING' | 'COMPLETE' | 'TIMED_OUT';

// Messages

export type Message = {
  id?: string;
  senderId?: string;
  threadId?: string;
  content?: string;
  createdAt?: number;
  updatedAt?: number;
  deletedAt?: number;
};

// Threads

export type Thread = {
  id?: string;
  participantsIds?: string[];
  createdAt?: number;
  updatedAt?: number;
  deletedAt?: number;
};

// Push Notifications

export type PushNotification = {
  userId?: string;
  title?: string;
  body?: string;
  channelId?: string;
};

export type PushNotificationsRegistration = {
  id?: string;
  userId?: string;
  pushToken?: string;
  createdAt?: number;
};

// Generic

export type AuthenticationMethod = 'PASSWORD' | 'FACEBOOK' | 'GOOGLE';

export type GeoJsonType = 'Point';

export type Coordinates = [Number, Number];

export type Location = {
  type?: GeoJsonType;
  coordinates?: Coordinates;
};

export type GeospatialQuery = {
  coordinates: Coordinates;
  maximumDistanceMeters: number;
};

/* GraphQL */

// Resolvers

export type Resolver<
  Parent = any,
  Args = any,
  Context = any,
  Response = any
> = (
  parent: Parent,
  args: Args,
  context: Context,
  info: any,
) => Response | Promise<Response>;

// Resolvers Context

export type Context = {
  request: Request;
  viewerId: string | undefined;
  viewerRole: AuthorizationRole;
  pubsub: PubSub;
  dataloaders: {
    usersLoader: ReturnType<typeof usersLoader>;
    eventsLoader: ReturnType<typeof eventsLoader>;
    postsLoader: ReturnType<typeof postsLoader>;
    eventCategoriesLoader: ReturnType<typeof eventCategoriesLoader>;
  };
};

// Resolvers (Queries) Arguments

export type PingQueryArgs = {};

export type GetUserQueryArgs = {
  id: string;
};

export type SearchUsersQueryArgs = {
  name: string;
};

export type SearchEventsQueryArgs = {
  longitude: number;
  latitude: number;
};

export type SearchEventsByTitleQueryArgs = {
  title: string;
};

export type GetEventQueryArgs = {
  id: string;
};

export type SearchPostsQueryArgs = {
  description?: string;
};

export type GetPostQueryArgs = {
  id: string;
};

export type GetActivitiesQueryArgs = {
  userId: string;
};

export type GetFeedQueryArgs = {};

export type GetThreadQueryArgs = {
  participantsIds: string[];
};

export type GetThreadsQueryArgs = {};

// Resolvers (Mutations) Arguments

export type SignupUserMutationArgs = {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  bio: string;
  avatarUrl: string;
};

export type LoginUserMutationArgs = {
  email: string;
  password: string;
};

export type LoginUserWithFacebookMutationArgs = {
  email: string;
  firstName: string;
  lastName: string;
  bio: string;
  avatarUrl: string;
};

export type LoginUserWithGoogleMutationArgs = {
  email: string;
  firstName: string;
  lastName: string;
  bio: string;
  avatarUrl: string;
};

export type UpdateUserMutationArgs = {
  id: string;
  firstName?: string;
  lastName?: string;
  bio?: string;
  avatarUrl?: string;
};

export type DeleteUserMutationArgs = {
  id: string;
};

export type BanUserMutationArgs = {
  id: string;
};

export type FollowUserMutationArgs = {
  id: string;
};

export type UnfollowUserMutationArgs = {
  id: string;
};

export type BlockUserMutationArgs = {
  id: string;
};

export type UnblockUserMutationArgs = {
  id: string;
};

export type CreateEventMutationArgs = {
  externalOriginName?: string;
  externalOriginId?: string;
  externalOriginUrl?: string;
  title: string;
  description?: string;
  startsAt: number;
  endsAt: number;
  isPublic: boolean;
  costAmount: number;
  costCurrency: string;
  categoryId?: string;
  imageUrl?: string;
  venueName?: string;
  venueAddress?: string;
  venueLocationType?: GeoJsonType;
  venueLocationCoordinates?: Coordinates;
};

export type UpdateEventMutationArgs = {
  id: string;
  title?: string;
  description?: string;
  startsAt?: number;
  endsAt?: number;
  isPublic?: boolean;
  costAmount?: number;
  costCurrency?: string;
  categoryId?: string;
  imageUrl?: string;
  venueName?: string;
  venueAddress?: string;
  venueLocationType?: GeoJsonType;
  venueLocationCoordinates?: Coordinates;
};

export type DeleteEventMutationArgs = {
  id: string;
};

export type AttendEventMutationArgs = {
  id: string;
};

export type UnattendEventMutationArgs = {
  id: string;
};

export type LikeEventMutationArgs = {
  id: string;
};

export type UnlikeEventMutationArgs = {
  id: string;
};

export type CreatePostMutationArgs = {
  description: string;
  imageUrl?: string;
};

export type CreatePostCommentMutationArgs = {
  postId: string;
  content: string;
};

export type UpdatePostMutationArgs = {
  id: string;
  description?: string;
  imageUrl?: string;
};

export type DeletePostMutationArgs = {
  id: string;
};

export type LikePostMutationArgs = {
  id: string;
};

export type UnlikePostMutationArgs = {
  id: string;
};

export type FindMatchMutationArgs = {
  coordinates: Coordinates;
  workoutType: MatchingRequestWorkoutType;
  activityLevel: MatchingRequestActivityLevel;
};

export type SendMessageMutationArgs = {
  threadId: string;
  content: string;
};

export type FetchMeetupEventsMutationArgs = {
  city: string;
};

export type RegisterForPushNotificationsMutationArgs = {
  pushToken: string;
};

// Resolvers (Subscriptions) Arguments

export type MatchFoundSubscriptionArgs = {
  userId: string;
};

export type MessageSentSubscriptionArgs = {
  userId: string;
  threadId: string;
};

// Access Control

export type AuthorizationRole = 'USER' | 'ADMIN';

export type JWTPayload = {
  id: string;
  role: AuthorizationRole;
};
