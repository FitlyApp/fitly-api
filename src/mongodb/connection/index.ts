import * as mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { connectionOptions } from '../constants';
import { match } from '../../utilities';
import settings from '../../settings';
import { DatabaseServer, Database } from '../../types';

export const startMongoDBConnection = async (): Promise<Database> => {
  const mongoConnectionUri: string = match<string, string>([
    { if: 'production', then: settings.mongodb.connectionUri.prod },
    { if: 'development', then: settings.mongodb.connectionUri.dev },
  ])(process.env.NODE_ENV);

  const mongooseConnectionOptions: mongoose.ConnectionOptions = {
    ...connectionOptions,
    authSource: 'admin',
    user: process.env.MONGODB_USERNAME,
    pass: process.env.MONGODB_PASSWORD,
  };

  const mongooseDatabase: Database = await mongoose.connect(
    mongoConnectionUri,
    mongooseConnectionOptions,
  );

  return mongooseDatabase;
};

export const startTestingMongoDBConnection = async (): Promise<
  [Database, DatabaseServer]
> => {
  const mongoMemoryServer: MongoMemoryServer = new MongoMemoryServer();
  const mongoConnectionUri: string = await mongoMemoryServer.getConnectionString();

  const mongooseConnectionOptions: mongoose.ConnectionOptions = {
    ...connectionOptions,
  };

  const mongooseDatabase: Database = await mongoose.connect(
    mongoConnectionUri,
    mongooseConnectionOptions,
  );

  return [mongooseDatabase, mongoMemoryServer];
};

export const closeMongoDBConnection = async (
  database: Database,
): Promise<void> => {
  await database.disconnect();
};

export const closeTestingMongoDBConnection = async (
  database: Database,
  databaseServer: DatabaseServer,
): Promise<void> => {
  await database.disconnect();
  await databaseServer.stop();
};

export const dropMongoDBDatabase = (database: Database): Promise<void> => {
  return database.connection.db.dropDatabase();
};
