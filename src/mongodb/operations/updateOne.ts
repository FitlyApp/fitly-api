import { Model, Document } from 'mongoose';
import { flatten } from 'flat';
import { normalizeDocument } from '../utilities';

type UpdateOneOptions<T> = {
  query: Query;
  document: T;
};

type Query = any;

const updateOne = <T>(model: Model<Document>) => async (
  options: UpdateOneOptions<T>,
): Promise<T> => {
  const { query, document } = options;
  const { id } = query;

  const flattenedDocument = flatten(document);

  const configuration = {
    new: true,
    omitUndefined: true,
  };

  let updatedDocument;

  if (id) {
    updatedDocument = await model.findByIdAndUpdate(
      id,
      flattenedDocument,
      configuration,
    );
  } else {
    updatedDocument = await model.findOneAndUpdate(
      query,
      flattenedDocument,
      configuration,
    );
  }

  return updatedDocument ? normalizeDocument(updatedDocument) : null;
};

export default updateOne;
