import { createError } from 'apollo-errors';

export const AuthenticationError = createError('AuthenticationError', {
  message: 'Unauthenticated',
});

export const AuthorizationError = createError('AuthorizationError', {
  message: 'Unauthorized',
});

export const ForbiddenError = createError('ForbiddenError', {
  message: 'Forbidden',
});

export const UnprocessableEntityError = createError(
  'UnprocessableEntityError',
  {
    message: 'Unprocessable Entity',
  },
);

export const ConflictError = createError('ConflictError', {
  message: 'Conflict',
});

export const NotFoundError = createError('NotFoundError', {
  message: 'Not Found',
});
