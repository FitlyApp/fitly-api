export default {
  server: {
    port: 3000,
  },
  mongodb: {
    connectionUri: {
      dev: 'mongodb://localhost/fitly',
      prod: 'mongodb://mongodb/fitly',
    },
  },
};
