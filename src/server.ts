import { Http2Server } from 'http2';
import { GraphQLServer, PubSub, Options } from 'graphql-yoga';
import { formatError } from 'apollo-errors';
import * as _ from 'lodash';
import { typeDefs, resolvers } from './graphql';
import {
  usersLoader,
  eventsLoader,
  postsLoader,
  eventCategoriesLoader,
} from './dataloaders';
import settings from './settings';
import { Server } from './types';

export const startGraphQLServer = async (): Promise<Server> => {
  const options: Options = {
    port: settings.server.port,
    endpoint: '/graphql',
    playground: '/playground',
    formatError,
  };

  const pubsub = new PubSub();

  const graphqlServer: GraphQLServer = new GraphQLServer({
    typeDefs,
    resolvers,
    context: (request) => ({
      ...request,
      pubsub,
      dataloaders: {
        usersLoader: usersLoader(),
        eventsLoader: eventsLoader(),
        postsLoader: postsLoader(),
        eventCategoriesLoader: eventCategoriesLoader(),
      },
    }),
  });

  const httpServer: Http2Server = await graphqlServer.start(options);

  return { httpServer, options };
};

export const startTestingGraphQLServer = async (): Promise<Server> => {
  const options: Options = {
    port: _.random(3000, 9000),
    endpoint: '/graphql',
    playground: '/playground',
  };

  const pubsub = new PubSub();

  const graphqlServer: GraphQLServer = new GraphQLServer({
    typeDefs,
    resolvers,
    context: (request) => ({
      ...request,
      pubsub,
    }),
  });

  const httpServer: Http2Server = await graphqlServer.start(options);

  return { httpServer, options };
};

export const closeGraphQLServer = (server: Server): void => {
  server.httpServer.close();
};

export const closeTestingGraphQLServer = (server: Server): void => {
  server.httpServer.close();
};
