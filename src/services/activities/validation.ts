import * as _ from 'lodash';
import { validate, isValidMongoID } from '../../utilities';
import {
  ValidationResult,
  Activity,
  ActivityObjectType,
  ActivityVerb,
} from '../../types';

const objectTypes: ActivityObjectType[] = ['Event', 'Post'];

const verbs: ActivityVerb[] = ['CREATED_EVENT', 'CREATED_POST'];

export const validateActivity = (query: Activity): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid activity ID: ${id}`,
    },
    actorId: {
      validationFunction: isValidMongoID,
      invalidMessage: (actorId) => `Invalid activity actor ID: ${actorId}`,
    },
    objectId: {
      validationFunction: isValidMongoID,
      invalidMessage: (objectId) => `Invalid activity object ID: ${objectId}`,
    },
    objectType: {
      validationFunction: (objectType) => _.includes(objectTypes, objectType),
      invalidMessage: (objectType) =>
        `Invalid activity object type: ${objectType}`,
    },
    verb: {
      validationFunction: (verb) => _.includes(verbs, verb),
      invalidMessage: (verb) => `Invalid activity verb: ${verb}`,
    },
  })(query);
};
