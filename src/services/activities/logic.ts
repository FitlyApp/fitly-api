import * as _ from 'lodash';
import model from './model';
import { validateActivity } from './validation';
import { getOne, getMany, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, NotFoundError } from '../../errors';
import { nowMillis } from '../../utilities';
import { Activity, ActivityVerb, ValidationResult } from '../../types';

export const getActivity = (query: Activity): Promise<Activity> => {
  const validationResult: ValidationResult = validateActivity(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchActivities = (query: {
  actorId?: string;
  verb?: ActivityVerb;
}): Promise<Activity[]> => {
  const validationResult: ValidationResult = validateActivity(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getMany(model)({
    query,
    sorting: {
      createdAt: 'DESCENDING',
    },
  });
};

export const createActivity = async (
  newActivity: Activity,
): Promise<Activity> => {
  const validationResult: ValidationResult = validateActivity(newActivity);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const createdActivity = await createOne(model)({
    document: {
      ...newActivity,
      createdAt: nowMillis(),
    },
  });

  return createdActivity;
};

export const deleteActivity = async (query: Activity): Promise<Activity> => {
  const validationResult: ValidationResult = validateActivity(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const activity: Activity = await getOne(model)({ query });

  if (_.isNull(activity)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No activity matches the selected query',
        userMessage: 'Cannot find this activity',
      },
    });
  }

  return deleteOne(model)({ query });
};
