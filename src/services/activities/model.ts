import { model, Schema, Model, Document } from 'mongoose';

const activitySchema: Schema = new Schema({
  actorId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  objectId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  // TODO: Use enums on database level
  objectType: {
    type: String,
    required: true,
  },
  // TODO: Use enums on database level
  verb: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const activityModel: Model<Document> = model(
  'Activity',
  activitySchema,
  'activities',
);

export default activityModel;
