import * as _ from 'lodash';
import model from './model';
import { validateQuery } from './validation';
import { getUsersByIds } from '../users';
import { getOne, getMany, count, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, ConflictError } from '../../errors';
import { nowMillis } from '../../utilities';
import { ValidationResult, User, EventHosting } from '../../types';

export const isHosting = async (
  userId: string,
  eventId: string,
): Promise<boolean> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventHostingDocument: EventHosting = await getOne(model)({
    query: {
      userId,
      eventId,
    },
  });

  return !_.isNull(eventHostingDocument);
};

export const getHosts = async (eventId: string): Promise<User[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const hostsIds: string[] = await getHostsIds(eventId);
  return getUsersByIds(hostsIds);
};

export const getExternalHosts = async (eventId: string): Promise<User[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventHostingDocuments: EventHosting[] = await getMany(model)({
    query: {
      eventId,
      userId: null,
    },
  });

  return eventHostingDocuments.map((document) => document.externalUser);
};

export const getHostsIds = async (eventId: string): Promise<string[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventHostingDocuments: EventHosting[] = await getMany(model)({
    query: {
      eventId,
      externalUser: null,
    },
  });

  return eventHostingDocuments.map((document) => document.userId);
};

export const getHostsCount = async (eventId: string): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      eventId,
      externalUser: null,
    },
  });
};

export const getExternalHostsCount = async (
  eventId: string,
): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      eventId,
      userId: null,
    },
  });
};

export const hostEvent = async (
  userId: string,
  eventId: string,
): Promise<EventHosting> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyHosting: boolean = await isHosting(userId, eventId);

  if (isAlreadyHosting) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer is already hosting this event: ${eventId}`,
        userMessage: 'You are already hosting this event',
      },
    });
  }

  return createOne(model)({
    document: {
      userId,
      eventId,
      externalUser: null,
      createdAt: nowMillis(),
    },
  });
};

export const unhostEvent = async (
  userId: string,
  eventId: string,
): Promise<EventHosting> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyHosting: boolean = await isHosting(userId, eventId);

  if (!isAlreadyHosting) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer is already not hosting this event: ${eventId}`,
        userMessage: 'You are already not hosting this event',
      },
    });
  }

  return deleteOne(model)({
    query: {
      userId,
      eventId,
    },
  });
};
