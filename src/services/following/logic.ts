import * as _ from 'lodash';
import model from './model';
import { validateQuery } from './validation';
import { getUsersByIds } from '../users';
import { getOne, getMany, count, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, ConflictError } from '../../errors';
import { nowMillis } from '../../utilities';
import { ValidationResult, User, Following } from '../../types';

export const isFollowing = async (
  followerId: string,
  followeeId: string,
): Promise<boolean> => {
  const validationResult: ValidationResult = validateQuery({
    followerId,
    followeeId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const followingDocument: Following = await getOne(model)({
    query: {
      followerId,
      followeeId,
    },
  });

  return !_.isNull(followingDocument);
};

export const getFollowers = async (userId: string): Promise<User[]> => {
  const validationResult: ValidationResult = validateQuery({
    followeeId: userId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const followersIds: string[] = await getFollowersIds(userId);
  return getUsersByIds(followersIds);
};

export const getFollowees = async (userId: string): Promise<User[]> => {
  const validationResult: ValidationResult = validateQuery({
    followerId: userId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const followeesIds: string[] = await getFolloweesIds(userId);
  return getUsersByIds(followeesIds);
};

export const getFollowersIds = async (userId: string): Promise<string[]> => {
  const validationResult: ValidationResult = validateQuery({
    followeeId: userId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const followingDocuments: Following[] = await getMany(model)({
    query: {
      followeeId: userId,
    },
  });

  return followingDocuments.map((document) => document.followerId);
};

export const getFolloweesIds = async (userId: string): Promise<string[]> => {
  const validationResult: ValidationResult = validateQuery({
    followerId: userId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const followingDocuments: Following[] = await getMany(model)({
    query: {
      followerId: userId,
    },
  });

  return followingDocuments.map((document) => document.followeeId);
};

export const getFollowersCount = async (userId: string): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    followeeId: userId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      followeeId: userId,
    },
  });
};

export const getFolloweesCount = async (userId: string): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    followerId: userId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      followerId: userId,
    },
  });
};

export const followUser = async (
  followerId: string,
  followeeId: string,
): Promise<Following> => {
  const validationResult: ValidationResult = validateQuery({
    followerId,
    followeeId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyFollowed: boolean = await isFollowing(followerId, followeeId);

  if (isAlreadyFollowed) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already follows this user: ${followeeId}`,
        userMessage: 'You already follow this user',
      },
    });
  }

  return createOne(model)({
    document: {
      followerId,
      followeeId,
      createdAt: nowMillis(),
    },
  });
};

export const unfollowUser = async (
  followerId: string,
  followeeId: string,
): Promise<Following> => {
  const validationResult: ValidationResult = validateQuery({
    followerId,
    followeeId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyFollowed: boolean = await isFollowing(followerId, followeeId);

  if (!isAlreadyFollowed) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already unfollows this user: ${followeeId}`,
        userMessage: 'You already unfollow this user',
      },
    });
  }

  return deleteOne(model)({
    query: {
      followerId,
      followeeId,
    },
  });
};
