import * as _ from 'lodash';
import { validate, isValidMongoID } from '../../utilities';
import { Following, ValidationResult } from '../../types';

export const validateQuery = (query: Following): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid following ID: ${id}`,
    },
    followerId: {
      validationFunction: isValidMongoID,
      invalidMessage: (followerId) => `Invalid follower user ID: ${followerId}`,
    },
    followeeId: {
      validationFunction: isValidMongoID,
      invalidMessage: (followeeId) => `Invalid followee user ID: ${followeeId}`,
    },
  })(query);
};
