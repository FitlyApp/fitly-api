import { model, Schema, Model, Document } from 'mongoose';

const followingSchema: Schema = new Schema({
  followerId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  followeeId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const followingModel: Model<Document> = model(
  'Following',
  followingSchema,
  'following',
);

export default followingModel;
