import * as _ from 'lodash';
import { validate, isValidMongoID, isValidUrl } from '../../utilities';
import { ValidationResult, EventCategory } from '../../types';

export const validateEventCategory = (
  eventCategory: EventCategory,
): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid event category ID: ${id}`,
    },
    imageUrl: {
      validationFunction: isValidUrl,
      invalidMessage: (imageUrl) =>
        `Invalid event category image URL: ${imageUrl}`,
    },
  })(eventCategory);
};
