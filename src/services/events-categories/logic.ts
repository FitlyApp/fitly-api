import * as _ from 'lodash';
import model from './model';
import { validateEventCategory } from './validation';
import { getOne, getMany, createOne, updateOne } from '../../mongodb';
import {
  UnprocessableEntityError,
  NotFoundError,
  ConflictError,
} from '../../errors';
import { nowMillis } from '../../utilities';
import { EventCategory, ValidationResult } from '../../types';

export const getEventCategory = (
  query: EventCategory,
): Promise<EventCategory> => {
  const validationResult: ValidationResult = validateEventCategory(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchEventCategories = (query: {
  title?: string;
}): Promise<EventCategory[]> => {
  const validationResult: ValidationResult = validateEventCategory(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getMany(model)({ query });
};

export const createEventCategory = async (
  newEventCategory: EventCategory,
): Promise<EventCategory> => {
  const validationResult: ValidationResult = validateEventCategory(
    newEventCategory,
  );

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const createdEventCategory = await createOne(model)({
    document: {
      ...newEventCategory,
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
    },
  });

  return createdEventCategory;
};

export const updateEventCategory = async (
  query: EventCategory,
  updatedEventCategory: EventCategory,
): Promise<EventCategory> => {
  const queryValidationResult: ValidationResult = validateEventCategory(query);

  if (!queryValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: queryValidationResult.message,
        userMessage: null,
      },
    });
  }

  const eventValidationResult: ValidationResult = validateEventCategory(
    updatedEventCategory,
  );

  if (!eventValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: eventValidationResult.message,
        userMessage: null,
      },
    });
  }

  const existingEventCategory: EventCategory = await getOne(model)({ query });

  if (_.isNull(existingEventCategory)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No event category matches the selected query',
        userMessage: 'Cannot find this event category',
      },
    });
  }

  return updateOne(model)({
    query,
    document: {
      ...updatedEventCategory,
      updatedAt: nowMillis(),
    },
  });
};

export const deleteEventCategory = async (
  query: EventCategory,
): Promise<EventCategory> => {
  const validationResult: ValidationResult = validateEventCategory(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventCategory: EventCategory = await getOne(model)({ query });

  if (_.isNull(eventCategory)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No event category matches the selected query',
        userMessage: 'Cannot find this event category',
      },
    });
  }

  if (!_.isNull(eventCategory.deletedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `Event category is already deleted: ${eventCategory.id}`,
        userMessage: 'This event category is already deleted',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      deletedAt: timestamp,
    },
  });
};
