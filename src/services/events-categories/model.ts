import { model, Schema, Model, Document } from 'mongoose';

const eventCategorySchema: Schema = new Schema({
  title: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
  updatedAt: {
    type: Number,
  },
  deletedAt: {
    type: Number,
  },
});

const eventCategoryModel: Model<Document> = model(
  'EventCategory',
  eventCategorySchema,
  'event-categories',
);

export default eventCategoryModel;
