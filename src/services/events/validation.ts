import * as _ from 'lodash';
import {
  validate,
  isValidMongoID,
  isValidUrl,
  isValidUnixMillisTimestamp,
  isValidLongitude,
  isValidLatitude,
} from '../../utilities';
import { ValidationResult, Event } from '../../types';

const externalOriginNames = ['MEETUP'];

const currencies = ['USD'];

export const validateQuery = (query: Event): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid event ID: ${id}`,
    },
  })(query);
};

export const validateEvent = (event: Event): ValidationResult => {
  return validate({
    'externalOrigin.name': {
      validationFunction: (externalOriginName) =>
        _.includes(externalOriginNames, externalOriginName),
      invalidMessage: (externalOriginName) =>
        `Invalid event external origin name: ${externalOriginName}`,
    },
    'externalOrigin.url': {
      validationFunction: isValidUrl,
      invalidMessage: (url) => `Invalid event external origin URL: ${url}`,
    },
    startsAt: {
      validationFunction: isValidUnixMillisTimestamp,
      invalidMessage: (startsAt) =>
        `Invalid event starting timestamp: ${startsAt}`,
    },
    endsAt: {
      validationFunction: isValidUnixMillisTimestamp,
      invalidMessage: (endsAt) => `Invalid event ending timestamp: ${endsAt}`,
    },
    'cost.amount': {
      validationFunction: (amount) => amount >= 0,
      invalidMessage: (amount) => `Invalid event cost amount: ${amount}`,
    },
    'cost.currency': {
      validationFunction: (currency) => _.includes(currencies, currency),
      invalidMessage: (currency) => `Invalid event cost currency: ${currency}`,
    },
    categoryId: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid event category ID: ${id}`,
    },
    imageUrl: {
      validationFunction: isValidUrl,
      invalidMessage: (imageUrl) => `Invalid event image URL: ${imageUrl}`,
    },
    'venue.location.coordinates[0]': {
      validationFunction: isValidLongitude,
      invalidMessage: (longitude) =>
        `Invalid event venue location longitude: ${longitude}`,
    },
    'venue.location.coordinates[1]': {
      validationFunction: isValidLatitude,
      invalidMessage: (latitude) =>
        `Invalid event venue location latitude: ${latitude}`,
    },
  })(event);
};
