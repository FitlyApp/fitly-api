import { model, Schema, Model, Document } from 'mongoose';

const eventSchema: Schema = new Schema({
  externalOrigin: {
    name: {
      type: String,
    },
    id: {
      type: String,
    },
    url: {
      type: String,
    },
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  startsAt: {
    type: Number,
    required: true,
  },
  endsAt: {
    type: Number,
    required: true,
  },
  isPublic: {
    type: Boolean,
    required: true,
  },
  cost: {
    amount: {
      type: Number,
      required: true,
    },
    currency: {
      type: String,
      required: true,
    },
  },
  categoryId: {
    type: Schema.Types.ObjectId,
    ref: 'EventCategory',
  },
  imageUrl: {
    type: String,
  },
  venue: {
    name: {
      type: String,
    },
    address: {
      type: String,
    },
    location: {
      type: {
        type: String,
      },
      coordinates: {
        type: [Number],
      },
    },
  },
  createdAt: {
    type: Number,
    required: true,
  },
  updatedAt: {
    type: Number,
  },
  deletedAt: {
    type: Number,
  },
});

eventSchema.index({ 'venue.location': '2dsphere' });

const eventModel: Model<Document> = model('Event', eventSchema, 'events');

export default eventModel;
