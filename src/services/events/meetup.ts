import * as puppeteer from 'puppeteer';
import axios from 'axios';
import * as $ from 'cheerio';
import * as moment from 'moment';
import * as Bluebird from 'bluebird';
import * as _ from 'lodash';
import { getEvent } from './logic';
import eventModel from '../events/model';
import eventAttendingModel from '../events-attending/model';
import { createMany } from '../../mongodb';
import { R, match, nowMillis } from '../../utilities';
import {
  Event,
  EventAttending,
  ExternalOrigin,
  Cost,
  Location,
  Venue,
} from '../../types';

export const fetchMeetupEvents = (city: string): Promise<Event[]> => {
  return R.pipeP(
    getEventUrls,
    getEventHtmls,
    parseEventHtmls,
    rejectPartialEvents,
    rejectExistingEvents,
    insertEvents,
    getEventRsvpUrls,
    getEventRsvpHtmls,
    parseEventRsvpHtmls,
    flattenEventRsvps,
    insertEventRsvps,
  )(city);
};

const getEventUrls = async (city: string): Promise<string[]> => {
  /* Set up browser */
  logWithTimestamp('Launching browser to scrape events...');

  const browser = await puppeteer.launch(getBrowserConfiguration());
  const page = await browser.newPage();

  /* Browse to Meetup's login page */
  logWithTimestamp('Navigating to login page...');

  await page.goto('https://secure.meetup.com/login/');

  /* Enter email */
  logWithTimestamp('Entering email...');

  await page.focus('#email');
  await page.keyboard.type('forebole@gustr.com', keyboardTypingOptions);
  await page.waitFor(1000);

  /* Enter password */
  logWithTimestamp('Entering password...');

  await page.focus('#password');
  await page.keyboard.type('123456', keyboardTypingOptions);
  await page.waitFor(1000);

  /* Submit login form  */
  logWithTimestamp('Clicking on submit...');

  await page.click('input[type="submit"]', mouseClickingOptions);
  await page.waitFor(5000);

  /* Click on Explore tab  */
  logWithTimestamp('Clicking on explore...');

  await page.click(
    '#globalNav > ul > li:nth-child(3) > a',
    mouseClickingOptions,
  );
  await page.waitFor(2500);

  /* Specify events category (Sports & Fitness) */
  logWithTimestamp('Choosing category...');

  await page.click('#mainKeywords', mouseClickingOptions);
  await page.waitFor(2000);
  await page.click(
    '#simple-criteria > li:nth-child(4) > ul > li:nth-child(3)',
    mouseClickingOptions,
  );
  await page.waitFor(1000);

  /* Specify search radius (50 miles) */
  logWithTimestamp('Choosing search radius...');

  await page.click('#searchForm > div:nth-child(5) > a', mouseClickingOptions);
  await page.waitFor(2000);
  await page.click('#simple-radius > li:nth-child(5)', mouseClickingOptions);
  await page.waitFor(1000);

  /* Specify city */
  logWithTimestamp('Choosing city...');

  await page.click(
    '#searchForm > div.dropdown.callout.center.location-display > a',
    mouseClickingOptions,
  );
  await page.focus('#locationSearch');
  await page.keyboard.type(city, keyboardTypingOptions);
  await page.keyboard.press('Enter');
  await page.waitFor(2500);

  /* Extract data */
  logWithTimestamp('Getting event URLs...');

  const eventListHtml = await page.content();

  const eventUrls = $(
    'a.resetLink.big.event.wrapNice.omnCamp.omngj_sj7emsa.omnrv_fe1msa',
    eventListHtml,
  )
    .map((_, element: any) => element.attribs.href)
    .get();

  await browser.close();

  return eventUrls;
};

const getEventHtmls = (urls: string[]): Promise<string[]> => {
  logWithTimestamp('Fetching event HTMLs...');

  return R.pipeP(
    /* FIXME: Declutter into pipable operations */
    (urls: string[]) => axios.all(R.map(axios.get)(urls)),
    R.map(R.prop('data')),
  )(urls);
};

const parseEventHtmls = (htmls: string[]): (Event | null)[] => {
  logWithTimestamp('Parsing event HTMLs...');

  return htmls.map((html) => {
    const metadata = JSON.parse(
      $('script[type="application/ld+json"]', html).get()[0].children[0].data,
    );

    /* Perform prelimenary checks */
    if (
      /**
       * The absence of any of the following properties
       * does not constitute a proper event.
       */
      R.any(R.isNil)([
        R.path(['name'])(metadata),
        R.path(['url'])(metadata),
        R.path(['startDate'])(metadata),
        R.path(['endDate'])(metadata),
        R.path(['offers'])(metadata),
        R.path(['location', 'name'])(metadata),
        R.path(['location', 'address', 'streetAddress'])(metadata),
        R.path(['location', 'address', 'addressLocality'])(metadata),
        R.path(['location', 'address', 'addressRegion'])(metadata),
        R.path(['location', 'address', 'addressCountry'])(metadata),
        R.path(['location', 'geo', 'longitude'])(metadata),
        R.path(['location', 'geo', 'latitude'])(metadata),
      ])
    ) {
      return null;
    }

    /* Extract external origin */
    const externalOriginName: string = 'MEETUP';
    const externalOriginUrl: string = metadata.url;
    const externalOriginId: string = externalOriginUrl.split('/')[5];

    const externalOrigin: ExternalOrigin = {
      name: externalOriginName,
      id: externalOriginId,
      url: externalOriginUrl,
    };

    /* Extract title */
    const title: string = metadata.name.trim();

    /* Extract description */
    const description: string = $(
      'div.event-description.runningText > p',
      html,
    ).html();

    /* If a Meetup event is fetchable, then it is public */
    const isPublic: boolean = true;

    /* Meetup events cannot (yet) be assigned internal categories */
    const categoryId: string | null = null;

    /* Extract image */
    const imageDivSelector = $(
      'div.photoCarousel-photoContainer.keepAspect--16-9',
      html,
    );

    const doesImageDivExist: boolean = imageDivSelector.length === 0;

    const imageUrl: string | null = doesImageDivExist
      ? null
      : imageDivSelector
          .attr('style')
          .replace('background-image:url(', '')
          .replace(')', '');

    /* Extract start datetime */
    const startsAt: number = moment.utc(metadata.startDate).valueOf();

    /* Extract end datetime */
    const endsAt: number = moment.utc(metadata.endDate).valueOf();

    /* Extract cost */
    const cost: Cost = {
      amount: Number(metadata.offers.price),
      currency: metadata.offers.priceCurrency,
    };

    /* Extract venue */
    const venueName: string = metadata.location.name;

    const venueAddress: string = [
      metadata.location.address.streetAddress,
      metadata.location.address.addressLocality,
      metadata.location.address.addressRegion,
      metadata.location.address.addressCountry,
    ].join(', ');

    const venueLocation: Location = {
      type: 'Point',
      coordinates: [
        metadata.location.geo.longitude,
        metadata.location.geo.latitude,
      ],
    };

    const venue: Venue = {
      name: venueName,
      address: venueAddress,
      location: venueLocation,
    };

    const event: Event = {
      externalOrigin,
      title,
      description,
      isPublic,
      categoryId,
      imageUrl,
      startsAt,
      endsAt,
      cost,
      venue,
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
    };

    return event;
  });
};

const rejectPartialEvents = (events: (Event | null)[]): Event[] => {
  logWithTimestamp('Filtering out partial events...');

  return R.reject(R.isNil)(events);
};

const rejectExistingEvents = (events: Event[]): Promise<Event[]> => {
  logWithTimestamp('Filtering out existing events...');

  return Bluebird.filter(events, async (event) => {
    const existingEvent = await getEvent({
      externalOrigin: {
        name: event.externalOrigin.name,
        id: event.externalOrigin.id,
      },
    });

    return !Boolean(existingEvent);
  });
};

const insertEvents = (events: Event[]): Promise<Event[]> => {
  logWithTimestamp('Inserting events...');

  return createMany(eventModel)({ documents: events });
};

const getEventRsvpUrls = (events: Event[]): [Event[], string[]] => {
  logWithTimestamp('Extracting RSVP URLs...');

  const urls = events.map((event) => {
    return event.externalOrigin.url.concat('attendees/');
  });

  return [events, urls];
};

const getEventRsvpHtmls = async ([events, urls]: [Event[], string[]]): Promise<
  [Event[], string[]]
> => {
  logWithTimestamp('Launching browser to scrape RSVPs...');

  const browser = await puppeteer.launch(getBrowserConfiguration());

  logWithTimestamp('Getting RSVPs...');

  const htmls = await Bluebird.map(
    urls,
    async (url, index) => {
      logWithTimestamp('Getting RSVPs for:', url);

      const page = await browser.newPage();

      await page.goto(url);
      await page.waitForSelector('span.avatar.avatar--large.avatar--person');

      const content = await page.content();

      await page.close();

      logWithTimestamp('Got RSVPs for:', url);

      return content;
    },
    { concurrency: 1 },
  );

  await browser.close();

  return [events, htmls];
};

const parseEventRsvpHtmls = ([events, htmls]: [Event[], string[]]): [
  Event[],
  EventAttending[][],
] => {
  logWithTimestamp('Parsing RSVP HTMLs...');

  const rsvps = events.map((event, index) => {
    const html = htmls[index];

    const rsvps = $('span.avatar.avatar--large.avatar--person', html)
      .map((index, element: any) => {
        const externalUserId = String(_.random(10000, 99999, false));
        const externalUserFullName = element.attribs['aria-label'];
        const externalUserAvatarUrl = element.attribs['style']
          ? element.attribs['style']
              .replace('background-image: url("', '')
              .replace('");', '')
          : null;

        return {
          eventId: event.id,
          userId: null,
          externalUser: {
            id: externalUserId,
            fullName: externalUserFullName,
            avatarUrl: externalUserAvatarUrl,
          },
          createdAt: nowMillis(),
        };
      })
      .get();

    return rsvps;
  });

  return [events, rsvps];
};

const flattenEventRsvps = ([events, rsvps]: [
  Event[],
  EventAttending[][],
]): EventAttending[] => {
  logWithTimestamp('Flattening RSVPs...');

  const flattenedRsvps = R.flatten(rsvps);
  return [events, flattenedRsvps];
};

const insertEventRsvps = async ([events, rsvps]: [
  Event[],
  EventAttending[],
]): Promise<Event[]> => {
  logWithTimestamp('Inserting RSVPs...');

  await createMany(eventAttendingModel)({ documents: rsvps });

  logWithTimestamp('Done!');

  return events;
};

const logWithTimestamp = (message: any, ...messages: any[]): void => {
  const allMessages = [message, messages];

  console.log(
    `${moment().format('MMM D YYYY HH:mm:ss:SSS Z')}: ${allMessages.join(' ')}`,
  );
};

const getBrowserConfiguration = () => {
  return match([
    {
      if: 'development',
      then: {
        headless: false,
        defaultViewport: { width: 1200, height: 1000 },
      },
    },
    {
      if: 'production',
      then: {
        executablePath: '/usr/bin/chromium-browser',
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-gpu',
          '--disable-dev-shm-usage',
        ],
      },
    },
  ])(process.env.NODE_ENV);
};

const keyboardTypingOptions = {
  /**
   *  The average male typing speed is 44 words per minute.
   *  A word has 5 characters on average, which makes the
   *  average male typing speed 220 characters per minute.
   *  Dividing by the number of seconds in a minute, we get
   *  3.666666667 characters per second. Finally, to get the
   *  delay between typing characters, we take the multiplicative
   *  inverse, which yields 0.272727273 seconds. Converting that
   *  into milliseconds, we get 272.727273.
   */
  delay: 273,
};

const mouseClickingOptions = {
  delay: 100,
};
