import * as _ from 'lodash';
import * as merge from 'deepmerge';
import model from './model';
import { validateQuery, validateEvent } from './validation';
import { getOne, getMany, createOne, updateOne } from '../../mongodb';
import {
  UnprocessableEntityError,
  NotFoundError,
  ConflictError,
} from '../../errors';
import { nowMillis } from '../../utilities';
import { Event, ValidationResult } from '../../types';

export const getEvent = (query: Event): Promise<Event> => {
  const validationResult: ValidationResult = validateQuery(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchEvents = (query: {
  longitude: number;
  latitude: number;
  deletedAt?: number;
}): Promise<Event[]> => {
  const { longitude, latitude, deletedAt = null } = query;

  // TODO: Validate arguments

  const coordinates = [longitude, latitude];
  const maximumDistanceMeters = 50000;

  return getMany(model)({
    query: {
      'venue.location': {
        $near: {
          $geometry: {
            type: 'Point',
            coordinates: coordinates,
          },
          $maxDistance: maximumDistanceMeters,
        },
      },
      deletedAt,
    },
    sorting: {
      startsAt: 'ASCENDING',
    },
  });
};

export const searchEventsByTitle = async (query: {
  title: string;
  deletedAt?: number;
}): Promise<Event[]> => {
  const { title, deletedAt } = query;

  const matches = await getMany(model)({
    query: {
      title: new RegExp(title, 'i'),
      deletedAt,
    },
  });

  return matches;
};

export const createEvent = async (newEvent: Event): Promise<Event> => {
  const validationResult: ValidationResult = validateEvent(newEvent);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  if (newEvent.startsAt > newEvent.endsAt) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: `Event's starting time cannot come after its ending time`,
        userMessage: null,
      },
    });
  }

  const createdEvent = await createOne(model)({
    document: {
      ...newEvent,
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
    },
  });

  return createdEvent;
};

export const updateEvent = async (
  query: Event,
  updatedEvent: Event,
): Promise<Event> => {
  const queryValidationResult: ValidationResult = validateQuery(query);

  if (!queryValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: queryValidationResult.message,
        userMessage: null,
      },
    });
  }

  const eventValidationResult: ValidationResult = validateEvent(updatedEvent);

  if (!eventValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: eventValidationResult.message,
        userMessage: null,
      },
    });
  }

  const existingEvent: Event = await getOne(model)({ query });

  if (_.isNull(existingEvent)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No event matches the selected query',
        userMessage: 'Cannot find this event',
      },
    });
  }

  const draftEvent: Event = merge(existingEvent, updatedEvent);

  if (draftEvent.startsAt > draftEvent.endsAt) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: `Event's starting time cannot come after its ending time`,
        userMessage: null,
      },
    });
  }

  return updateOne(model)({
    query,
    document: {
      ...updatedEvent,
      updatedAt: nowMillis(),
    },
  });
};

export const deleteEvent = async (query: Event): Promise<Event> => {
  const validationResult: ValidationResult = validateQuery(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const event: Event = await getOne(model)({ query });

  if (_.isNull(event)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No event matches the selected query',
        userMessage: 'Cannot find this event',
      },
    });
  }

  if (!_.isNull(event.deletedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `Event is already deleted: ${event.id}`,
        userMessage: 'This event is already deleted',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      deletedAt: timestamp,
    },
  });
};
