import { model, Schema, Model, Document } from 'mongoose';

const matchingRequestSchema: Schema = new Schema({
  requesterId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  accepterId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  location: {
    type: {
      type: String,
    },
    coordinates: {
      type: [Number],
    },
  },
  workoutType: {
    type: String,
    required: true,
  },
  activityLevel: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
  updatedAt: {
    type: Number,
  },
  deletedAt: {
    type: Number,
  },
});

matchingRequestSchema.index({ location: '2dsphere' });

const matchingRequestModel: Model<Document> = model(
  'MatchingRequest',
  matchingRequestSchema,
  'matching-requests',
);

export default matchingRequestModel;
