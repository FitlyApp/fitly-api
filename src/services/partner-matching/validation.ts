import * as _ from 'lodash';
import {
  validate,
  isValidMongoID,
  isValidLongitude,
  isValidLatitude,
} from '../../utilities';
import {
  ValidationResult,
  MatchingRequest,
  MatchingRequestWorkoutType,
  MatchingRequestActivityLevel,
  MatchingRequestStatus,
} from '../../types';

const workoutTypes: MatchingRequestWorkoutType[] = [
  'CARDIO',
  'STRENGTH_TRAINING',
];

const activityLevels: MatchingRequestActivityLevel[] = [
  'COUCH_POTATO',
  'SLOTH',
  'WIMP',
  'BABY',
  'STUDENT',
  'DETERMINED',
  'ATHLETE',
  'CHAMP',
  'BEAST',
  'WARRIOR',
  'SPARTAN',
];

const statuses: MatchingRequestStatus[] = ['PENDING', 'COMPLETE', 'TIMED_OUT'];

export const validateMatchingRequest = (
  query: MatchingRequest,
): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid matching request ID: ${id}`,
    },
    requesterId: {
      validationFunction: isValidMongoID,
      invalidMessage: (requesterId) =>
        `Invalid matching request requester ID: ${requesterId}`,
    },
    accepterId: {
      validationFunction: isValidMongoID,
      invalidMessage: (accepterId) =>
        `Invalid matching request accepter ID: ${accepterId}`,
    },
    'location.coordinates[0]': {
      validationFunction: isValidLongitude,
      invalidMessage: (longitude) =>
        `Invalid matching request location longitude: ${longitude}`,
    },
    'location.coordinates[1]': {
      validationFunction: isValidLatitude,
      invalidMessage: (latitude) =>
        `Invalid matching request location latitude: ${latitude}`,
    },
    workoutType: {
      validationFunction: (workoutType) =>
        _.includes(workoutTypes, workoutType),
      invalidMessage: (workoutType) =>
        `Invalid matching request workout type: ${workoutType}`,
    },
    activityLevel: {
      validationFunction: (activityLevel) =>
        _.includes(activityLevels, activityLevel),
      invalidMessage: (activityLevel) =>
        `Invalid matching request activity level: ${activityLevel}`,
    },
    status: {
      validationFunction: (status) => _.includes(statuses, status),
      invalidMessage: (status) => `Invalid matching request status: ${status}`,
    },
  })(query);
};
