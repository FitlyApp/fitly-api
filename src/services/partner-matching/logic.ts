import * as _ from 'lodash';
import model from './model';
import { validateMatchingRequest } from './validation';
import { getOne, createOne, updateOne, getMany } from '../../mongodb';
import {
  UnprocessableEntityError,
  NotFoundError,
  ConflictError,
} from '../../errors';
import { nowMillis } from '../../utilities';
import {
  MatchingRequest,
  ValidationResult,
  MatchingRequestWorkoutType,
  MatchingRequestActivityLevel,
  Coordinates,
  MatchingRequestStatus,
} from '../../types';

export const getMatchingRequest = (
  query: MatchingRequest,
): Promise<MatchingRequest> => {
  const validationResult: ValidationResult = validateMatchingRequest(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchMatchingRequests = (query: {
  requesterId: string;
  coordinates: Coordinates;
  maximumDistanceMeters: number;
  workoutType: MatchingRequestWorkoutType;
  activityLevel: MatchingRequestActivityLevel;
  status: MatchingRequestStatus;
}): Promise<MatchingRequest[]> => {
  // TODO: Validate arguments
  // TODO: Honor activity level parameter

  const {
    requesterId,
    coordinates,
    maximumDistanceMeters,
    workoutType,
    activityLevel,
    status,
  } = query;

  return getMany(model)({
    query: {
      requesterId: {
        $ne: requesterId,
      },
      workoutType: workoutType,
      status: status,
      location: {
        $near: {
          $maxDistance: maximumDistanceMeters,
          $geometry: {
            type: 'Point',
            coordinates,
          },
        },
      },
    },
  });
};

export const createMatchingRequest = async (
  newMatchingRequest: MatchingRequest,
): Promise<MatchingRequest> => {
  const validationResult: ValidationResult = validateMatchingRequest(
    newMatchingRequest,
  );

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const createdMatchingRequest = await createOne(model)({
    document: {
      ...newMatchingRequest,
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
    },
  });

  return createdMatchingRequest;
};

export const updateMatchingRequest = async (
  query: MatchingRequest,
  updatedMatchingRequest: MatchingRequest,
): Promise<MatchingRequest> => {
  const queryValidationResult: ValidationResult = validateMatchingRequest(
    query,
  );

  if (!queryValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: queryValidationResult.message,
        userMessage: null,
      },
    });
  }

  const matchingRequestValidationResult: ValidationResult = validateMatchingRequest(
    updatedMatchingRequest,
  );

  if (!matchingRequestValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: matchingRequestValidationResult.message,
        userMessage: null,
      },
    });
  }

  const existingMatchingRequest: MatchingRequest = await getOne(model)({
    query,
  });

  if (_.isNull(existingMatchingRequest)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No matching request matches the selected query',
        userMessage: 'Cannot find this matching request',
      },
    });
  }

  return updateOne(model)({
    query,
    document: {
      ...updatedMatchingRequest,
      updatedAt: nowMillis(),
    },
  });
};

export const deleteMatchingRequest = async (
  query: MatchingRequest,
): Promise<MatchingRequest> => {
  const validationResult: ValidationResult = validateMatchingRequest(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const existingMatchingRequest: MatchingRequest = await getOne(model)({
    query,
  });

  if (_.isNull(existingMatchingRequest)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No matching request matches the selected query',
        userMessage: 'Cannot find this matching request',
      },
    });
  }

  if (!_.isNull(existingMatchingRequest.deletedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `Matching request is already deleted: ${existingMatchingRequest.id}`,
        userMessage: 'This matching request is already deleted',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      deletedAt: timestamp,
    },
  });
};
