import * as _ from 'lodash';
import { validate, isValidMongoID } from '../../utilities';
import { ValidationResult, EventLiking } from '../../types';

export const validateQuery = (query: EventLiking): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid event liking ID: ${id}`,
    },
    eventId: {
      validationFunction: isValidMongoID,
      invalidMessage: (eventId) => `Invalid event ID: ${eventId}`,
    },
    userId: {
      validationFunction: isValidMongoID,
      invalidMessage: (userId) => `Invalid user ID: ${userId}`,
    },
  })(query);
};
