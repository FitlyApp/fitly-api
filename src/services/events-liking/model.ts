import { model, Schema, Model, Document } from 'mongoose';

const eventsLikingSchema: Schema = new Schema({
  eventId: {
    type: Schema.Types.ObjectId,
    ref: 'Event',
    required: true,
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const eventsLikingModel: Model<Document> = model(
  'EventsLiking',
  eventsLikingSchema,
  'events-liking',
);

export default eventsLikingModel;
