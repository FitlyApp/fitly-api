import * as _ from 'lodash';
import model from './model';
import { validateQuery } from './validation';
import { getUsersByIds } from '../users';
import { getOne, getMany, count, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, ConflictError } from '../../errors';
import { nowMillis } from '../../utilities';
import { ValidationResult, User, EventLiking } from '../../types';

export const isLiking = async (
  userId: string,
  eventId: string,
): Promise<boolean> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventLikingDocument: EventLiking = await getOne(model)({
    query: {
      userId,
      eventId,
    },
  });

  return !_.isNull(eventLikingDocument);
};

export const getLikers = async (eventId: string): Promise<User[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const likersIds: string[] = await getLikersIds(eventId);
  return getUsersByIds(likersIds);
};

export const getLikersIds = async (eventId: string): Promise<string[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventLikingDocuments: EventLiking[] = await getMany(model)({
    query: {
      eventId,
    },
  });

  return eventLikingDocuments.map((document) => document.userId);
};

export const getLikersCount = async (eventId: string): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      eventId,
    },
  });
};

export const likeEvent = async (
  userId: string,
  eventId: string,
): Promise<EventLiking> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyLiking: boolean = await isLiking(userId, eventId);

  if (isAlreadyLiking) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already likes this event: ${eventId}`,
        userMessage: 'You already liked this event',
      },
    });
  }

  return createOne(model)({
    document: {
      userId,
      eventId,
      createdAt: nowMillis(),
    },
  });
};

export const unlikeEvent = async (
  userId: string,
  eventId: string,
): Promise<EventLiking> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyLiking: boolean = await isLiking(userId, eventId);

  if (!isAlreadyLiking) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already unlikes this event: ${eventId}`,
        userMessage: 'You already unliked this event',
      },
    });
  }

  return deleteOne(model)({
    query: {
      userId,
      eventId,
    },
  });
};
