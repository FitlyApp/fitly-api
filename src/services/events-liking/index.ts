export {
  isLiking,
  getLikers,
  getLikersIds,
  getLikersCount,
  likeEvent,
  unlikeEvent,
} from './logic';
