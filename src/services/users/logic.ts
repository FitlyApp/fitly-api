import * as _ from 'lodash';
import model from './model';
import { validateQuery, validateUser, validateId } from './validation';
import { getOne, getMany, createOne, updateOne } from '../../mongodb';
import {
  UnprocessableEntityError,
  AuthenticationError,
  NotFoundError,
  ConflictError,
} from '../../errors';
import { hashPassword, nowMillis } from '../../utilities';
import { User, ValidationResult } from '../../types';

export const getUser = (query: User): Promise<User> => {
  const validationResult: ValidationResult = validateQuery(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const getUsersByIds = (ids: string[]): Promise<User[]> => {
  ids.forEach((id) => {
    const validationResult: ValidationResult = validateId(id);

    if (!validationResult.isValid) {
      throw new UnprocessableEntityError({
        data: {
          developerMessage: validationResult.message,
          userMessage: null,
        },
      });
    }
  });

  return getMany(model)({
    query: {
      _id: {
        $in: ids,
      },
    },
  });
};

export const searchUsers = async (query: { name: string }): Promise<User[]> => {
  const { name } = query;

  /* TODO: Perform one query instead of two */
  const [firstNameMatches, lastNameMatches] = await Promise.all([
    getMany(model)({ query: { 'name.first': new RegExp(name, 'i') } }),
    getMany(model)({ query: { 'name.last': new RegExp(name, 'i') } }),
  ]);

  const matches = [...firstNameMatches, ...lastNameMatches];

  return _.uniqWith<User>(matches, (user1, user2) => {
    return user1.id === user2.id;
  });
};

export const createUser = async (newUser: User): Promise<User> => {
  const validationResult: ValidationResult = validateUser(newUser);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const existingUser = await getOne(model)({
    query: {
      email: newUser.email,
    },
  });

  if (!_.isNull(existingUser)) {
    throw new AuthenticationError({
      data: {
        developerMessage: `Email already exists: ${newUser.email}`,
        userMessage: 'This email is already being used',
      },
    });
  }

  if (newUser.authenticationMethod !== 'PASSWORD') {
    return createOne(model)({
      document: {
        ...newUser,
        createdAt: nowMillis(),
        updatedAt: null,
        deletedAt: null,
        bannedAt: null,
      },
    });
  }

  return createOne(model)({
    document: {
      ...newUser,
      passwordHash: await hashPassword(newUser.password),
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
      bannedAt: null,
    },
  });
};

export const updateUser = async (
  query: User,
  updatedUser: User,
): Promise<User> => {
  const queryValidationResult: ValidationResult = validateQuery(query);

  if (!queryValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: queryValidationResult.message,
        userMessage: null,
      },
    });
  }

  const userValidationResult: ValidationResult = validateUser(updatedUser);

  if (!userValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: userValidationResult.message,
        userMessage: null,
      },
    });
  }

  const user: User = await getOne(model)({ query });

  if (_.isNull(user)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No user matches the selected query',
        userMessage: 'Cannot find this user',
      },
    });
  }

  return updateOne(model)({
    query,
    document: {
      ...updatedUser,
      updatedAt: nowMillis(),
    },
  });
};

export const deleteUser = async (query: User): Promise<User> => {
  const validationResult: ValidationResult = validateQuery(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const user: User = await getOne(model)({ query });

  if (_.isNull(user)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No user matches the selected query',
        userMessage: 'Cannot find this user',
      },
    });
  }

  if (!_.isNull(user.deletedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `User is already deleted: ${user.id}`,
        userMessage: 'This user is already deleted',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      deletedAt: timestamp,
    },
  });
};

export const banUser = async (query: User): Promise<User> => {
  const validationResult: ValidationResult = validateQuery(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const user: User = await getOne(model)({ query });

  if (_.isNull(user)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No user matches the selected query',
        userMessage: 'Cannot find this user',
      },
    });
  }

  if (!_.isNull(user.bannedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `User is already banned: ${user.id}`,
        userMessage: 'This user is already banned',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      bannedAt: timestamp,
    },
  });
};
