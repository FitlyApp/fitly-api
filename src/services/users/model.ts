import { model, Schema, Model, Document } from 'mongoose';

const userSchema: Schema = new Schema({
  email: {
    type: String,
    required: true,
    index: { unique: true },
  },
  passwordHash: {
    type: String,
  },
  authenticationMethod: {
    type: String,
    required: true,
  },
  name: {
    first: {
      type: String,
      required: true,
    },
    last: {
      type: String,
      required: true,
    },
  },
  role: {
    type: String,
    required: true,
  },
  bio: {
    type: String,
  },
  avatarUrl: {
    type: String,
  },
  createdAt: {
    type: Number,
    required: true,
  },
  updatedAt: {
    type: Number,
  },
  deletedAt: {
    type: Number,
  },
  bannedAt: {
    type: Number,
  },
});

const userModel: Model<Document> = model('User', userSchema, 'users');

export default userModel;
