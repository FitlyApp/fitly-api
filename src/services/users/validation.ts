import * as _ from 'lodash';
import {
  validate,
  isValidMongoID,
  isValidEmail,
  isValidUrl,
} from '../../utilities';
import { ValidationResult, User, AuthenticationMethod } from '../../types';

const authenticationMethods: AuthenticationMethod[] = [
  'PASSWORD',
  'FACEBOOK',
  'GOOGLE',
];

export const validateQuery = (query: User): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid user ID: ${id}`,
    },
    email: {
      validationFunction: isValidEmail,
      invalidMessage: (email) => `Invalid email: ${email}`,
    },
    // TODO: Test
    authenticationMethod: {
      validationFunction: (authenticationMethod) =>
        _.includes(authenticationMethods, authenticationMethod),
      invalidMessage: (authenticationMethod) =>
        `Invalid authentication method: ${authenticationMethod}`,
    },
  })(query);
};

export const validateUser = (user: User): ValidationResult => {
  return validate({
    email: {
      validationFunction: isValidEmail,
      invalidMessage: (email) => `Invalid email: ${email}`,
    },
    password: {
      validationFunction: (password) => password.length >= 6,
      invalidMessage: () => 'Invalid password',
    },
    // TODO: Test
    authenticationMethod: {
      validationFunction: (authenticationMethod) =>
        _.includes(authenticationMethods, authenticationMethod),
      invalidMessage: (authenticationMethod) =>
        `Invalid authentication method: ${authenticationMethod}`,
    },
    avatarUrl: {
      validationFunction: isValidUrl,
      invalidMessage: (avatarUrl) => `Invalid avatar URL: ${avatarUrl}`,
    },
  })(user);
};

export const validateId = (id: string): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid user ID: ${id}`,
    },
  })({ id });
};
