import {
  validate,
  isValidMongoID,
  isValidExpoPushToken,
} from '../../utilities';
import {
  ValidationResult,
  PushNotification,
  PushNotificationsRegistration,
} from '../../types';

export const validatePushNotification = (
  pushNotification: PushNotification,
): ValidationResult => {
  return validate({
    userId: {
      validationFunction: isValidMongoID,
      invalidMessage: (userId) => `Invalid user ID: ${userId}`,
    },
  })(pushNotification);
};

export const validatePushNotificationsRegistration = (
  pushNotificationsRegistration: PushNotificationsRegistration,
): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) =>
        `Invalid push notification registration ID: ${id}`,
    },
    userId: {
      validationFunction: isValidMongoID,
      invalidMessage: (userId) => `Invalid user ID: ${userId}`,
    },
    pushToken: {
      validationFunction: isValidExpoPushToken,
      invalidMessage: (token) => `Invalid Expo push token: ${token}`,
    },
  })(pushNotificationsRegistration);
};
