import { model, Schema, Model, Document } from 'mongoose';

const pushNotificationsRegistrationSchema: Schema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  pushToken: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const pushNotificationsRegistrationModel: Model<Document> = model(
  'PushNotificationsRegistration',
  pushNotificationsRegistrationSchema,
  'push-notifications-registrations',
);

export default pushNotificationsRegistrationModel;
