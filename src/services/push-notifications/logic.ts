import * as _ from 'lodash';
import { Expo, ExpoPushMessage } from 'expo-server-sdk';
import model from './model';
import {
  validatePushNotification,
  validatePushNotificationsRegistration,
} from './validation';
import { getOne, getMany, createOne } from '../../mongodb';
import { UnprocessableEntityError, ConflictError } from '../../errors';
import { nowMillis } from '../../utilities';
import {
  PushNotification,
  PushNotificationsRegistration,
  ValidationResult,
} from '../../types';

export const sendPushNotification = async (
  pushNotification: PushNotification,
) => {
  const validationResult: ValidationResult = validatePushNotification(
    pushNotification,
  );

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const { userId, title, body, channelId } = pushNotification;

  const pushTokens: string[] = await getPushTokens(userId);

  const pushMessages: ExpoPushMessage[] = pushTokens.map((pushToken) => ({
    to: pushToken,
    title,
    body,
    channelId,
  }));

  const expo: Expo = new Expo();

  const pushMessagesChunks: ExpoPushMessage[][] = expo.chunkPushNotifications(
    pushMessages,
  );

  for (const pushMessagesChunk of pushMessagesChunks) {
    try {
      const ticketChunk = await expo.sendPushNotificationsAsync(
        pushMessagesChunk,
      );
    } catch (error) {
      console.error('ERROR >', error);
    }
  }

  /**
   *  TODO: Collect & handle tickets
   *  See: https://github.com/expo/expo-server-sdk-node/blob/master/README.md#usage
   */
};

export const registerForPushNotifications = async (
  newPushNotificationsRegistration: PushNotificationsRegistration,
): Promise<PushNotificationsRegistration> => {
  const validationResult: ValidationResult = validatePushNotificationsRegistration(
    newPushNotificationsRegistration,
  );

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyRegistered: boolean = await isRegisteredForPushNotifications({
    userId: newPushNotificationsRegistration.userId,
    pushToken: newPushNotificationsRegistration.pushToken,
  });

  if (isAlreadyRegistered) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer's device is already registered for push notifications`,
        userMessage: null,
      },
    });
  }

  const createdPushNotificationsRegistration = await createOne(model)({
    document: {
      ...newPushNotificationsRegistration,
      createdAt: nowMillis(),
    },
  });

  return createdPushNotificationsRegistration;
};

export const isRegisteredForPushNotifications = async (
  query: PushNotificationsRegistration,
): Promise<boolean> => {
  const validationResult: ValidationResult = validatePushNotificationsRegistration(
    query,
  );

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const pushNotificationsRegistrationDocument: PushNotificationsRegistration = await getOne(
    model,
  )({
    query,
  });

  return !_.isNull(pushNotificationsRegistrationDocument);
};

export const getPushTokens = async (userId: string): Promise<string[]> => {
  const validationResult: ValidationResult = validatePushNotificationsRegistration(
    { userId },
  );

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const pushNotificationDocuments: PushNotificationsRegistration[] = await getMany(
    model,
  )({
    query: {
      userId,
    },
  });

  return pushNotificationDocuments.map((document) => document.pushToken);
};
