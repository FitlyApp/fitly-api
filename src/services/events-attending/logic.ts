import * as _ from 'lodash';
import model from './model';
import { validateQuery } from './validation';
import { getUsersByIds } from '../users';
import { getOne, getMany, count, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, ConflictError } from '../../errors';
import { nowMillis } from '../../utilities';
import {
  ValidationResult,
  User,
  ExternalUser,
  EventAttending,
} from '../../types';

export const isAttending = async (
  userId: string,
  eventId: string,
): Promise<boolean> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventAttendingDocument: EventAttending = await getOne(model)({
    query: {
      userId,
      eventId,
    },
  });

  return !_.isNull(eventAttendingDocument);
};

export const getAttendees = async (eventId: string): Promise<User[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const attendeesIds: string[] = await getAttendeesIds(eventId);
  return getUsersByIds(attendeesIds);
};

export const getExternalAttendees = async (
  eventId: string,
): Promise<ExternalUser[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventAttendingDocuments: EventAttending[] = await getMany(model)({
    query: {
      eventId,
      userId: null,
    },
  });

  return eventAttendingDocuments.map((document) => document.externalUser);
};

export const getAttendeesIds = async (eventId: string): Promise<string[]> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const eventAttendingDocuments: EventAttending[] = await getMany(model)({
    query: {
      eventId,
      externalUser: null,
    },
  });

  return eventAttendingDocuments.map((document) => document.userId);
};

export const getAttendeesCount = async (eventId: string): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      eventId,
      externalUser: null,
    },
  });
};

export const getExternalAttendeesCount = async (
  eventId: string,
): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      eventId,
      userId: null,
    },
  });
};

export const attendEvent = async (
  userId: string,
  eventId: string,
): Promise<EventAttending> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyAttending: boolean = await isAttending(userId, eventId);

  if (isAlreadyAttending) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer is already attending this event: ${eventId}`,
        userMessage: 'You are already attending this event',
      },
    });
  }

  return createOne(model)({
    document: {
      userId,
      eventId,
      externalUser: null,
      createdAt: nowMillis(),
    },
  });
};

export const unattendEvent = async (
  userId: string,
  eventId: string,
): Promise<EventAttending> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    eventId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyAttending: boolean = await isAttending(userId, eventId);

  if (!isAlreadyAttending) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer is already not attending this event: ${eventId}`,
        userMessage: 'You are already not attending this event',
      },
    });
  }

  return deleteOne(model)({
    query: {
      userId,
      eventId,
    },
  });
};
