import * as _ from 'lodash';
import { validate, isValidMongoID, isValidUrl } from '../../utilities';
import { ValidationResult, EventAttending } from '../../types';

export const validateQuery = (query: EventAttending): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid event attending ID: ${id}`,
    },
    eventId: {
      validationFunction: isValidMongoID,
      invalidMessage: (eventId) => `Invalid event ID: ${eventId}`,
    },
    userId: {
      validationFunction: isValidMongoID,
      invalidMessage: (userId) => `Invalid user ID: ${userId}`,
    },
    'externalUser.imageUrl': {
      validationFunction: isValidUrl,
      invalidMessage: (imageUrl) =>
        `Invalid external user image URL: ${imageUrl}`,
    },
  })(query);
};
