import { model, Schema, Model, Document } from 'mongoose';

const eventsAttendingSchema: Schema = new Schema({
  eventId: {
    type: Schema.Types.ObjectId,
    ref: 'Event',
    required: true,
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  externalUser: {
    id: {
      type: String,
    },
    fullName: {
      type: String,
    },
    avatarUrl: {
      type: String,
    },
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const eventsAttendingModel: Model<Document> = model(
  'EventsAttending',
  eventsAttendingSchema,
  'events-attending',
);

export default eventsAttendingModel;
