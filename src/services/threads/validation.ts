import { validate, isValidMongoID } from '../../utilities';
import { ValidationResult, Thread } from '../../types';

export const validateThread = (query: Thread): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid thread ID: ${id}`,
    },
  })(query);
};
