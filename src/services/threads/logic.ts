import * as _ from 'lodash';
import model from './model';
import { validateThread } from './validation';
import { getOne, getMany, createOne, updateOne } from '../../mongodb';
import {
  UnprocessableEntityError,
  NotFoundError,
  ConflictError,
} from '../../errors';
import { nowMillis } from '../../utilities';
import { Thread, ValidationResult } from '../../types';

export const getThreadByParticipantsIds = (
  participantsIds: string[],
): Promise<Thread> => {
  const validationResult: ValidationResult = validateThread({
    participantsIds,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  /**
   *  FIXME: $all searches for an array that contains the
   *  specified elements. The problem is that the matched
   *  array might contain other elements, which is unintended.
   *  Another MongoDB query is needed to match the exact array
   *  but disregard the order of the elements.
   *
   *  See: https://docs.mongodb.com/manual/tutorial/query-arrays/
   */
  return getOne(model)({
    query: {
      participantsIds: {
        $all: participantsIds,
      },
    },
  });
};

export const getThread = (query: Thread): Promise<Thread> => {
  const validationResult: ValidationResult = validateThread(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchThreads = (query: {
  participantsIds: string | string[];
}): Promise<Thread[]> => {
  /* TODO: Validate */
  return getMany(model)({ query });
};

export const createThread = async (newThread: Thread): Promise<Thread> => {
  const validationResult: ValidationResult = validateThread(newThread);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const createdThread = await createOne(model)({
    document: {
      ...newThread,
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
    },
  });

  return createdThread;
};

export const updateThread = async (
  query: Thread,
  updatedThread: Thread,
): Promise<Thread> => {
  const queryValidationResult: ValidationResult = validateThread(query);

  if (!queryValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: queryValidationResult.message,
        userMessage: null,
      },
    });
  }

  const threadValidationResult: ValidationResult = validateThread(
    updatedThread,
  );

  if (!threadValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: threadValidationResult.message,
        userMessage: null,
      },
    });
  }

  const existingThread: Thread = await getOne(model)({ query });

  if (_.isNull(existingThread)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No thread matches the selected query',
        userMessage: 'Cannot find this thread',
      },
    });
  }

  return updateOne(model)({
    query,
    document: {
      ...updatedThread,
      updatedAt: nowMillis(),
    },
  });
};

export const deleteThread = async (query: Thread): Promise<Thread> => {
  const validationResult: ValidationResult = validateThread(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const existingThread: Thread = await getOne(model)({ query });

  if (_.isNull(existingThread)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No thread matches the selected query',
        userMessage: 'Cannot find this thread',
      },
    });
  }

  if (!_.isNull(existingThread.deletedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `Thread is already deleted: ${existingThread.id}`,
        userMessage: 'This thread is already deleted',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      deletedAt: timestamp,
    },
  });
};
