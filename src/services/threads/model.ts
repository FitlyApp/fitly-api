import { model, Schema, Model, Document } from 'mongoose';

const threadSchema: Schema = new Schema({
  participantsIds: [
    {
      // TODO: Change to ObjectId
      type: String,
    },
  ],
  createdAt: {
    type: Number,
    required: true,
  },
  updatedAt: {
    type: Number,
  },
  deletedAt: {
    type: Number,
  },
});

const threadModel: Model<Document> = model('Thread', threadSchema, 'threads');

export default threadModel;
