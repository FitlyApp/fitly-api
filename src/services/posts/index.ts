export {
  getPost,
  searchPosts,
  createPost,
  updatePost,
  deletePost,
} from './logic';
