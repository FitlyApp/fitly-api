import { model, Schema, Model, Document } from 'mongoose';

const postSchema: Schema = new Schema({
  description: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
  },
  authorId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
  updatedAt: {
    type: Number,
  },
  deletedAt: {
    type: Number,
  },
});

const postModel: Model<Document> = model('Post', postSchema, 'posts');

export default postModel;
