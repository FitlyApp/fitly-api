import * as _ from 'lodash';
import { validate, isValidMongoID, isValidUrl } from '../../utilities';
import { ValidationResult, Post } from '../../types';

export const validatePost = (post: Post): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid post ID: ${id}`,
    },
    imageUrl: {
      validationFunction: isValidUrl,
      invalidMessage: (imageUrl) => `Invalid post image URL: ${imageUrl}`,
    },
    authorId: {
      validationFunction: isValidMongoID,
      invalidMessage: (authorId) => `Invalid post author ID: ${authorId}`,
    },
  })(post);
};
