import * as _ from 'lodash';
import model from './model';
import { validatePost } from './validation';
import { getOne, getMany, createOne, updateOne } from '../../mongodb';
import {
  UnprocessableEntityError,
  NotFoundError,
  ConflictError,
} from '../../errors';
import { nowMillis } from '../../utilities';
import { Post, ValidationResult } from '../../types';

export const getPost = (query: Post): Promise<Post> => {
  const validationResult: ValidationResult = validatePost(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchPosts = (query: { deletedAt?: number }): Promise<Post[]> => {
  const { deletedAt = null } = query;

  // TODO: Validate arguments

  return getMany(model)({
    query: {
      deletedAt,
    },
  });
};

export const createPost = async (newPost: Post): Promise<Post> => {
  const validationResult: ValidationResult = validatePost(newPost);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const createdPost = await createOne(model)({
    document: {
      ...newPost,
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
    },
  });

  return createdPost;
};

export const updatePost = async (
  query: Post,
  updatedPost: Post,
): Promise<Post> => {
  const queryValidationResult: ValidationResult = validatePost(query);

  if (!queryValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: queryValidationResult.message,
        userMessage: null,
      },
    });
  }

  const postValidationResult: ValidationResult = validatePost(updatedPost);

  if (!postValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: postValidationResult.message,
        userMessage: null,
      },
    });
  }

  const existingPost: Post = await getOne(model)({ query });

  if (_.isNull(existingPost)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No post matches the selected query',
        userMessage: 'Cannot find this post',
      },
    });
  }

  return updateOne(model)({
    query,
    document: {
      ...updatedPost,
      updatedAt: nowMillis(),
    },
  });
};

export const deletePost = async (query: Post): Promise<Post> => {
  const validationResult: ValidationResult = validatePost(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const post: Post = await getOne(model)({ query });

  if (_.isNull(post)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No post matches the selected query',
        userMessage: 'Cannot find this post',
      },
    });
  }

  if (!_.isNull(post.deletedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `Post is already deleted: ${post.id}`,
        userMessage: 'This post is already deleted',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      deletedAt: timestamp,
    },
  });
};
