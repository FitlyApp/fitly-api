import * as _ from 'lodash';
import model from './model';
import { validateMessage } from './validation';
import { getOne, getMany, createOne, updateOne } from '../../mongodb';
import {
  UnprocessableEntityError,
  NotFoundError,
  ConflictError,
} from '../../errors';
import { nowMillis } from '../../utilities';
import { Message, ValidationResult } from '../../types';

export const getMessage = (query: Message): Promise<Message> => {
  const validationResult: ValidationResult = validateMessage(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchMessages = (query: Message): Promise<Message[]> => {
  const validationResult: ValidationResult = validateMessage(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getMany(model)({
    query,
    sorting: {
      createdAt: 'DESCENDING',
    },
  });
};

export const createMessage = async (newMessage: Message): Promise<Message> => {
  const validationResult: ValidationResult = validateMessage(newMessage);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const createdMessage = await createOne(model)({
    document: {
      ...newMessage,
      createdAt: nowMillis(),
      updatedAt: null,
      deletedAt: null,
    },
  });

  return createdMessage;
};

export const updateMessage = async (
  query: Message,
  updatedMessage: Message,
): Promise<Message> => {
  const queryValidationResult: ValidationResult = validateMessage(query);

  if (!queryValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: queryValidationResult.message,
        userMessage: null,
      },
    });
  }

  const messageValidationResult: ValidationResult = validateMessage(
    updatedMessage,
  );

  if (!messageValidationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: messageValidationResult.message,
        userMessage: null,
      },
    });
  }

  const existingMessage: Message = await getOne(model)({ query });

  if (_.isNull(existingMessage)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No message matches the selected query',
        userMessage: 'Cannot find this message',
      },
    });
  }

  return updateOne(model)({
    query,
    document: {
      ...updatedMessage,
      updatedAt: nowMillis(),
    },
  });
};

export const deleteMessage = async (query: Message): Promise<Message> => {
  const validationResult: ValidationResult = validateMessage(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const existingMessage: Message = await getOne(model)({ query });

  if (_.isNull(existingMessage)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No message matches the selected query',
        userMessage: 'Cannot find this message',
      },
    });
  }

  if (!_.isNull(existingMessage.deletedAt)) {
    throw new ConflictError({
      data: {
        developerMessage: `Message is already deleted: ${existingMessage.id}`,
        userMessage: 'This message is already deleted',
      },
    });
  }

  const timestamp: number = nowMillis();

  return updateOne(model)({
    query,
    document: {
      updatedAt: timestamp,
      deletedAt: timestamp,
    },
  });
};
