import { validate, isValidMongoID } from '../../utilities';
import { ValidationResult, Message } from '../../types';

export const validateMessage = (query: Message): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid message ID: ${id}`,
    },
    senderId: {
      validationFunction: isValidMongoID,
      invalidMessage: (senderId) => `Invalid message sender ID: ${senderId}`,
    },
    threadId: {
      validationFunction: isValidMongoID,
      invalidMessage: (threadId) => `Invalid message thread ID: ${threadId}`,
    },
  })(query);
};
