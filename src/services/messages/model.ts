import { model, Schema, Model, Document } from 'mongoose';

const messageSchema: Schema = new Schema({
  senderId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  threadId: {
    type: Schema.Types.ObjectId,
    ref: 'Thread',
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
  updatedAt: {
    type: Number,
  },
  deletedAt: {
    type: Number,
  },
});

const messageModel: Model<Document> = model(
  'Message',
  messageSchema,
  'messages',
);

export default messageModel;
