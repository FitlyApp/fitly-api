export {
  isLiking,
  getLikers,
  getLikersIds,
  getLikersCount,
  likePost,
  unlikePost,
} from './logic';
