import * as _ from 'lodash';
import model from './model';
import { validateQuery } from './validation';
import { getUsersByIds } from '../users';
import { getOne, getMany, count, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, ConflictError } from '../../errors';
import { nowMillis } from '../../utilities';
import { ValidationResult, User, PostLiking } from '../../types';

export const isLiking = async (
  userId: string,
  postId: string,
): Promise<boolean> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    postId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const postLikingDocument: PostLiking = await getOne(model)({
    query: {
      userId,
      postId,
    },
  });

  return !_.isNull(postLikingDocument);
};

export const getLikers = async (postId: string): Promise<User[]> => {
  const validationResult: ValidationResult = validateQuery({
    postId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const likersIds: string[] = await getLikersIds(postId);
  return getUsersByIds(likersIds);
};

export const getLikersIds = async (postId: string): Promise<string[]> => {
  const validationResult: ValidationResult = validateQuery({
    postId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const postLikingDocuments: PostLiking[] = await getMany(model)({
    query: {
      postId,
    },
  });

  return postLikingDocuments.map((document) => document.userId);
};

export const getLikersCount = async (postId: string): Promise<number> => {
  const validationResult: ValidationResult = validateQuery({
    postId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      postId,
    },
  });
};

export const likePost = async (
  userId: string,
  postId: string,
): Promise<PostLiking> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    postId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyLiking: boolean = await isLiking(userId, postId);

  if (isAlreadyLiking) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already likes this post: ${postId}`,
        userMessage: 'You already liked this post',
      },
    });
  }

  return createOne(model)({
    document: {
      userId,
      postId,
      createdAt: nowMillis(),
    },
  });
};

export const unlikePost = async (
  userId: string,
  postId: string,
): Promise<PostLiking> => {
  const validationResult: ValidationResult = validateQuery({
    userId,
    postId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const isAlreadyLiking: boolean = await isLiking(userId, postId);

  if (!isAlreadyLiking) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already unlikes this post: ${postId}`,
        userMessage: 'You already unliked this post',
      },
    });
  }

  return deleteOne(model)({
    query: {
      userId,
      postId,
    },
  });
};
