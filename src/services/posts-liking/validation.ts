import * as _ from 'lodash';
import { validate, isValidMongoID } from '../../utilities';
import { ValidationResult, PostLiking } from '../../types';

export const validateQuery = (query: PostLiking): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid event attending ID: ${id}`,
    },
    postId: {
      validationFunction: isValidMongoID,
      invalidMessage: (postId) => `Invalid post ID: ${postId}`,
    },
    userId: {
      validationFunction: isValidMongoID,
      invalidMessage: (userId) => `Invalid user ID: ${userId}`,
    },
  })(query);
};
