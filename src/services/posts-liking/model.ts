import { model, Schema, Model, Document } from 'mongoose';

const postsLikingSchema: Schema = new Schema({
  postId: {
    type: Schema.Types.ObjectId,
    ref: 'Post',
    required: true,
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const postsLikingModel: Model<Document> = model(
  'PostsLiking',
  postsLikingSchema,
  'posts-liking',
);

export default postsLikingModel;
