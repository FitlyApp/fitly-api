import * as _ from 'lodash';
import model from './model';
import { validateQuery } from './validation';
import { getOne, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, ConflictError } from '../../errors';
import { nowMillis } from '../../utilities';
import { ValidationResult, Blocking } from '../../types';

export const isBlocking = async (
  blockerId: string,
  blockedId: string,
): Promise<boolean> => {
  const validationResult: ValidationResult = validateQuery({
    blockerId,
    blockedId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const blockingDocument: Blocking = await getOne(model)({
    query: {
      blockerId,
      blockedId,
    },
  });

  return !_.isNull(blockingDocument);
};

export const blockUser = async (
  blockerId: string,
  blockedId: string,
): Promise<Blocking> => {
  const validationResult: ValidationResult = validateQuery({
    blockerId,
    blockedId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const hasAlreadyBlocked: boolean = await isBlocking(blockerId, blockedId);

  if (hasAlreadyBlocked) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already blocked this user: ${blockedId}`,
        userMessage: 'You already blocked this user',
      },
    });
  }

  return createOne(model)({
    document: {
      blockerId,
      blockedId,
      createdAt: nowMillis(),
    },
  });
};

export const unblockUser = async (
  blockerId: string,
  blockedId: string,
): Promise<Blocking> => {
  const validationResult: ValidationResult = validateQuery({
    blockerId,
    blockedId,
  });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const hasAlreadyBlocked: boolean = await isBlocking(blockerId, blockedId);

  if (!hasAlreadyBlocked) {
    throw new ConflictError({
      data: {
        developerMessage: `Viewer already unblocked this user: ${blockedId}`,
        userMessage: 'You already unblocked this user',
      },
    });
  }

  return deleteOne(model)({
    query: {
      blockerId,
      blockedId,
    },
  });
};
