import { model, Schema, Model, Document } from 'mongoose';

const blockingSchema: Schema = new Schema({
  blockerId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  blockedId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const blockingModel: Model<Document> = model(
  'Blocking',
  blockingSchema,
  'blocking',
);

export default blockingModel;
