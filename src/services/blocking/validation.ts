import * as _ from 'lodash';
import { validate, isValidMongoID } from '../../utilities';
import { Blocking, ValidationResult } from '../../types';

export const validateQuery = (query: Blocking): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid blocking ID: ${id}`,
    },
    blockerId: {
      validationFunction: isValidMongoID,
      invalidMessage: (blockerId) => `Invalid blocker user ID: ${blockerId}`,
    },
    blockedId: {
      validationFunction: isValidMongoID,
      invalidMessage: (blockedId) => `Invalid blocked user ID: ${blockedId}`,
    },
  })(query);
};
