import * as _ from 'lodash';
import model from './model';
import { validatePostComment } from './validation';
import { getOne, getMany, count, createOne, deleteOne } from '../../mongodb';
import { UnprocessableEntityError, NotFoundError } from '../../errors';
import { nowMillis } from '../../utilities';
import { PostComment, ValidationResult } from '../../types';

export const getPostComment = (query: PostComment): Promise<PostComment> => {
  const validationResult: ValidationResult = validatePostComment(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getOne(model)({ query });
};

export const searchPostsComments = (query: {
  postId?: string;
}): Promise<PostComment[]> => {
  const validationResult: ValidationResult = validatePostComment(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return getMany(model)({
    query,
    sorting: {
      createdAt: 'ASCENDING',
    },
  });
};

export const getPostCommentsCount = (postId: string): Promise<number> => {
  const validationResult: ValidationResult = validatePostComment({ postId });

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  return count(model)({
    query: {
      postId,
    },
  });
};

export const createPostComment = async (
  newPostComment: PostComment,
): Promise<PostComment> => {
  const validationResult: ValidationResult = validatePostComment(
    newPostComment,
  );

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const createdActivity = await createOne(model)({
    document: {
      ...newPostComment,
      createdAt: nowMillis(),
    },
  });

  return createdActivity;
};

export const deletePostComment = async (
  query: PostComment,
): Promise<PostComment> => {
  const validationResult: ValidationResult = validatePostComment(query);

  if (!validationResult.isValid) {
    throw new UnprocessableEntityError({
      data: {
        developerMessage: validationResult.message,
        userMessage: null,
      },
    });
  }

  const postComment: PostComment = await getOne(model)({ query });

  if (_.isNull(postComment)) {
    throw new NotFoundError({
      data: {
        developerMessage: 'No post comment matches the selected query',
        userMessage: 'Cannot find this post comment',
      },
    });
  }

  return deleteOne(model)({ query });
};
