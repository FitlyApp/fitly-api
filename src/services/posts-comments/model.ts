import { model, Schema, Model, Document } from 'mongoose';

const postCommentSchema: Schema = new Schema({
  content: {
    type: String,
    required: true,
  },
  authorId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  postId: {
    type: Schema.Types.ObjectId,
    ref: 'Post',
    required: true,
  },
  createdAt: {
    type: Number,
    required: true,
  },
});

const postCommentModel: Model<Document> = model(
  'PostComment',
  postCommentSchema,
  'posts-comments',
);

export default postCommentModel;
