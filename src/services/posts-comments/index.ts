export {
  getPostComment,
  searchPostsComments,
  getPostCommentsCount,
  createPostComment,
  deletePostComment,
} from './logic';
