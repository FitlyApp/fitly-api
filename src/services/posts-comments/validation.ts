import * as _ from 'lodash';
import { validate, isValidMongoID } from '../../utilities';
import { ValidationResult, PostComment } from '../../types';

export const validatePostComment = (query: PostComment): ValidationResult => {
  return validate({
    id: {
      validationFunction: isValidMongoID,
      invalidMessage: (id) => `Invalid post comment ID: ${id}`,
    },
    authorId: {
      validationFunction: isValidMongoID,
      invalidMessage: (userId) => `Invalid author ID: ${userId}`,
    },
    postId: {
      validationFunction: isValidMongoID,
      invalidMessage: (postId) => `Invalid post ID: ${postId}`,
    },
  })(query);
};
