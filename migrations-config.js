require('dotenv').config();

const nodeEnvironment = process.env.NODE_ENV;
const mongodbUsername = process.env.MONGODB_USERNAME;
const mongodbPassword = process.env.MONGODB_PASSWORD;

const mongodbConnectionUri =
  nodeEnvironment === 'production'
    ? `mongodb://${mongodbUsername}:${mongodbPassword}@mongodb:27017`
    : `mongodb://${mongodbUsername}:${mongodbPassword}@localhost:27017`;

module.exports = {
  mongodb: {
    url: mongodbConnectionUri,
    databaseName: 'fitly',
    options: {
      useNewUrlParser: true,
      authSource: 'admin',
    },
  },
  migrationsDir: 'migrations',
  changelogCollectionName: 'changelog',
};
