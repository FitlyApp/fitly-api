<div align="center">
  <p>
    <img src="https://gitlab.com/FitlyApp/fitly-api/uploads/4f570e4696672b81b68869750106a51b/server__1_.png" width="200"/>
  </p>

  <h1>fitly-api</h1>
</div>

## Production

### Prerequisites

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- `.env`
  - `NODE_ENV`
  - `JWT_SECRET`
  - `MONGODB_USERNAME`
  - `MONGODB_PASSWORD`

### Running

- `❯ docker-compose up -d --build`

## Development

### Prerequisites

- [Node.js](https://nodejs.org/en) `v14.15.4`
- [Yarn](https://yarnpkg.com/en/)
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- `.env`
  - `NODE_ENV`
  - `JWT_SECRET`
  - `MONGODB_USERNAME`
  - `MONGODB_PASSWORD`

### Running

- `❯ yarn`
- `❯ docker-compose --file docker-compose.dev.yml up`
- `❯ yarn migrate-up`
- `❯ yarn start-watch-ts` _(in one tab)_
- `❯ yarn start-watch-node` _(in another tab)_
